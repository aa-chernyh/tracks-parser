# Настройка фоновых задач #
***

#### Редактирование файла cron ####
```text
crontab -u $USER -e
```

#### Просмотр файла cron ####
```text
crontab -u $USER -l
```

- Сканирование всех интеграций (каждый день в 11:00)
```text
00 11 * * * docker exec -itd tracks-parser-app php /var/www/tracks-parser/backend/console/yii scan/all
```

- Скачивание всех файлов (каждый день в 14:00)
```text
00 14 * * * docker exec -itd tracks-parser-app php /var/www/tracks-parser/backend/console/yii download/all
```

- Удаление старых логов (каждый месяц первого числа)
```text
0 0 1 * * docker exec -itd tracks-parser-app php /var/www/tracks-parser/backend/console/yii logs/clean-old
```