Запуск проекта
=====================
Для корректной работы необходимо прописать домен в **/etc/hosts**
```
127.0.0.1 tracks-parser.local
127.0.0.1 v2.tracks-parser.local
127.0.0.1 apiv1.tracks-parser.local
127.0.0.1 apiv2.tracks-parser.local
127.0.0.1 ws.tracks-parser.local
```

Копирование файлов среды
```
cp -R ~/Документы/Проекты/tracks-parser/environment/development/tracks-parser/* ~/Документы/Проекты/tracks-parser/
```

Установка полных прав на mysql-data
```text
sudo chmod -R 777 ~/Документы/Проекты/tracks-parser/docker/containers/mysql/data
```

Первая установка node_modules
```text
v1
docker run -v $(pwd)/frontend:/app -w /app --rm node:current-alpine3.14 yarn install

v2
docker build -t tp-frontend-v2:latest $(pwd)/docker/containers/frontend-v2/
docker run -it --rm --volume $(pwd)/frontend-v2:/app -w /app tp-frontend-v2:latest yarn install
```

Первый запуск контейнеров
```
cd docker
docker-compose build
docker-compose up -d
```

Установка composer пакетов
```
docker exec -it tracks-parser-app composer install
```

Применение миграций
```
docker exec -it tracks-parser-app php console/yii migrate
```

Установка npm пакетов
```
docker exec -it tracks-parser-frontend yarn install
```

Запуск после настройки
```
sudo sh bash/development/up.sh
Frontend v1 - http://localhost:8080
Frontend v2 - http://localhost:8081
```

### Полезности

Сборка vue
```
cd ~/Документы/Проекты/tracks-parser
docker run -v $(pwd)/frontend:/app -w /app --rm node:current-alpine3.14 yarn install
docker run -v $(pwd)/frontend:/app -w /app --rm node:current-alpine3.14 yarn build
```

Запуск фоновой задачи из терминала
```
docker exec tracks-parser-app /var/www/tracks-parser/backend/console/yii контроллер(из папки backend/console/controllers)/действие
```

### Подключение к локальному mysql
```text
Host: 127.0.0.1
Port: 33060
User: tracks-parser
Password: tracks-parser
Database: tracks-parser
```

### Подключение к production mysql
Сначала надо пробросить порт с production сервера на локалочку:
```text
ssh -L 33066:localhost:33060 $USER@192.168.0.254
```

Затем подключиться:
```text
Host: 127.0.0.1
Port: 33066
User: tracks-parser  
Password: tracks-parser  
Database: tracks-parser
```
 
### Настройка proxy
```text
sudo nano /etc/environment
```
добавить строки:
```text
http_proxy_docker=http://login:password@ip:port/
https_proxy_docker=https://login:password@ip:port/
```

### Настройка Xdebug
```text
Открыть порт 9003 на хосте: sudo ufw allow 9003
В настройках Idea -> Languages & Frameworks -> PHP -> Servers
добавить сервер с именем Docker, хостом 127.0.0.1 и портом 80
смапить папку с проектом на /var/www/tracks-parser
```

### Frontend v2
Выполнение команд (build, install)
```text
cd ~/Документы/Проекты/tracks-parser
docker build -t tp-frontend-v2:latest ./docker/frontendNew/
docker run -it --rm --volume $(pwd)/frontendNew:/app -w /app tp-frontend-v2:latest команда

Примеры команд:
yarn install - Установка node_modules
quasar build - Билд SPA
```

Обычный запуск среды разработки осуществляется с помощью команды:
```text
docker-compose up -d
```

### Генерация favicon
```text
docker exec -it tracks-parser-frontend-v2 icongenie generate -m spa -i src/assets/favicon-original.png
```