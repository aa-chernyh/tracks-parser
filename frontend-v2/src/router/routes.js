import * as RoutesEnum from 'src/scripts/enums/RoutesEnum'

const routes = [
  {
    path: '/',
    redirect: RoutesEnum.Playlist
  },
  {
    path: RoutesEnum.Playlist,
    component: () => import('layouts/PlayerLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Playlist.vue'), name: 'Плейлист' },
    ],
  },
  {
    path: RoutesEnum.Filters,
    component: () => import('layouts/PlayerLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Filters.vue'), name: 'Фильтры' },
    ],
  },
  {
    path: RoutesEnum.Playlists,
    component: () => import('layouts/PlayerLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Playlists.vue'), name: 'Плейлисты' },
    ],
  },
  {
    path: RoutesEnum.Eq,
    component: () => import('layouts/PlayerLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Eq.vue'), name: 'Эквалайзер' }
    ],
  },
  {
    path: RoutesEnum.Account,
    component: () => import('layouts/PlayerLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Account.vue'), name: 'Аккаунт' }
    ],
  },
  {
    path: RoutesEnum.Authors,
    component: () => import('layouts/PlayerLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Authors.vue'), name: 'Авторы' }
    ],
  },
  {
    path: RoutesEnum.Settings,
    component: () => import('layouts/PlayerLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Settings.vue'), name: 'Настройки' }
    ],
  },
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
