export const tracksPerPage = 255
export const defaultTracksPage = 1
export const defaultVolumeLevel = 0.9
export const defaultCurrentTrack = {
  "id":null,
  "name":null,
  "extension":null,
  "pageUrl":null,
  "downloadUrl":null,
  "duration":null,
  "bpm":null,
  "key":null,
  "size":null,
  "releaseDate":null,
  "bitrate":null,
  "insertDate":null,
  "IntegrationForm":{
    "id":null,
    "name":null,
    "description":null
  },
  "authors":[],
  "styles":[]
}
