import AudioControllerHelper from 'src/scripts/helpers/AudioControllerHelper'
import { backendUrl } from 'assets/environment'
import ListenAudioEventsService from 'src/scripts/services/ListenAudioEventsService'
import { defaultVolumeLevel } from 'src/scripts/constants/PlayerConstants'

const downloadTrackUrl = backendUrl + '/player/download-track-by-id?trackId='
const defaultAudioPreload = 'metadata'

export default class AudioController {
  /** private */
  static instance = null

  /** private */
  constructor() {
    this.audioControllerHelper = new AudioControllerHelper()
    this.audio = new Audio('')
    this.audio.preload = defaultAudioPreload
    this.setVolumeLevel(defaultVolumeLevel)

    const listenAudioEventsService = new ListenAudioEventsService(this.audio, this)
    listenAudioEventsService.service()
  }

  /**
   * public
   * @return AudioController
   */
  static getInstance() {
    if(!AudioController.instance) {
      AudioController.instance = new AudioController()
    }

    return AudioController.instance
  }

  /** public */
  setTrack(track) {
    const lastIsPlaying = !this.audio.paused
    if(!this.audio.paused) {
      this.pause()
    }

    this.audio.src = downloadTrackUrl + track.id
    this.audioControllerHelper.setCurrentTrack(track)

    if(lastIsPlaying) {
      this.play()
    }
  }

  /** public */
  play() {
    return this.audio.play().catch(() => {})
  }

  /** public */
  pause() {
    this.audio.pause()
  }

  /** public */
  setVolumeLevel(level) {
    this.audio.volume = level
  }

  /** public */
  setPlayTime(seconds) {
    this.audio.currentTime = seconds
  }

  /** public */
  setIsLoop(isLoop) {
    this.audio.loop = isLoop
    this.audioControllerHelper.setIsLoop(isLoop)
  }
}
