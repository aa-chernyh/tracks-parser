import Utils from 'src/scripts/helpers/Utils'

export default class UrlController {
  static instance = null
  params = {}

  /** private */
  constructor() {
    this.url = new URL(window.location.href)
    this.readParamsFromUrl()
  }

  /** private */
  readParamsFromUrl() {
    this.url.searchParams.forEach((value, key) => {
      this.params[key] = value
    })
  }

  /**
   * public
   * @return UrlController
   */
  static getInstance() {
    if(!UrlController.instance) {
      UrlController.instance = new UrlController()
    }

    return UrlController.instance
  }

  /** public */
  getUrlParam(key) {
    return this.hasParam(key)
      ? this.params[key]
      : null
  }

  /** public */
  setUrlParam(key, value) {
    const alreadyHasParam = this.hasParam(key)
    this.params[key] = value

    alreadyHasParam
      ? this.url.searchParams.set(key, value)
      : this.url.searchParams.append(key, value)
  }

  /** public */
  removeUrlParam(key) {
    if(Utils.isset(this.params[key])) {
      delete this.params[key]
      this.url.searchParams.delete(key)
    }
  }

  /** public */
  updatePageHistory() {
    window.history.pushState({}, document.title, this.url.href)
    window.history.replaceState({}, document.title, this.url.href)
  }

  /** public */
  onRedirect() {
    this.url = new URL(window.location.href)
    for(const paramName in this.params) {
      const paramValue = this.params[paramName]
      this.setUrlParam(paramName, paramValue)
    }
    this.updatePageHistory()
  }

  /** public */
  hasParam(key) {
    return Utils.isset(this.params[key])
  }
}
