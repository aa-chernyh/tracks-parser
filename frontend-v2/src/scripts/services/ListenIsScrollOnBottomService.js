import { storeInstance } from 'src/store'

export default class ListenIsScrollOnBottomService {

  /** public */
  constructor() {}

  /** public */
  service() {
    window.onscroll = () => {
      const value = (window.innerHeight + window.pageYOffset) >= document.body.offsetHeight
      this.setIsScrollOnBottom(value)
    }
  }

  /** private */
  setIsScrollOnBottom(value) {
    if(this.getIsScrollOnBottom() !== value) {
      storeInstance.dispatch('common/setIsScrollOnBottom', value)
    }
  }

  /** private */
  getIsScrollOnBottom() {
    return storeInstance.state.common.isScrollOnBottom
  }
}
