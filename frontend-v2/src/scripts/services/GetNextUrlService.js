import * as WidthRangeEnum from 'src/scripts/enums/WidthRangeEnum'
import * as RoutesEnum from 'src/scripts/enums/RoutesEnum'

const redirectMap = {
  [RoutesEnum.Filters]: {
    'redirectTo': RoutesEnum.Playlist,
    'onWidthRanges': [
      WidthRangeEnum.Medium,
      WidthRangeEnum.Large,
      WidthRangeEnum.ExtraLarge,
    ]
  },
  [RoutesEnum.Eq]: {
    'redirectTo': RoutesEnum.Playlist,
    'onWidthRanges': [
      WidthRangeEnum.Medium,
      WidthRangeEnum.Large,
      WidthRangeEnum.ExtraLarge,
    ]
  },
}

export default class GetNextUrlService {

  /** public */
  constructor(toRoute, currentWidthRange) {
    this.toRoute = toRoute
    this.currentWidthRange = currentWidthRange
  }

  /** public */
  service() {
    for(const route in redirectMap) {
      if(route === this.toRoute) {
        if(redirectMap[route].onWidthRanges.includes(this.currentWidthRange)) {
          return redirectMap[route].redirectTo
        }

        break
      }
    }

    return this.toRoute
  }
}
