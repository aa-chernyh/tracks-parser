import { storeInstance } from 'src/store'
import GetNextUrlService from 'src/scripts/services/GetNextUrlService'
import { routerInstance } from 'src/router'

export default class RedirectIfCurrentPageIsUnavailableService {

  /** public */
  constructor(currentWidthRange) {
    this.currentWidthRange = currentWidthRange
    this.currentRoute = storeInstance.state.common.route.path
  }

  /** public */
  service() {
    const nextRoute = this.getNextRoute()
    if(nextRoute !== this.currentRoute) {
      routerInstance.push(nextRoute)
    }
  }

  /** private */
  getNextRoute() {
    const service = new GetNextUrlService(this.currentRoute, this.currentWidthRange)
    return service.service()
  }
}
