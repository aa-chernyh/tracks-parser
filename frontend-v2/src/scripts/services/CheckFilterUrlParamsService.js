import Utils from 'src/scripts/helpers/Utils'

export default class CheckFilterUrlParamsService {

  /** public */
  constructor(urlController, filters) {
    this.urlController = urlController
    this.filters = filters
  }

  /** public */
  service() {
    return this.hasFilterParams()
  }

  /** private */
  hasFilterParams() {
    const allFilterNames = this.getAllFilterNames()
    for(const i in allFilterNames) {
      const filterName = allFilterNames[i]
      if(this.urlController.hasParam(Utils.getShortFilterName(filterName))) {
        return true
      }
    }

    return false
  }

  /** private */
  getAllFilterNames() {
    return Object.keys(this.filters)
  }
}
