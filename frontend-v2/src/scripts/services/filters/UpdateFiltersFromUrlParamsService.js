import { arraySeparator, rangeSeparator } from 'src/scripts/constants/UrlConstants'
import AbstractFiltersService from 'src/scripts/services/filters/AbstractFiltersService'
import Utils from 'src/scripts/helpers/Utils'

export default class UpdateFiltersFromUrlParamsService extends AbstractFiltersService {

  /** public */
  constructor(filters, urlController) {
    super(filters)
    this.urlController = urlController
    this.filterValueFromUrl = null
  }

  /** public */
  service() {
    let newFilters = Utils.clone(this.filters)
    for(const filterName in this.filters) {
      const shortFilterName = Utils.getShortFilterName(filterName)
      this.filterValueFromUrl = this.urlController.getUrlParam(shortFilterName)

      if(this.filterValueFromUrl) {
        newFilters[filterName] = this.serviceFilterByType(this.filters[filterName])
      }
    }

    return newFilters
  }

  /** protected */
  serviceInputFilter(filter) {
    filter.value = this.filterValueFromUrl
    return filter
  }

  /** protected */
  serviceDateRangeFilter(filter) {
    const rangeValues = this.filterValueFromUrl.split(rangeSeparator)

    const valueFrom = rangeValues[0]
    if(valueFrom) {
      filter.valueFrom = valueFrom
    }

    const valueTo = rangeValues[1]
    if(valueTo) {
      filter.valueTo = valueTo
    }

    return filter
  }

  /** protected */
  serviceRangeFilter(filter) {
    const rangeValues = this.filterValueFromUrl.split(rangeSeparator)

    filter.isUse = true

    const rangeMin = rangeValues[0]
    if(rangeMin) {
      filter.RangeForm.min = rangeMin
    }

    const rangeMax = rangeValues[1]
    if(rangeMax) {
      filter.RangeForm.max = rangeMax
    }

    return filter
  }

  /** protected */
  serviceMultipleSelectorFilter(filter) {
    const selectedOptionsKeys = this.filterValueFromUrl.split(arraySeparator)
    for(const i in selectedOptionsKeys) {
      const optionKey = selectedOptionsKeys[i]
      for(const j in filter.options) {
        const option = filter.options[j]
        if(option.value === optionKey && !this.isOptionExist(filter.selected, optionKey)) {
          filter.selected.push(option)
          break
        }
      }
    }

    return filter
  }

  /** private */
  isOptionExist(options, optionKey) {
    for(const i in options) {
      const option = options[i]
      if(option.value === optionKey) {
        return true
      }
    }

    return false
  }

  /** protected */
  serviceCheckboxFilter(filter) {
    filter.value = JSON.parse(this.filterValueFromUrl)
    return filter
  }
}
