import { rangeSeparator, arraySeparator } from 'src/scripts/constants/UrlConstants'
import AbstractFiltersService from 'src/scripts/services/filters/AbstractFiltersService'
import Utils from 'src/scripts/helpers/Utils'

export default class SetUrlParamsFromFiltersService extends AbstractFiltersService {

  /** public */
  constructor(filters, urlController) {
    super(filters)
    this.urlController = urlController
  }

  /** public */
  service() {
    this.resetFilterUrlParams()
    const params = this.getParams()
    this.setParamsToUrl(params)
  }

  /** private */
  resetFilterUrlParams() {
    for(const filterName in this.filters) {
      const shortFilterName = Utils.getShortFilterName(filterName)
      this.urlController.removeUrlParam(shortFilterName)
    }
  }

  /** private */
  getParams() {
    let allParams = {}
    for (const filterName in this.filters) {
      const filterParams = this.serviceFilterByType(this.filters[filterName])
      if(filterParams) {
        allParams[Utils.getShortFilterName(filterName)] = filterParams
      }
    }

    return allParams
  }

  /** protected */
  serviceInputFilter(filter) {
    if(!filter.value) {
      return null
    }

    return filter.value
  }

  /** protected */
  serviceDateRangeFilter(filter) {
    if(!filter.valueFrom && !filter.valueTo) {
      return null
    }

    return [filter.valueFrom, filter.valueTo].join(rangeSeparator)
  }

  /** protected */
  serviceRangeFilter(filter) {
    if(!filter.isUse) {
      return null
    }

    return [filter.RangeForm.min, filter.RangeForm.max].join(rangeSeparator)
  }

  /** protected */
  serviceMultipleSelectorFilter(filter) {
    if(!filter.selected.length) {
      return null
    }

    const selectedValues = filter.selected.map((selected) => {
      return selected.value
    })
    return selectedValues.join(arraySeparator)
  }

  /** protected */
  serviceCheckboxFilter(filter) {
    if(!filter.value) {
      return null
    }

    return filter.value
  }

  /** private */
  setParamsToUrl(params) {
    for(const shortFilterName in params) {
      this.urlController.setUrlParam(shortFilterName, params[shortFilterName])
    }
    this.urlController.updatePageHistory()
  }
}
