import AbstractFiltersService from 'src/scripts/services/filters/AbstractFiltersService'

export default class ResetFiltersService extends AbstractFiltersService {

  /** public */
  constructor(filters) {
    super(filters)
  }

  /** public */
  service() {
    let newFilters = {}
    for(const filterName in this.filters) {
      newFilters[filterName] = this.serviceFilterByType(this.filters[filterName])
    }

    return newFilters
  }

  /** protected */
  serviceInputFilter(filter) {
    filter.value = ''
    return filter
  }

  /** protected */
  serviceDateRangeFilter(filter) {
    filter.valueFrom = ''
    filter.valueTo = ''
    return filter
  }

  /** protected */
  serviceRangeFilter(filter) {
    filter.isUse = false
    filter.RangeForm.min = filter.minValue
    filter.RangeForm.max = filter.maxValue
    return filter
  }

  /** protected */
  serviceMultipleSelectorFilter(filter) {
    filter.selected = []
    return filter
  }

  /** protected */
  serviceCheckboxFilter(filter) {
    filter.value = false
    return filter
  }
}
