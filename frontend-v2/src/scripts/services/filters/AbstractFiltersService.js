import * as FilterTypeEnum from 'src/scripts/enums/FilterTypeEnum'

export default class AbstractFiltersService {

  /** protected */
  constructor(filters) {
    this.filters = filters
  }

  /** protected */
  serviceFilterByType(filter) {
    switch (filter.type) {
      case FilterTypeEnum.Input:
        return this.serviceInputFilter(filter)
      case FilterTypeEnum.DateRange:
        return this.serviceDateRangeFilter(filter)
      case FilterTypeEnum.Range:
        return this.serviceRangeFilter(filter)
      case FilterTypeEnum.MultipleSelector:
        return this.serviceMultipleSelectorFilter(filter)
      case FilterTypeEnum.Checkbox:
        return this.serviceCheckboxFilter(filter)
      default:
        throw new Error('Нет такого типа фильтра')
    }
  }

  /** protected */
  serviceInputFilter(filter) {
    throw new Error('Метод не реализован')
  }

  /** protected */
  serviceDateRangeFilter(filter) {
    throw new Error('Метод не реализован')
  }

  /** protected */
  serviceRangeFilter(filter) {
    throw new Error('Метод не реализован')
  }

  /** protected */
  serviceMultipleSelectorFilter(filter) {
    throw new Error('Метод не реализован')
  }

  /** protected */
  serviceCheckboxFilter(filter) {
    throw new Error('Метод не реализован')
  }
}
