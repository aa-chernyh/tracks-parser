import ListenPageWidthService from 'src/scripts/services/ListenPageWidthService'
import ListenIsScrollOnBottomService from 'src/scripts/services/ListenIsScrollOnBottomService'
import ListenRedirectHooksService from 'src/scripts/services/ListenRedirectHooksService'
import RequestFiltersService from 'src/scripts/services/RequestFiltersService'
import UpdateFiltersFromUrlParamsService from 'src/scripts/services/filters/UpdateFiltersFromUrlParamsService'
import RequestPlaylistService from 'src/scripts/services/RequestPlaylistService'
import SetDefaultCurrentTrackService from 'src/scripts/services/SetDefaultCurrentTrackService'
import ClearFilterUrlParamsService from 'src/scripts/services/ClearFilterUrlParamsService'
import SetUrlParamsFromFiltersService from 'src/scripts/services/filters/SetUrlParamsFromFiltersService'
import CheckFilterUrlParamsService from 'src/scripts/services/CheckFilterUrlParamsService'
import ResetFiltersService from 'src/scripts/services/filters/ResetFiltersService'
import { storeInstance } from 'src/store'

export default class AppInitService {

  /** public */
  constructor(urlController) {
    this.urlController = urlController
  }

  /** public */
  async service() {
    this.listenPageWidth()
    this.listenIsScrollOnBottom()
    this.listenRedirectHooks()
    await this.prepareFilters()
    await this.requestPlaylist()
    this.setDefaultCurrentTrack()
  }

  /** private */
  listenPageWidth() {
    const listenPageWidthService = new ListenPageWidthService()
    listenPageWidthService.service()
  }

  /** private */
  listenIsScrollOnBottom() {
    const listenIsScrollOnBottomService = new ListenIsScrollOnBottomService()
    listenIsScrollOnBottomService.service()
  }

  /** private */
  listenRedirectHooks() {
    const listenRedirectHooksService = new ListenRedirectHooksService(this.urlController)
    listenRedirectHooksService.service()
  }

  /** private */
  async prepareFilters() {
    let filters = await this.requestFilters()

    if(this.isUrlHasFilterParams(filters)) {
      filters = this.resetFilters(filters)
      filters = this.updateFiltersFromUrlParams(filters)
    } else {
      this.clearFilterUrlParams(filters)
      this.setUrlParamsFromFilters(filters)
    }

    return this.setFilters(filters)
  }

  /** private */
  async requestFilters() {
    const requestFiltersService = new RequestFiltersService()
    return await requestFiltersService.service()
  }

  /** private */
  isUrlHasFilterParams(filters) {
    const checkFilterUrlParamsService = new CheckFilterUrlParamsService(this.urlController, filters)
    return checkFilterUrlParamsService.service()
  }

  /** private */
  resetFilters(filters) {
    const resetFiltersService = new ResetFiltersService(filters)
    return resetFiltersService.service()
  }

  /** private */
  updateFiltersFromUrlParams(filters) {
    const updateFiltersFromUrlParamsService = new UpdateFiltersFromUrlParamsService(filters, this.urlController)
    return updateFiltersFromUrlParamsService.service()
  }

  /** private */
  clearFilterUrlParams(filters) {
    const clearFilterUrlParamsService = new ClearFilterUrlParamsService(filters, this.urlController)
    clearFilterUrlParamsService.service()
  }

  /** private */
  setUrlParamsFromFilters(filters) {
    const setUrlParamsFromFiltersService = new SetUrlParamsFromFiltersService(filters, this.urlController)
    setUrlParamsFromFiltersService.service()
  }

  /** private */
  async setFilters(filters) {
    return storeInstance.dispatch('player/setFilters', filters)
  }

  /** private */
  async requestPlaylist() {
    this.requestPlaylistService = new RequestPlaylistService()
    return this.requestPlaylistService.service()
  }

  /** private */
  setDefaultCurrentTrack() {
    const setDefaultCurrentTrackService = new SetDefaultCurrentTrackService()
    setDefaultCurrentTrackService.service()
  }
}
