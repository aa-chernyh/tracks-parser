import * as WidthRangeEnum from 'src/scripts/enums/WidthRangeEnum'

const linkList = [
  {
    data: {
      title: 'Плейлист',
      icon: 'playlist_play',
      link: '/playlist'
    },
    showOnWidthRanges: [
      WidthRangeEnum.ExtraSmall,
      WidthRangeEnum.Small,
      WidthRangeEnum.Medium,
      WidthRangeEnum.Large,
      WidthRangeEnum.ExtraLarge,
    ]
  },
  {
    data: {
      title: 'Фильтры',
      icon: 'filter_alt',
      link: '/filters'
    },
    showOnWidthRanges: [
      WidthRangeEnum.ExtraSmall,
      WidthRangeEnum.Small,
    ]
  },
  {
    data: {
      title: 'Плейлисты',
      icon: 'queue_music',
      link: '/playlists'
    },
    showOnWidthRanges: [
      WidthRangeEnum.ExtraSmall,
      WidthRangeEnum.Small,
      WidthRangeEnum.Medium,
      WidthRangeEnum.Large,
      WidthRangeEnum.ExtraLarge,
    ]
  },
  {
    data: {
      title: 'Эквалайзер',
      icon: 'equalizer',
      link: '/eq'
    },
    showOnWidthRanges: [
      WidthRangeEnum.ExtraSmall,
      WidthRangeEnum.Small,
    ]
  },
  {
    data: {
      title: 'Аккаунт',
      icon: 'account_circle',
      link: '/account'
    },
    showOnWidthRanges: [
      WidthRangeEnum.ExtraSmall,
      WidthRangeEnum.Small,
      WidthRangeEnum.Medium,
      WidthRangeEnum.Large,
      WidthRangeEnum.ExtraLarge,
    ]
  },
  {
    data: {
      title: 'Авторы',
      icon: 'code',
      link: '/authors'
    },
    showOnWidthRanges: [
      WidthRangeEnum.ExtraSmall,
      WidthRangeEnum.Small,
      WidthRangeEnum.Medium,
      WidthRangeEnum.Large,
      WidthRangeEnum.ExtraLarge,
    ]
  },
  {
    data: {
      title: 'Настройки',
      icon: 'settings',
      link: '/settings'
    },
    showOnWidthRanges: [
      WidthRangeEnum.ExtraSmall,
      WidthRangeEnum.Small,
      WidthRangeEnum.Medium,
      WidthRangeEnum.Large,
      WidthRangeEnum.ExtraLarge,
    ]
  },
]

export default class GetMenuLinksService {

  /** public */
  constructor(widthRange) {
    this.widthRange = widthRange
  }

  /** public */
  service() {
    let linkListData = []
    linkList.forEach(link => {
      if(link.showOnWidthRanges.includes(this.widthRange)) {
        linkListData.push(link.data)
      }
    })

    return linkListData
  }
}
