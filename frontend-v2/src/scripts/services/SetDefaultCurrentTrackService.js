import { storeInstance } from 'src/store'
import Utils from 'src/scripts/helpers/Utils'
import AudioController from 'src/scripts/controllers/AudioController'

export default class SetDefaultCurrentTrackService {

  /** public */
  constructor() {
    this.tracks = storeInstance.state.player.tracks
  }

  /** public */
  service() {
    this.setFirstTrackToCurrent()
  }

  /** private */
  setFirstTrackToCurrent() {
    if(Utils.isset(this.tracks[0])) {
      AudioController.getInstance().setTrack(this.tracks[0])
    }
  }
}
