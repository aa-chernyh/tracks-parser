import { storeInstance } from 'src/store'
import ChangeCurrentTrackService from 'src/scripts/services/ChangeCurrentTrackService'
import * as ChangeCurrentTrackModeEnum from 'src/scripts/enums/ChangeCurrentTrackModeEnum'

export default class ListenAudioEventsService {

  /** public */
  constructor(audio, audioController) {
    this.audio = audio
    this.audioController = audioController
  }

  /** public */
  service() {
    this.listenOnTimeUpdate()
    this.listenOnPause()
    this.listenOnPlay()
    this.listenOnVolumeChange()
    this.listenOnPreviousTrack()
    this.listenOnNextTrack()
    this.listenOnEnded()
  }

  /** private */
  listenOnTimeUpdate() {
    this.audio.ontimeupdate = () => {
      this.setCurrentPlayTime(this.audio.currentTime)
    }
  }

  /** private */
  setCurrentPlayTime(time) {
    storeInstance.commit('player/setCurrentPlayTime', time)
  }

  /** private */
  listenOnPause() {
    this.audio.onpause = () => {
      this.setIsPlaying(false)
    }
  }

  /** private */
  listenOnPlay() {
    this.audio.onplay = () => {
      this.setIsPlaying(true)
    }
  }

  /** private */
  setIsPlaying(isPlaying) {
    storeInstance.dispatch('player/setIsPlaying', isPlaying)
  }

  /** private */
  listenOnVolumeChange() {
    this.audio.onvolumechange = () => {
      this.setVolumeLevel(this.audio.volume)
    }
  }

  /** private */
  setVolumeLevel(level) {
    storeInstance.commit('player/setVolumeLevel', level)
  }

  /** private */
  listenOnPreviousTrack() {
    navigator.mediaSession.setActionHandler('previoustrack', () => {
      const changeCurrentTrackService = new ChangeCurrentTrackService(ChangeCurrentTrackModeEnum.Prev, this.audioController)
      changeCurrentTrackService.service()
    })
  }

  /** private */
  listenOnNextTrack() {
    navigator.mediaSession.setActionHandler('nexttrack', () => {
      const changeCurrentTrackService = new ChangeCurrentTrackService(ChangeCurrentTrackModeEnum.Next, this.audioController)
      changeCurrentTrackService.service()
    })
  }

  /** private */
  listenOnEnded() {
    this.audio.onended = () => {
      const service = new ChangeCurrentTrackService(ChangeCurrentTrackModeEnum.Next, this.audioController)
      service.service().then(() => {
        this.audioController.play()
      })
    }
  }
}
