import { routerInstance } from 'src/router'
import GetNextUrlService from 'src/scripts/services/GetNextUrlService'
import {storeInstance} from 'src/store'

export default class ListenRedirectHooksService {

  /** public */
  constructor(urlController) {
    this.urlController = urlController
  }

  /** public */
  service() {
    this.listenBeforeRedirectHook()
    this.listenAfterRedirectHook()
  }

  /** private */
  listenBeforeRedirectHook() {
    routerInstance.beforeEach((to, from, next) => {
      const nextRoute = this.getNextRoute(to.path)
      nextRoute !== to.path
        ? next(nextRoute)
        : next()
    })
  }

  /** private */
  getNextRoute(toRoute) {
    const getNextUrlService = new GetNextUrlService(toRoute, this.getCurrentWidthRange())
    return getNextUrlService.service()
  }

  /** private */
  getCurrentWidthRange() {
    return storeInstance.state.common.widthRange
  }

  /** private */
  listenAfterRedirectHook() {
    routerInstance.afterEach((to, from) => {
      this.urlController.onRedirect()
    })
  }
}
