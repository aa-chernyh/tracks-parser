import { storeInstance } from 'src/store'
import * as BreakpointsSizeEnum from 'src/scripts/enums/BreakpointsSizeEnum'
import * as WidthRangeEnum from 'src/scripts/enums/WidthRangeEnum'

export default class ListenPageWidthService {

  /** public */
  constructor() {}

  /** public */
  service() {
    this.setWidthRange()
    this.listenPageWidth()
  }

  /** private */
  setWidthRange() {
    let widthRange = this.getCurrentWidthRange()
    if(this.widthRangeOnStore() !== widthRange) {
      this.setWidthRangeToStore(widthRange)
    }
  }

  /** private */
  getCurrentWidthRange() {
    const width = window.innerWidth

    if(width <= BreakpointsSizeEnum.ExtraSmallMax) {
      return WidthRangeEnum.ExtraSmall
    }

    if(width >= BreakpointsSizeEnum.SmallMin && width <= BreakpointsSizeEnum.SmallMax) {
      return WidthRangeEnum.Small
    }

    if(width >= BreakpointsSizeEnum.MediumMin && width <= BreakpointsSizeEnum.MediumMax) {
      return WidthRangeEnum.Medium
    }

    if(width >= BreakpointsSizeEnum.LargeMin && width <= BreakpointsSizeEnum.LargeMax) {
      return WidthRangeEnum.Large
    }

    return WidthRangeEnum.ExtraLarge
  }

  /** private */
  widthRangeOnStore() {
    return storeInstance.state.common.widthRange
  }

  /** private */
  setWidthRangeToStore(widthRange) {
    storeInstance.dispatch('common/setWidthRange', widthRange)
  }

  /** private */
  listenPageWidth() {
    window.onresize = () => {
      this.setWidthRange()
    }
  }
}
