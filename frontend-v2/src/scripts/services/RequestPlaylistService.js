import RequestHelper from 'src/scripts/helpers/RequestHelper'
import { storeInstance } from 'src/store'
import { tracksPerPage } from 'src/scripts/constants/PlayerConstants'

export default class RequestPlaylistService {

  /** public */
  constructor() {
    this.filters = storeInstance.state.player.filters
    this.tracks = storeInstance.state.player.tracks
    this.currentTracksPage = storeInstance.state.player.currentTracksPage
  }

  /** public */
  async service() {
    const request = this.getRequest()
    const response = await RequestHelper.send('player/get-playlist', request)
    this.setResult(response)
  }

  /** private */
  getRequest() {
    let request = {}
    request.FiltersForm = this.filters
    request.PaginationForm = {
      perPage: tracksPerPage,
      currentPage: this.currentTracksPage,
      totalCount: null
    }

    return request
  }

  /** private */
  setResult(response) {
    if (response.isSuccess) {
      const tracks = [].concat((this.currentTracksPage > 1 ? this.tracks : []), response.data.tracks)
      storeInstance.commit('player/setTracks', tracks)
      storeInstance.commit('player/setTracksTotalCount', response.data.PaginationForm.totalCount)
    }

    const validationErrors = response.validationErrors && response.validationErrors.FiltersForm
      ? response.validationErrors.FiltersForm
      : {}
    storeInstance.commit('player/setFiltersValidationErrors', validationErrors)
  }
}
