import { storeInstance } from 'src/store'
import { tracksPerPage } from 'src/scripts/constants/PlayerConstants'

export default class IncrementCurrentTracksPageService {

  /** public */
  constructor() {}

  /** public */
  service() {
    const maxPageNumber = Math.ceil(this.getTotalTracksCount() / tracksPerPage)

    if(
      this.getTotalTracksCount()
      && this.getCurrentTracksPage() < maxPageNumber
    ) {
      this.setCurrentTracksPage(this.getCurrentTracksPage() + 1)
      return true
    }

    return false
  }

  /** private */
  getCurrentTracksPage() {
    return storeInstance.state.player.currentTracksPage
  }

  /** private */
  setCurrentTracksPage(page) {
    storeInstance.dispatch('player/setCurrentTracksPage', page)
  }

  /** private */
  getTotalTracksCount() {
    return storeInstance.state.player.tracksTotalCount
  }
}
