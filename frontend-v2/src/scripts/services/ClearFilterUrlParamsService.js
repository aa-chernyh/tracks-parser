import Utils from 'src/scripts/helpers/Utils'

export default class ClearFilterUrlParamsService {

  /** public */
  constructor(filters, urlController) {
    this.filters = filters
    this.urlController = urlController
  }

  /** public */
  service() {
    const allFilterNames = this.getAllFilterNames()
    for(const i in allFilterNames) {
      const filterName = allFilterNames[i]
      this.urlController.removeUrlParam(Utils.getShortFilterName(filterName))
    }
  }

  /** private */
  getAllFilterNames() {
    return Object.keys(this.filters)
  }
}
