import { storeInstance } from 'src/store'

export default class CheckCurrentRouteService {

  /** public */
  constructor(routes) {
    this.routes = routes
    this.currentRoutePath = storeInstance.state.common.route.path
  }

  /** public */
  service() {
    return this.isCurrentRouteInRoutes()
  }

  /** private */
  isCurrentRouteInRoutes() {
    return this.routes.includes(this.currentRoutePath)
  }
}
