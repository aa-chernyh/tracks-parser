import RequestHelper from 'src/scripts/helpers/RequestHelper'

export default class RequestFiltersService {

  /** public */
  constructor() {}

  /** public */
  async service() {
    return await this.requestFilters()
  }

  /** private */
  async requestFilters() {
    const response = await RequestHelper.send('player/get-filters')
    return response.isSuccess ? response.data.FiltersForm : {}
  }
}
