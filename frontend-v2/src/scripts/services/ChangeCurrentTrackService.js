import { storeInstance } from 'src/store'
import * as ChangeCurrentTrackModeEnum from 'src/scripts/enums/ChangeCurrentTrackModeEnum'
import Utils from 'src/scripts/helpers/Utils'
import IncrementCurrentTracksPageService from 'src/scripts/services/IncrementCurrentTracksPageService'
import RequestPlaylistService from 'src/scripts/services/RequestPlaylistService'

export default class ChangeCurrentTrackService {

  /** private */
  static serviceInWork = false

  /** public */
  constructor(mode, audioController) {
    this.mode = mode
    this.audioController = audioController
    this.currentTrackId = storeInstance.state.player.currentTrack.id
  }

  /** public */
  async service() {
    if(!ChangeCurrentTrackService.serviceInWork) {
      ChangeCurrentTrackService.serviceInWork = true
      await this.changeCurrentTrack()
      ChangeCurrentTrackService.serviceInWork = false
    }
  }

  /** private */
  async changeCurrentTrack() {
    const allTracks = this.getTracks()
    let futureTrack = allTracks[0]

    for(let increment = 0; increment < allTracks.length; increment++) {
      const track = allTracks[increment]

      if (track.id === this.currentTrackId) {
        if (this.mode === ChangeCurrentTrackModeEnum.Next) {
          if (Utils.isset(allTracks[increment + 1])) {
            futureTrack = allTracks[increment + 1]
          } else if (await this.loadNextPlaylistPage()) {
            futureTrack = this.getTracks()[increment + 1]
          }
        }

        if (this.mode === ChangeCurrentTrackModeEnum.Prev && Utils.isset(allTracks[increment - 1])) {
          futureTrack = allTracks[increment - 1]
        }

        break
      }
    }

    this.audioController.setTrack(futureTrack)
  }

  /** private */
  getTracks() {
    return storeInstance.state.player.tracks
  }

  /** private */
  async loadNextPlaylistPage() {
    if(this.incrementCurrentTracksPage()) {
      await this.requestPlaylist()
      return true
    }

    return false
  }

  /** private */
  incrementCurrentTracksPage() {
    const incrementCurrentTracksPageService = new IncrementCurrentTracksPageService()
    return incrementCurrentTracksPageService.service()
  }

  /** private */
  requestPlaylist() {
    const requestPlaylistService = new RequestPlaylistService()
    return requestPlaylistService.service()
  }
}
