export const ExtraSmallMax = 599
export const SmallMin = 600
export const SmallMax = 1023
export const MediumMin = 1024
export const MediumMax = 1439
export const LargeMin = 1440
export const LargeMax = 1919
export const ExtraLargeMin = 1920
