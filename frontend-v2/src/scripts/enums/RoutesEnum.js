export const Playlist = '/playlist'
export const Filters = '/filters'
export const Playlists = '/playlists'
export const Eq = '/eq'
export const Account = '/account'
export const Authors = '/authors'
export const Settings = '/settings'
