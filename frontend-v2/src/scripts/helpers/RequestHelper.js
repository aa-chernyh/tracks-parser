import { storeInstance } from 'src/store'
import { backendUrl } from 'assets/environment'

export default class RequestHelper {
  static async send (method, request = null) {
    const response = await storeInstance.state.common.axios.post(method, JSON.stringify(request), {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      }
    }).catch((error) => {
      console.log('Ошибка запроса ' + method + ': ' + error)
      return null
    })

    return response.data
  }

  static download (method) {
    let link = document.createElement('a');
    link.setAttribute('href', backendUrl + method);
    link.setAttribute('download', 'download');
    link.click()
  }
}
