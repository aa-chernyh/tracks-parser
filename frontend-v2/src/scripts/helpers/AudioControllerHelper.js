import { storeInstance } from 'src/store'

export default class AudioControllerHelper {
  constructor() {}

  getVolumeLevel() {
    return storeInstance.state.player.volumeLevel
  }

  setCurrentTrack(track) {
    storeInstance.dispatch('player/setCurrentTrack', track)
  }

  setIsPlaying(isPlaying) {
    storeInstance.dispatch('player/setIsPlaying', isPlaying)
  }

  setIsLoop(isLoop) {
    storeInstance.commit('player/setIsLoop', isLoop)
  }
}
