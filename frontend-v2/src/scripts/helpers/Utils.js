import { filterFormEnding } from 'src/scripts/constants/UrlConstants'

export default class Utils {
  static getTimeStringFromDuration(duration) {
    const hours = Math.floor(duration / 3600);
    const minutes = Math.floor((duration - (hours * 3600)) / 60);
    const seconds = Math.floor(duration - (hours * 3600) - (minutes * 60));

    let result = ''
    if(hours > 0) result += hours.toString() + ':'

    if(minutes < 10) result += '0'
    result += minutes.toString() + ':'

    if(seconds < 10) result += '0'
    result += seconds.toString()

    return result;
  }

  /**
   * Слоняет численное значение: 1 ягода, 2 ягоды, 7 ягод
   *
   * @param number - число
   * @param after - массив из 3х значений, например ['ягода', 'ягоды', 'ягод']
   */
  static getPluralForm(number, after) {
    return after[(number % 100 > 4 && number % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(number % 10 < 5) ? Math.abs(number) % 10 : 5]]
  }

  static clone(object) {
    return JSON.parse(JSON.stringify(object))
  }

  static isset(value) {
    return typeof(value) != 'undefined' && value !== null
  }

  static roundToHundredths(value) {
    const result = Math.round(value * 100) / 100
    if(result === 0) {
      return 0
    }

    return result
  }

  static sprintf(format, ...args) {
    let i = 0;
    return format.replace(/%s/g, () => args[i++]);
  }

  static getShortFilterName(filterName) {
    return filterName.replace(filterFormEnding, '')
  }
}
