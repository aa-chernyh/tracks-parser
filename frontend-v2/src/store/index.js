import {store} from 'quasar/wrappers'
import {createStore} from 'vuex'

import common from './common'
import player from './player'

let storeInstance = null

export default store(function () {
  storeInstance = createStore({
    modules: {
      common,
      player,
    },
    strict: process.env.DEBUGGING
  })

  return storeInstance
})

export { storeInstance }
