import IncrementCurrentTracksPageService from 'src/scripts/services/IncrementCurrentTracksPageService'
import RequestPlaylistService from 'src/scripts/services/RequestPlaylistService'
import CheckCurrentRouteService from 'src/scripts/services/CheckCurrentRouteService'
import * as RoutesEnum from 'src/scripts/enums/RoutesEnum'
import RedirectIfCurrentPageIsUnavailableService from 'src/scripts/services/RedirectIfCurrentPageIsUnavailableService'

export function setWidthRange(context, rangeEnum) {
  context.commit('setWidthRange', rangeEnum)

  const service = new RedirectIfCurrentPageIsUnavailableService(rangeEnum)
  service.service()
}

export function setRoute(context, route) {
  context.commit('setRoute', route)
}

export function setIsScrollOnBottom(context, isScrollOnBottom) {
  context.commit('setIsScrollOnBottom', isScrollOnBottom)

  const checkCurrentRouteService = new CheckCurrentRouteService(RoutesEnum.Playlist)
  const isPlaylistPage = checkCurrentRouteService.service()

  if(isScrollOnBottom && isPlaylistPage) {
    const incrementCurrentTracksPageService = new IncrementCurrentTracksPageService()
    if(incrementCurrentTracksPageService.service()) {
      const requestPlaylistService = new RequestPlaylistService()
      requestPlaylistService.service()
    }
  }
}

