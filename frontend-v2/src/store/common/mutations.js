export function setWidthRange(state, rangeEnum) {
  state.widthRange = rangeEnum
}

export function setRoute(state, route) {
  state.route = route
}

export function setIsScrollOnBottom(state, isScrollOnBottom) {
  state.isScrollOnBottom = isScrollOnBottom
}
