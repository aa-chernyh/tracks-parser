import axios from 'axios'
import { backendUrl } from 'assets/environment'

export default function () {
  return {
    widthRange: undefined,
    isScrollOnBottom: false,
    route: undefined,
    axios: axios.create({
      baseURL: backendUrl
    })
  }
}
