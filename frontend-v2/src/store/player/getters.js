export function getCurrentTracksCount(state) {
  return state.tracks.length
}

export function getCurrentTrackAuthorNames(state) {
  let authorNames = []
  state.currentTrack.authors.forEach(function(author) {
    authorNames.push(author.name)
  })

  if(authorNames.length === 0) {
    authorNames.push('Неизвестно')
  }

  return authorNames.join(', ')
}
