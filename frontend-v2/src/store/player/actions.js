export function setFilters(context, filters) {
  context.commit('setFilters', filters)
}

export function setCurrentTracksPage(context, page) {
  context.commit('setCurrentTracksPage', page)
}

export function setCurrentTrack(context, track) {
  context.commit('setCurrentTrack', track)
}

export function setIsPlaying(context, isPlaying) {
  context.commit('setIsPlaying', isPlaying)
}
