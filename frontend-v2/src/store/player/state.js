import { defaultTracksPage, defaultCurrentTrack, defaultVolumeLevel } from 'src/scripts/constants/PlayerConstants'

export default function () {
  return {
    tracksTotalCount: 0,
    currentTracksPage: defaultTracksPage,
    tracks: [],
    filtersValidationErrors: {},
    currentTrack: defaultCurrentTrack,
    volumeLevel: defaultVolumeLevel,
    currentPlayTime: 0,
    isPlaying: false,
    isLoop: false,
  }
}
