export function setFilters(state, filters) {
  state.filters = filters
}

export function setTracks(state, tracks) {
  state.tracks = tracks
}

export function setCurrentTracksPage(state, page) {
  state.currentTracksPage = page
}

export function setTracksTotalCount(state, count) {
  state.tracksTotalCount = count
}

export function setFiltersValidationErrors(state, errors) {
  state.filtersValidationErrors = errors
}

export function setCurrentTrack(state, track) {
  state.currentTrack = track
}

export function setVolumeLevel(state, level) {
  state.volumeLevel = level
}

export function setCurrentPlayTime(state, time) {
  state.currentPlayTime = time
}

export function setIsPlaying(state, isPlaying) {
  state.isPlaying = isPlaying
}

export function setIsLoop(state, isLoop) {
  state.isLoop = isLoop
}
