class PlayerInterface {

    static callEvent = (type) => {
        document.dispatchEvent(new Event(type))
    }

    static play = () => {
        this.callEvent('player.play')
    }

    static pause = () => {
        this.callEvent('player.pause')
    }

    static next = () => {
        this.callEvent('player.next')
    }

    static previous = () => {
        this.callEvent('player.previous')
    }

    static shuffle = () => {
        this.callEvent('player.shuffle')
    }

    static resetCurrentTime = () => {
        this.callEvent('player.resetCurrentTime')
    }
}

module.exports = PlayerInterface