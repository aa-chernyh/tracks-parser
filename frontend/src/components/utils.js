const isset = (variable) => {
    return typeof(variable) !== 'undefined'
}

const empty = (variable) => {
    return Object.keys(variable).length === 0
}

export {isset, empty}