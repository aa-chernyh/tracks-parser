import {send} from "@/api"
import {empty} from "../../components/utils";

const defaultVolumeLevel = 95
const changeTrackDelay = 500

export default {
    namespaced: true,
    state: {
        filters: [],
        activeFilters: [],

        tracks: [],
        currentTrack: {},

        tracksIdToPositionMapping: {},

        shownTracksCount: 0,
        foundTracksCount: 0,

        volume: defaultVolumeLevel,
        currentTrackPosition: 0,
        currentTrackPositionSeeker: 0,

        isRepeatEnabled: false,
        isPlayed: false,
        isTrackLoading: false,
        isFiltersLoading: false,
        filtersLoadingPromise: Promise.resolve(),
        isTracklistLoading: false,
        tracksLoadingPromise: Promise.resolve(),
        isTrackChangedRecently: false,

        audio: undefined,
    },
    mutations: {
        setFilters(state, filters) {
            state.filters = filters
        },
        setActiveFilters(state, activeFilters) {
            state.activeFilters = activeFilters
        },
        setTracks(state, tracks) {
            state.tracks = tracks
        },
        setShownTracksCount(state, count) {
            state.shownTracksCount = count
        },
        setFoundTracksCount(state, count) {
            state.foundTracksCount = count
        },
        setCurrentTrack(state, track) {
            state.currentTrack = track
        },
        setVolume(state, level) {
            state.volume = level
        },
        setIsRepeatEnabled(state, isRepeatEnabled) {
            state.isRepeatEnabled = isRepeatEnabled
        },
        setCurrentTrackPosition(state, currentTrackPosition) {
            state.currentTrackPosition = currentTrackPosition
        },
        setCurrentTrackPositionSeeker(state, currentTrackPositionSeeker) {
            state.currentTrackPositionSeeker = currentTrackPositionSeeker
        },
        setIsPlayed(state, isPlayed) {
            state.isPlayed = isPlayed
        },
        setIsTrackLoading(state, isTrackLoading) {
            state.isTrackLoading = isTrackLoading
        },
        setIsTrackChangedRecently(state, isTrackChangedRecently) {
            state.isTrackChangedRecently = isTrackChangedRecently
        },
        setAudio(state, audio) {
            state.audio = audio
        },
        setTracksIdToPositionMapping(state, tracksIdToPositionMapping) {
            state.tracksIdToPositionMapping = tracksIdToPositionMapping
        },
        setIsFiltersLoading(state, isFiltersLoading) {
            state.isFiltersLoading = isFiltersLoading
        },
        setFiltersLoadingPromise(state, filtersLoadingPromise) {
            state.filtersLoadingPromise = filtersLoadingPromise
        },
        setIsTracklistLoading(state, isTracklistLoading) {
            state.isTracklistLoading = isTracklistLoading
        },
        setTracklistLoadingPromise(state, filtersTracklistPromise) {
            state.filtersTracklistPromise = filtersTracklistPromise
        },
    },
    actions: {
        setTracks(context, tracks) {
            context.commit('setTracks', tracks)
        },
        setShownTracksCount(context, count) {
            context.commit('setShownTracksCount', count)
        },
        setFoundTracksCount(context, count) {
            context.commit('setFoundTracksCount', count)
        },
        setCurrentTrack(context, track) {
            if (context.getters.isCanChangeTrack) {
                context.commit('setCurrentTrack', track)
                context.dispatch('setIsTrackChangedRecently')
            }
        },
        setActiveFilters(context, activeFilters) {
            context.commit('setActiveFilters', activeFilters)
        },
        setFiltersToActive(context, filters) {
            let activeFilters = context.state.activeFilters
            for (let k = 0; k < filters.length; k++) {
                for (let i = 0; i < activeFilters.length; i++) {
                    if (activeFilters[i].key === filters[k].key) {
                        activeFilters.splice(i, 1);
                        break;
                    }
                }

                if (filters[k].value) {
                    let name = ''
                    let rusValue = ''
                    for (let i = 0; i < context.state.filters.length; i++) {
                        if (context.state.filters[i].key === filters[k].key) {
                            name = context.state.filters[i].name

                            switch (context.state.filters[i].type) {
                                case 'select':
                                    for (let j = 0; j < context.state.filters[i].positions.length; j++) {
                                        if (context.state.filters[i].positions[j].key === filters[k].value) {
                                            rusValue = context.state.filters[i].positions[j].name
                                            break;
                                        }
                                    }
                                    break;
                                case 'checkbox':
                                    rusValue = context.state.filters[i].name
                                    break;
                                default:
                                    break;
                            }

                            break;
                        }
                    }

                    activeFilters.push({
                        key: filters[k].key,
                        value: filters[k].value,
                        name: name,
                        rusValue: rusValue,
                    })
                }
            }

            context.dispatch('setActiveFilters', activeFilters)
        },
        removeFilterFromActive(context, key) {
            let activeFilters = context.state.activeFilters

            for (let i = 0; i < activeFilters.length; i++) {
                if (activeFilters[i].key === key) {
                    activeFilters.splice(i, 1)
                }
            }

            context.dispatch('setActiveFilters', activeFilters)
        },
        setIsPlayed(context, isPlayed) {
            context.commit('setIsPlayed', isPlayed)
        },
        setCurrentTrackPosition(context, currentTrackPosition) {
            context.commit('setCurrentTrackPosition', currentTrackPosition)
        },
        setCurrentTrackPositionSeeker(context, currentTrackPositionSeeker) {
            context.commit('setCurrentTrackPositionSeeker', currentTrackPositionSeeker)
        },
        setVolume(context, level) {
            context.commit('setVolume', level)
        },
        setIsRepeatEnabled(context, isRepeatEnabled) {
            context.commit('setIsRepeatEnabled', isRepeatEnabled)
        },
        requestFilters(context) {
            if(empty(context.state.filters) && !context.state.isFiltersLoading) {
                context.commit('setIsFiltersLoading', true)
                let filtersLoadingPromise = send('player/get-filters').then((filters) => {
                    context.commit('setFilters', filters)
                    context.commit('setIsFiltersLoading', false)
                })
                context.commit('setFiltersLoadingPromise', filtersLoadingPromise)
            }

            return context.state.filtersLoadingPromise
        },
        updateTrack(context, track) {
            let tracks = context.state.tracks
            for (let i = 0; i < tracks.length; i++) {
                if (tracks[i].id === track.id) {
                    tracks[i] = track
                    break
                }
            }
            context.commit('setTracks', tracks)

            if (track.id === context.state.currentTrack.id) {
                context.commit('setCurrentTrack', track)
            }
            //todo send('player/update-track')
        },
        setIsTrackLoading(context, isTrackLoading) {
            context.commit('setIsTrackLoading', isTrackLoading)
        },
        setIsTrackChangedRecently(context) {
            context.commit('setIsTrackChangedRecently', true)
            setTimeout(() => {
                context.commit('setIsTrackChangedRecently', false)
            }, changeTrackDelay)
        },
        setAudio(context, audio) {
            context.commit('setAudio', audio)
        },
        setTracksIdToPositionMapping(context, tracksIdToPositionMapping) {
            context.commit('setTracksIdToPositionMapping', tracksIdToPositionMapping)
        },
        requestTracklist(context) {
            if(!context.state.isTracklistLoading) {
                context.commit('setIsTrackLoading', true)
                let tracksLoadingPromise = send('player/get-tracklist', context.state.activeFilters).then((response) => {
                    context.dispatch('setTracks', response.tracks)
                    context.dispatch('setShownTracksCount', response.shownTracksCount)
                    context.dispatch('setFoundTracksCount', response.foundTracksCount)
                    context.commit('setIsTrackLoading', false)
                })
                context.commit('setTracklistLoadingPromise', tracksLoadingPromise)
            }

            return context.state.tracksLoadingPromise
        },
    },
    getters: {
        isCanChangeTrack(state) {
            return !state.isTrackLoading && !state.isTrackChangedRecently
        }
    },
}