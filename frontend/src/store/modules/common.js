import {alertTimeout} from "@/components/constants";

export default {
    namespaced: true,
    state: {
        successMessage: '',
        errorMessage: '',
        successTimeoutId: undefined,
        errorTimeoutId: undefined,
    },
    mutations: {
        setSuccessMessage(state, message) {
            state.successMessage = message
        },
        setErrorMessage(state, message) {
            state.errorMessage = message
        },
        setSuccessTimeoutId(state, id) {
            state.successTimeoutId = id
        },
        setErrorTimeoutId(state, id) {
            state.errorTimeoutId = id
        },
    },
    actions: {
        setSuccessMessage(context, message) {
            if(context.state.successMessage !== '') {
                clearTimeout(context.state.successTimeoutId)
                context.commit('setSuccessTimeoutId', undefined)
            }

            context.commit('setSuccessMessage', message)
            let id = setTimeout(() => {
                context.commit('setSuccessMessage', '')
            }, alertTimeout)
            context.commit('setSuccessTimeoutId', id)
        },
        setErrorMessage(context, message) {
            if(context.state.errorMessage !== '') {
                clearTimeout(context.state.errorTimeoutId)
                context.commit('setErrorTimeoutId', undefined)
            }

            context.commit('setErrorMessage', message)
            let id = setTimeout(() => {
                context.commit('setErrorMessage', '')
            }, alertTimeout)
            context.commit('setErrorTimeoutId', id)
        },
    }
}