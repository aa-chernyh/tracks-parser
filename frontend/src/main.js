import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap';
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faBackward, faPlay, faPause, faForward, faVolumeUp, faVolumeMute, faRandom, faSync, faStar,
    faFileDownload, faFileArchive, faArrowRight, faCogs
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import Multiselect from 'vue-multiselect'

Vue.use(Vuex)
library.add(
    faBackward, faPlay, faPause, faForward, faVolumeUp, faVolumeMute, faRandom, faSync, faStar,
    faFileDownload, faFileArchive, faArrowRight, faCogs
)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('multiselect', Multiselect)
Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
