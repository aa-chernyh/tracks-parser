import axios from 'axios'
import {backUrl, wsUrl} from '@/components/environment'

const axiosInstance = axios.create({
    baseURL: backUrl
})

const baseSend = async (method, request = null) => {
    let response = await axiosInstance.post(method, JSON.stringify(request), {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
        }
    }).catch((error) => {
        throw 'Неожиданный ответ сервера: ' + error
    });

    const successHttpStatus = 200
    if (response.status === successHttpStatus) {
        return response.data
    } else {
        throw 'Неверный статус ответа: ' + response.status
    }
}

const send = (method, data = null) => {
    return baseSend(method, data).then((baseResponseStruct) => {
        if (baseResponseStruct.isSuccess) {
            return baseResponseStruct.data
        } else if (baseResponseStruct.error) {
            throw baseResponseStruct.error
        } else {
            throw 'Результат не ок и нет сообщений об ошибке'
        }
    })
}

const download = (method) => {
    let link = document.createElement('a');
    link.setAttribute('href', backUrl + method);
    link.setAttribute('download', 'download');
    link.click()
}

const setDefaultWsEvents = (connect) => {
    connect.onerror = () => {
        connect.close()
        console.log('Ошибка при подключении к WebSocket серверу. Соединение закрыто')
    }
}

const getWsConnect = () => {
    let connect = new WebSocket(wsUrl)
    setDefaultWsEvents(connect)

    return connect
}

const prepareWsData = (method, data) => {
    return JSON.stringify({action: method, data: data})
}

const parseWsResult = (method, event) => {
    let response = (JSON.parse(event.data))
    let result = response.result && response.action === method && response.data
    return {result: result, data: response.data}
}

export {send, download, getWsConnect, prepareWsData, parseWsResult}