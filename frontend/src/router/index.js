import Vue from 'vue'
import VueRouter from 'vue-router'

import Config from '../views/Config.vue'
import ConfigPromodj from '../views/config/Promodj'
import ConfigVK from '../views/config/VK'
import Player from '../views/Player.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Player',
        component: Player
    },
    {
        path: '/config',
        name: 'Config',
        component: Config
    },
    {
        path: '/config/promodj',
        name: 'ConfigPromodj',
        component: ConfigPromodj
    },
    {
        path: '/config/vk',
        name: 'ConfigVK',
        component: ConfigVK
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
