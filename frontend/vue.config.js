// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const path = require('path')

module.exports = {
    runtimeCompiler: true,
    productionSourceMap: false,
    css: {
        extract: false
    },
    configureWebpack: {
        plugins: [
            // new BundleAnalyzerPlugin()
        ],
        resolve: {
            alias: {
                "@src": path.resolve(__dirname, 'src/'),
                "@img": path.resolve(__dirname, 'src/assets/img'),
                "@css": path.resolve(__dirname, 'src/assets/css')
            },
            extensions: [
                '.js', '.json', '.vue', '.css'
            ]
        }
    }
}