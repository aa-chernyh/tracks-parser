<?php

namespace console\controllers;

use common\components\MetaConsoleController;
use common\managers\DownloadManager;
use yii\console\ExitCode;

class DownloadController extends MetaConsoleController
{

    /**
     * @var DownloadManager
     */
    private $downloadManager;

    /**
     * DownloadController constructor.
     * @param $id
     * @param $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->downloadManager = new DownloadManager();
    }

    /**
     * @return int
     * @throws \Throwable
     */
    public function actionAll(): int
    {
        $this->downloadManager->downloadAll();
        return ExitCode::OK;
    }
}
