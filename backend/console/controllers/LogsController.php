<?php

namespace console\controllers;

use common\components\MetaConsoleController;
use yii\console\ExitCode;
use common\managers\CleanLogManager;

class LogsController extends MetaConsoleController
{

    /**
     * @var CleanLogManager
     */
    private $cleanLogManager;

    /**
     * LogsController constructor.
     * @param $id
     * @param $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->cleanLogManager = new CleanLogManager();
    }

    /**
     * @return int
     */
    public function actionCleanOld(): int
    {
        $this->cleanLogManager->cleanOld();
        return ExitCode::OK;
    }

}
