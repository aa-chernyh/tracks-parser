<?php

namespace console\controllers;

use common\components\MetaConsoleController;
use common\exceptions\LogicException;
use common\integrations\common\BaseIntegration;
use common\managers\ScanManager;
use Throwable;
use yii\console\ExitCode;

class ScanController extends MetaConsoleController
{

    /**
     * @var ScanManager
     */
    private $scanManager;

    /**
     * ScanController constructor.
     * @param $id
     * @param $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->scanManager = new ScanManager();
    }

    /**
     * @return int
     * @throws Throwable
     * @throws LogicException
     */
    public function actionAll(): int
    {
        $this->scanManager->startScanByIntegrationList(BaseIntegration::LIST);
        return ExitCode::OK;
    }

    public function actionById(string $integrationId): int
    {
        $this->scanManager->startByIntegrationId($integrationId);
        return ExitCode::OK;
    }

}
