<?php

namespace console\controllers;

use common\components\MetaConsoleController;
use api\v1\forms\player\TracklistFiltersForm;
use socket\models\SocketRequestForm;
use socket\models\SocketResponseForm;
use consik\yii2websocket\events\WSClientMessageEvent;
use Exception;
use common\managers\PlayerManager;
use yii\helpers\ArrayHelper;

class PlayerController extends MetaConsoleController
{

    /**
     * @var PlayerManager
     */
    private $playerManager;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->playerManager = new PlayerManager();
    }

    /**
     * Метод работает через WebSocketServer
     * @param WSClientMessageEvent $event
     * @param SocketRequestForm $request
     * @throws Exception
     */
    public function actionCreateTracklistArchive(WSClientMessageEvent $event, SocketRequestForm $request): void
    {
        $filtersArray = ArrayHelper::getValue($request->data, 'filters');

        $trackslistFiltersForm = new TracklistFiltersForm();
        $trackslistFiltersForm->loadFromActiveFilterList($filtersArray);
        $path = $this->playerManager->createTracksArchive($trackslistFiltersForm);

        $response = (new SocketResponseForm($request))->setData(['path' => $path])->toString();
        $event->client->send($response);
    }
}