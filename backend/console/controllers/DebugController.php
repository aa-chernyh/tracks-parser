<?php

namespace console\controllers;

use common\collections\TracksCollection;
use common\components\MetaConsoleController;
use common\helpers\CollectionsAdapter;
use common\models\Track;
use common\services\RemoveTracksService;
use Yii;

class DebugController extends MetaConsoleController
{

    public function actionRemoveNotLoadedTracks(): void
    {
        $tracksReadable = CollectionsAdapter::getTrackFacades(Track::find()->select(['id', 'path'])->all());
        $collection = new TracksCollection($tracksReadable);
        $notLoadedTrackReadables = $collection->notDownloaded()->all();

        $service = new RemoveTracksService($notLoadedTrackReadables);
        $service->service();
    }
}