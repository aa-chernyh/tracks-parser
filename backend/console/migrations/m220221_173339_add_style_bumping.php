<?php

use common\components\MetaMigration;
use common\models\Style;
use PromodjSDK\enums\Styles;

class m220221_173339_add_style_bumping extends MetaMigration
{

    public function up()
    {
        $style = new Style();
        $style->id = Styles::BUMPING;
        $style->name = Styles::BUMPING_NAME;
        $style->insert();
    }

    public function down()
    {
        Style::deleteAll(['id' => Styles::BUMPING]);
    }
}