<?php

use common\components\MetaMigration;

class m210321_215232_add_table_async_session extends MetaMigration
{

    public function up()
    {
        $this->truncateTable('async_process');

        $this->createTable('async_session', [
            'id' => $this->primaryKey(),
            'process_key' => $this->string()->notNull()->comment('Ключ процесса'),
            'start_date' => $this->dateTime()->comment('Дата начала'),
            'end_date' => $this->dateTime()->comment('Дата окончания'),
            'status' => $this->string()->notNull()->comment('Статус сессии'),
        ]);

        $this->createIndex(
            'idx-async_session-status',
            'async_session',
            'status'
        );

        $this->dropIndex(
            'idx-async_process-is_launched',
            'async_process'
        );

        $this->addColumn(
            'async_process',
            'async_session_id',
            $this->integer()->notNull()->comment('AsyncSession::id')
        );

        $this->addForeignKey(
            'fk-async_process-async_session_id-async_session-id',
            'async_process',
            'async_session_id',
            'async_session',
            'id',
            'CASCADE'
        );

        $this->dropColumn(
            'async_process',
            'key',
        );

        $this->dropColumn(
            'async_process',
            'is_launched',
        );

        $this->dropColumn(
            'async_process',
            'is_killed_by_user',
        );

        $this->dropColumn(
            'async_process',
            'process_key',
        );

        $this->addColumn(
            'async_process',
            'status',
            $this->string()->notNull()->comment('Статус процесса')
        );
    }

    public function down()
    {
        echo 'То, что было не вернёшь... Да и надо ли?';
        return false;
    }

}