<?php

use common\components\MetaMigration;

class m210509_122848_drop_tables_async extends MetaMigration
{

    public function up()
    {
        $this->dropTable('async_process');
        $this->dropTable('async_session');
    }

    public function down()
    {
        echo 'Не надо это возвращать -)';
    }

}