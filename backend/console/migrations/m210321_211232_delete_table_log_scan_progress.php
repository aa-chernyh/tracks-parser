<?php

use common\components\MetaMigration;

class m210321_211232_delete_table_log_scan_progress extends MetaMigration
{

    public function up()
    {
        $this->dropForeignKey(
            'fk-log_scan_progress-source-integration-key',
            'log_scan_progress'
        );

        $this->dropTable('log_scan_progress');
    }

    public function down()
    {
        $this->createTable('log_scan_progress', [
            'id' => $this->primaryKey(),
            'source' => $this->string()->notNull()->comment('Источник'),
            'status' => $this->string()->notNull()->comment('Статус'),
            'text' => $this->string(1023)->comment('Текст'),
            'date' => $this->dateTime()->comment('Дата'),
            'scan_position_number' => $this->integer(8)->comment('Позиция в очереди сканирования'),
            'scan_max_number' => $this->integer(8)->comment('Количество элементов в очереди сканирования'),
        ]);

        $this->addForeignKey(
            'fk-log_scan_progress-source-integration-key',
            'log_scan_progress',
            'source',
            'integration',
            'key',
            'CASCADE'
        );
    }

}