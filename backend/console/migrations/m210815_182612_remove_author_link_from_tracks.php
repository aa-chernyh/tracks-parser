<?php

use common\components\MetaMigration;
use common\models\Track;

class m210815_182612_remove_author_link_from_tracks extends MetaMigration
{

    public function up()
    {
        $this->dropForeignKey('fk-authors-id', Track::tableName());
        $this->dropColumn(Track::tableName(), 'author_id');

        return true;
    }

    public function down()
    {
        echo 'Миграцию невозможно отменить!';
        return false;
    }

}