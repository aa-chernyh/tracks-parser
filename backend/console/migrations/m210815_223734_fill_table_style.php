<?php

use common\components\MetaMigration;
use common\models\Style;
use PromodjSDK\enums\Styles;

class m210815_223734_fill_table_style extends MetaMigration
{

    public function up()
    {
        $stylesData = [];
        foreach (Styles::MAPPING as $id => $name) {
            $stylesData[] = [$id, $name];
        }

        Yii::$app->getDb()->createCommand()->batchInsert(
            Style::tableName(),
            ['id', 'name'],
            $stylesData
        )->execute();
    }

    public function down()
    {
        $this->truncateTable(Style::tableName());
    }

}