<?php

use common\components\MetaMigration;
use common\models\Style;
use common\models\Track;

class m210815_224219_create_table_track_to_style extends MetaMigration
{

    private const LINK_TABLE_NAME = 'track_to_style';

    public function up()
    {
        $this->createTable(self::LINK_TABLE_NAME, [
            'track_id' => $this->integer()->comment('ID трека'),
            'style_id' => $this->string()->comment('ID стиля'),
        ]);

        $this->addPrimaryKey(
            self::LINK_TABLE_NAME . '-pk',
            self::LINK_TABLE_NAME,
            ['track_id', 'style_id'],
        );

        $this->addForeignKey(
            'fk-' . self::LINK_TABLE_NAME . '-track_id-track-id',
            self::LINK_TABLE_NAME,
            'track_id',
            Track::tableName(),
            'id',
        );

        $this->addForeignKey(
            'fk-' . self::LINK_TABLE_NAME . '-style_id-style-id',
            self::LINK_TABLE_NAME,
            'style_id',
            Style::tableName(),
            'id',
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-track-id', self::LINK_TABLE_NAME);
        $this->dropForeignKey('fk-style-id', self::LINK_TABLE_NAME);

        $this->dropTable(self::LINK_TABLE_NAME);
    }

}