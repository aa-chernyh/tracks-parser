<?php

use common\components\MetaMigration;

class m201220_200458_add_table_log_any_error extends MetaMigration
{

    public function up()
    {
        $this->createTable('log_any_error', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime()->comment('Дата'),
            'message' => $this->string(1023)->comment('Текст ошибки'),
            'method' => $this->string(1023)->comment('Класс::Метод'),
            'exception_name' => $this->string(511)->comment('Имя Exception-класса')
        ]);
    }

    public function down()
    {
        $this->dropTable('log_any_error');
    }

}