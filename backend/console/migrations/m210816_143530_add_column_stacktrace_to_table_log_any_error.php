<?php

use common\components\MetaMigration;
use common\models\LogAnyError;

class m210816_143530_add_column_stacktrace_to_table_log_any_error extends MetaMigration
{

    public function up()
    {
        $this->addColumn(
            LogAnyError::tableName(),
            'stacktrace',
            $this->text()->comment('StackTrace')
        );
    }

    public function down()
    {
        $this->dropColumn(
            LogAnyError::tableName(),
            'stacktrace'
        );
    }

}