<?php

use common\components\MetaMigration;
use common\models\Integration;
use common\integrations\common\BaseIntegration;

class m210825_191414_add_integration_vk extends MetaMigration
{

    public function up()
    {
        $integration = new Integration();
        $integration->id = BaseIntegration::VK;
        $integration->name = 'Вконтакте';
        $integration->description = 'Социальная сеть.';

        $integration->insert();
    }

    public function down()
    {
        Integration::deleteAll(['id' => BaseIntegration::VK]);
    }

}