<?php

use common\components\MetaMigration;

class m210507_232300_create_queue_task_table extends MetaMigration
{
    const TABLE_NAME = 'queue_task';
    
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'class_name' => $this->string()->notNull()->comment('Имя класса Job-а'),
            'params' => $this->json()->comment('Json параметров класса Job-а'),
            'start_date' => $this->dateTime()->comment('Дата начала'),
            'end_date' => $this->dateTime()->comment('Дата окончания'),
            'status' => $this->string(63)->notNull()->comment('Статус процесса'),
            'pid' => $this->integer(6)->comment('Id процесса'),
            'error' => $this->string(1023)->comment('Текст ошибки'),
            'attempt' => $this->integer(3)->comment('Номер попытки'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}