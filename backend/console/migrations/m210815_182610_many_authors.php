<?php

use common\components\MetaMigration;
use common\models\Author;
use common\models\Track;

class m210815_182610_many_authors extends MetaMigration
{

    const LINK_TABLE_NAME = 'track_to_author';

    public function up()
    {
        $this->createTable(self::LINK_TABLE_NAME, [
            'track_id' => $this->integer()->comment('ID трека'),
            'author_id' => $this->integer()->comment('ID автора'),
        ]);

        $this->addPrimaryKey(
            'track_to_author_pk',
            'track_to_author',
            ['track_id', 'author_id'],
        );

        $this->addForeignKey(
            'fk-track-id',
            self::LINK_TABLE_NAME,
            'track_id',
            Track::tableName(),
            'id',
        );

        $this->addForeignKey(
            'fk-author-id',
            self::LINK_TABLE_NAME,
            'author_id',
            Author::tableName(),
            'id',
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-track-id', self::LINK_TABLE_NAME);
        $this->dropForeignKey('fk-author-id', self::LINK_TABLE_NAME);

        $this->dropTable(self::LINK_TABLE_NAME);
    }

}