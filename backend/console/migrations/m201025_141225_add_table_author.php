<?php

use yii\db\Migration;

class m201025_141225_add_table_author extends Migration
{

    public function safeUp()
    {
        $tableOptions = '';
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('author', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->defaultValue('')->unique()->comment('Имя'),
            'url' => $this->string(1023)->comment('Ссылка на страницу'),
            'real_name' => $this->string()->comment('Реальное имя'),
            'meta' => $this->json()->comment('Параметры, специфичные для интеграций'),
        ], $tableOptions);

        $this->createIndex(
            'idx-author-url',
            'author',
            'url'
        );
    }

    public function safeDown()
    {
        $this->dropIndex(
            'idx-author-url',
            'author'
        );

        $this->dropTable('author');
    }

}
