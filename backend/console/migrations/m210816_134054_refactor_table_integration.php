<?php

use common\components\MetaMigration;
use common\models\Integration;
use common\models\Track;

class m210816_134054_refactor_table_integration extends MetaMigration
{

    public function up()
    {
        $this->dropForeignKey('fk-track-source-integration-key', Track::tableName());
        $this->alterColumn(
            Integration::tableName(),
            'id',
            $this->integer()
        );
        $this->dropPrimaryKey('id', Integration::tableName());

        $this->dropColumn(Integration::tableName(), 'id');
        $this->renameColumn(Integration::tableName(), 'key', 'id');
        $this->alterColumn(
            Integration::tableName(),
            'id',
            $this->string()->comment('ID (константа BaseIntegration)')
        );

        $this->addPrimaryKey('id', Integration::tableName(), 'id');

        $this->renameColumn(Track::tableName(), 'source', 'integration');
        $this->addForeignKey(
            'fk-track-integration-integration-id',
            Track::tableName(),
            'integration',
            Integration::tableName(),
            'id',
        );
    }

    public function down()
    {
        echo "migration can't be reverted";
        return false;
    }

}