<?php

use yii\db\Migration;

class m201217_205916_add_missed_keys_and_indexes extends Migration
{

    public function safeUp()
    {
        $this->addForeignKey(
            'fk-log_scan_progress-source-integration-key',
            'log_scan_progress',
            'source',
            'integration',
            'key',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-track-source-integration-key',
            'track',
            'source',
            'integration',
            'key',
            'CASCADE'
        );

        $this->createIndex(
            'idx-log_scan_progress-date',
            'log_scan_progress',
            'date'
        );
    }

    public function safeDown()
    {
        $this->dropIndex(
            'idx-log_scan_progress-date',
            'log_scan_progress'
        );

        $this->dropForeignKey(
            'fk-track-source-integration-key',
            'track'
        );

        $this->dropForeignKey(
            'fk-log_scan_progress-source-integration-key',
            'log_scan_progress'
        );
    }
}
