<?php

use common\components\MetaMigration;

class m210321_210615_delete_table_log_model_validation_error extends MetaMigration
{

    public function up()
    {
        $this->dropTable('log_model_validation_error');
    }

    public function down()
    {
        $this->createTable('log_model_validation_error', [
            'id' => $this->primaryKey(),
            'model' => $this->string()->notNull()->comment('Объект валидации'),
            'fields' => $this->json()->comment('Список ошибочных полей'),
            'date' => $this->dateTime()->comment('Дата'),
            'method' => $this->string(1023)->comment('Класс::Метод')
        ]);
    }

}