<?php

use common\components\MetaMigration;
use queue\models\QueueTask;

class m210519_211500_alter_queue_task_table extends MetaMigration
{
    const TABLE_NAME = 'queue_task';
    const COLUMN = 'priority';
    
    public function safeUp()
    {
        $this->addColumn(
            self::TABLE_NAME,
            self::COLUMN,
            $this->integer(3)->notNull()->defaultValue(QueueTask::PRIORITY_DEFAULT)->comment('Приоритет')
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            self::TABLE_NAME,
            self::COLUMN
        );
    }
}