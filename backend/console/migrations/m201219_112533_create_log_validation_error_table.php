<?php

use common\components\MetaMigration;

class m201219_112533_create_log_validation_error_table extends MetaMigration
{

    public function safeUp()
    {
        $this->createTable('log_model_validation_error', [
            'id' => $this->primaryKey(),
            'model' => $this->string()->notNull()->comment('Объект валидации'),
            'fields' => $this->json()->comment('Список ошибочных полей'),
            'date' => $this->dateTime()->comment('Дата'),
            'method' => $this->string(1023)->comment('Класс::Метод')
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('log_model_validation_error');
    }
}
