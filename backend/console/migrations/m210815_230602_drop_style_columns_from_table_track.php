<?php

use common\components\MetaMigration;
use common\models\Track;

class m210815_230602_drop_style_columns_from_table_track extends MetaMigration
{

    public function up()
    {
        $this->dropColumn(Track::tableName(), 'style_1');
        $this->dropColumn(Track::tableName(), 'style_2');
    }

    public function down()
    {
        $this->addColumn(
            Track::tableName(),
            'style_1',
            $this->string()->comment('Стиль 1')
        );

        $this->addColumn(
            Track::tableName(),
            'style_2',
            $this->string()->comment('Стиль 2')
        );
    }

}