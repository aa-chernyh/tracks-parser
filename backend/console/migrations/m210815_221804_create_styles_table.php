<?php

use common\components\MetaMigration;

class m210815_221804_create_styles_table extends MetaMigration
{

    private const TABLE_NAME = 'style';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->string()->comment('ID'),
            'name' => $this->string()->comment('Название')
        ]);

        $this->addPrimaryKey('style_pk', self::TABLE_NAME, 'id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
