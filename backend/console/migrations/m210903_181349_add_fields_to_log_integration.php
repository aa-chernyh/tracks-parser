<?php

use common\components\MetaMigration;
use common\models\LogIntegration;

class m210903_181349_add_fields_to_log_integration extends MetaMigration
{

    public function up()
    {
        $this->addColumn(
            LogIntegration::tableName(),
            'date',
            $this->dateTime()->comment('Дата выполнения запроса')
        );

        $this->addColumn(
            LogIntegration::tableName(),
            'errors',
            $this->json()->comment('Ошибки')
        );
    }

    public function down()
    {
        $this->dropColumn(LogIntegration::tableName(), 'date');
        $this->dropColumn(LogIntegration::tableName(), 'errors');
    }

}