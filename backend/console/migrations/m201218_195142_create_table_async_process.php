<?php

use common\components\MetaMigration;

class m201218_195142_create_table_async_process extends MetaMigration
{

    public function safeUp()
    {
        $this->createTable('async_process', [
            'id' => $this->primaryKey(),
            'pid' => $this->integer(5)->notNull()->comment('Id процесса'),
            'key' => $this->string()->notNull()->comment('Название процесса или ключ интеграции'),
            'start_date' => $this->dateTime()->comment('Дата начала'),
            'end_date' => $this->dateTime()->comment('Дата окончания'),
            'is_launched' => $this->boolean()->notNull()->defaultValue(false)->comment('Процесс запущен'),
            'is_killed_by_user' => $this->boolean()->notNull()->defaultValue(false)->comment('Процесс убит пользователем'),
        ]);

        $this->createIndex(
            'idx-async_process-is_launched',
            'async_process',
            'is_launched'
        );
    }

    public function safeDown()
    {
        $this->dropIndex(
            'idx-async_process-is_launched',
            'async_process'
        );

        $this->dropTable('async_process');
    }

}
