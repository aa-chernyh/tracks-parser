<?php

use common\components\MetaMigration;
use common\models\Track;

class m210823_142004_alter_track_column_key extends MetaMigration
{

    public function up()
    {
        $this->alterColumn(
            Track::tableName(),
            'key',
            $this->string(4)->comment('Тональность')
        );
    }

    public function down()
    {
        $this->alterColumn(
            Track::tableName(),
            'key',
            $this->string(2)->comment('Тональность')
        );
    }

}