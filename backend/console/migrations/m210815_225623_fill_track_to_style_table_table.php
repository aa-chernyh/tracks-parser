<?php

use common\components\MetaMigration;
use common\models\Track;
use common\models\TrackToStyle;
use PromodjSDK\enums\Styles;

class m210815_225623_fill_track_to_style_table_table extends MetaMigration
{

    public function up()
    {
        $tracksQuery = Track::find()->select(['id', 'style_1', 'style_2']);
        $flippedPdjStylesMapping = array_flip(Styles::MAPPING);

        $trackToStyleRelations = [];
        /** @var Track $track */
        foreach ($tracksQuery->each() as $track) {
            if($track->style_1 && in_array($track->style_1, Styles::NAME_LIST)) {
                $trackToStyleRelations[] = [$track->id, $flippedPdjStylesMapping[$track->style_1]];
            }

            if($track->style_2 && in_array($track->style_2, Styles::NAME_LIST)) {
                $trackToStyleRelations[] = [$track->id, $flippedPdjStylesMapping[$track->style_2]];
            }
        }

        Yii::$app->getDb()->createCommand()->batchInsert(
            TrackToStyle::tableName(),
            ['track_id', 'style_id'],
            $trackToStyleRelations
        )->execute();
    }

    public function down()
    {
        $this->truncateTable(TrackToStyle::tableName());
    }

}