<?php

use common\components\MetaMigration;
use common\models\Track;

class m210517_143723_remove_tracks_with_incorrect_path extends MetaMigration
{

    public function up()
    {
        /** @var Track $track */
        $incorrectTrackIds = Track::find()->where(['like', 'path', 'filesPromodj'])->select(['id'])->column();
        Track::deleteAll(['id' => $incorrectTrackIds]);
    }

    public function down()
    {
        echo 'Не возвратишь(';
    }

}