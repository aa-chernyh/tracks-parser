<?php

use common\components\MetaMigration;

class m210327_160101_add_column_is_marked_to_table_tracks extends MetaMigration
{

    public function up()
    {
        $this->addColumn(
            'track',
            'is_marked',
            $this->boolean()->notNull()->defaultValue(false)->comment('Избранное')
        );
    }

    public function down()
    {
        $this->dropColumn(
            'track',
            'is_marked'
        );
    }

}