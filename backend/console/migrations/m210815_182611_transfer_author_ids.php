<?php

use common\components\MetaMigration;
use common\models\Track;
use common\models\TrackToAuthor;

class m210815_182611_transfer_author_ids extends MetaMigration
{

    public function up()
    {
        $tracksQuery = Track::find()->select(['id', 'author_id']);

        $tracksData = [];
        /** @var Track $track */
        foreach ($tracksQuery->each() as $track) {
            $tracksData[] = [$track->id, $track->author_id];
        }

        Yii::$app->getDb()->createCommand()->batchInsert(
            TrackToAuthor::tableName(),
            ['track_id', 'author_id'],
            $tracksData
        )->execute();

        return true;
    }

    public function down()
    {
        echo 'Миграцию невозможно отменить!';
        return false;
    }

}