<?php

use yii\db\Migration;

class m201025_142150_add_table_track extends Migration
{

    public function safeUp()
    {
        $tableOptions = '';
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('track', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer()->notNull()->comment('author::id'),
            'source' => $this->string()->notNull()->comment('Источник'),
            'name' => $this->string()->notNull()->defaultValue('')->unique()->comment('Название'),
            'path' => $this->string(1023)->unique()->comment('Путь хранения'),
            'extension' => $this->string(4)->comment('Расширение без точки'),
            'page_url' => $this->string(1023)->comment('Страница трека'),
            'download_url' => $this->string(1023)->unique()->comment('Ссылка на скачивание'),
            'style_1' => $this->string()->comment('Стиль 1'),
            'style_2' => $this->string()->comment('Стиль 2'),
            'duration' => $this->integer(9)->comment('Длительность в секундах'),
            'bpm' => $this->integer(3)->comment('Ударов в минуту'),
            'key' => $this->string(2)->comment('Тональность'),
            'is_mastered' => $this->boolean()->comment('Присутствие мастеринга'),
            'size' => $this->float()->comment('Размер в мегабайтах'),
            'publication_date' => $this->dateTime()->comment('Дата публикации'),
            'bitrate' => $this->integer(3)->comment('Битрейт'),
            'insert_date' => $this->dateTime()->comment('Дата добавления в таблицу'),
            'meta' => $this->json()->comment('Параметры, специфичные для интеграций')
        ], $tableOptions);
        
        $this->addForeignKey(
            'fk-authors-id', 
            'track', 
            'author_id', 
            'author', 
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-track-page_url',
            'track',
            'page_url'
        );
    }

    public function safeDown()
    {
        $this->dropIndex(
            'idx-track-page_url',
            'track'
        );

        $this->dropForeignKey(
            'fk-authors-id', 
            'track'
        );
        
        $this->dropTable('track');
    }
    
}
