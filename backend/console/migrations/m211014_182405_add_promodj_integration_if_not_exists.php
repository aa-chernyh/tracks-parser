<?php

use common\components\MetaMigration;
use common\integrations\common\BaseIntegration;
use common\models\Integration;

class m211014_182405_add_promodj_integration_if_not_exists extends MetaMigration
{

    public function safeUp()
    {
        if(!Integration::findOne(['id' => BaseIntegration::PROMODJ])) {
            $pdjIntegration = new Integration();
            $pdjIntegration->id = BaseIntegration::PROMODJ;
            $pdjIntegration->name = 'PromoDJ';
            $pdjIntegration->description = 'Cпециализированный сайт для диджеев, музыкантов и любителей электронной музыки, сочетающий в себе возможности файл-хостинга и социальной сети.';

            $pdjIntegration->insert();
        }
    }

    public function safeDown()
    {
        $pdjIntegration = Integration::findOne(['id' => BaseIntegration::PROMODJ]);

        if($pdjIntegration) {
            $pdjIntegration->delete();
        }
    }
}