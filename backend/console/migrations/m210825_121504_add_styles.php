<?php

use common\components\MetaMigration;
use common\models\Style;

class m210825_121504_add_styles extends MetaMigration
{

    public function up()
    {
        $this->addStyle(Style::ID_OTHER, 'Другой');
        $this->addStyle(Style::ID_METAL, 'Metal');
        $this->addStyle(Style::ID_CHANSON, 'Шансон');
        $this->addStyle(Style::ID_CLASSICAL, 'Классика');
        $this->addStyle(Style::ID_INDIE_POP, 'Indie Pop');
        $this->addStyle(Style::ID_ETHNIC, 'Этнический');
    }

    /**
     * @param string $id
     * @param string $name
     * @throws Throwable
     */
    private function addStyle(string $id, string $name): void
    {
        $style = new Style();
        $style->id = $id;
        $style->name = $name;
        $style->insert();
    }

    public function down()
    {
        Style::deleteAll(['id' => [
            Style::ID_OTHER,
            Style::ID_METAL,
            Style::ID_CHANSON,
            Style::ID_CLASSICAL,
            Style::ID_INDIE_POP,
        ]]);
    }

}