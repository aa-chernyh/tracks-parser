<?php

use common\components\MetaMigration;

class m210130_155405_alter_table_async_process extends MetaMigration
{

    const TABLE_NAME = 'async_process';

    public function up()
    {
        $this->alterColumn(
            self::TABLE_NAME,
            'key',
            $this->string()->comment('Ключ интеграции')
        );

        $this->addColumn(
            self::TABLE_NAME,
            'process_key',
            $this->string()->comment('Ключ процесса')
        );
    }

    public function down()
    {
        $this->dropColumn(
            self::TABLE_NAME,
            'process_key'
        );

        $this->alterColumn(
            self::TABLE_NAME,
            'key',
            $this->string()->notNull()->comment('Название процесса или ключ интеграции'),
        );
    }

}