<?php

use common\components\MetaMigration;
use common\models\Integration;
use yii\base\NotSupportedException;

class m210816_191554_create_table_log_integration extends MetaMigration
{

    private const TABLE_NAME = 'log_integration';

    /**
     * @return bool|void|null
     * @throws \yii\base\Exception
     * @throws NotSupportedException
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->comment('ID'),
            'request' => $this->text()->comment('Запрос'),
            'response' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->comment('Ответ'),
            'is_success' => $this->boolean()->comment('Успешно ли?'),
            'type' => $this->string()->comment('Тип запроса'),
            'request_url' => $this->string(1023)->comment('URL запроса'),
            'request_headers' => $this->json()->comment('Заголовки запроса'),
            'response_headers' => $this->json()->comment('Заголовки ответа'),
            'integration' => $this->string()->comment('Интеграция'),
        ]);

        $this->addForeignKey(
            'fk-' . self::TABLE_NAME . '-integration-integration-id',
            self::TABLE_NAME,
            'integration',
            Integration::tableName(),
            'id',
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}