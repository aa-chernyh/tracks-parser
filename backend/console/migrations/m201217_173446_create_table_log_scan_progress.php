<?php

use common\components\MetaMigration;

class m201217_173446_create_table_log_scan_progress extends MetaMigration
{

    public function safeUp()
    {
        $this->createTable('log_scan_progress', [
            'id' => $this->primaryKey(),
            'source' => $this->string()->notNull()->comment('Источник'),
            'status' => $this->string()->notNull()->comment('Статус'),
            'text' => $this->string(1023)->comment('Текст'),
            'date' => $this->dateTime()->comment('Дата'),
            'scan_position_number' => $this->integer(8)->comment('Позиция в очереди сканирования'),
            'scan_max_number' => $this->integer(8)->comment('Количество элементов в очереди сканирования'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('log_scan_progress');
    }

}
