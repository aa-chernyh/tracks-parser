<?php

use common\components\MetaMigration;
use common\models\Track;
use yii\db\Expression;

class m210816_184736_refactor_track_table extends MetaMigration
{

    private const SIZE_COEFF = 1024 * 1024;

    public function up()
    {
        $this->alterColumn(
            Track::tableName(),
            'duration',
            $this->float()->comment('Длительность в секундах')
        );

        $this->dropColumn(
            Track::tableName(),
            'is_mastered',
        );

        $this->alterColumn(
            Track::tableName(),
            'size',
            $this->integer()->comment('Размер в байтах')
        );

        $this->updateSizeUp();

        $this->renameColumn(
            Track::tableName(),
            'publication_date',
            'release_date',
        );

        $this->dropColumn(
            Track::tableName(),
            'is_marked',
        );
    }

    private function updateSizeUp(): void
    {
        /** @var Track $track */
        foreach (Track::find()->where(['is not', 'size', new Expression('null')])->all() as $track) {
            $track->updateAttributes(['size' => $track->size * self::SIZE_COEFF]);
        }
    }

    private function updateSizeDown(): void
    {
        /** @var Track $track */
        foreach (Track::find()->where(['is not', 'size', new Expression('null')])->all() as $track) {
            $track->updateAttributes(['size' => $track->size / self::SIZE_COEFF]);
        }
    }

    public function down()
    {
        $this->alterColumn(
            Track::tableName(),
            'duration',
            $this->integer()->comment('Длительность в секундах')
        );

        $this->addColumn(
            Track::tableName(),
            'is_mastered',
            $this->boolean()->comment('Присутствие мастеринга')
        );

        $this->alterColumn(
            Track::tableName(),
            'size',
            $this->double()->comment('Размер в мегабайтах')
        );

        $this->updateSizeDown();

        $this->renameColumn(
            Track::tableName(),
            'release_date',
            'publication_date',
        );

        $this->addColumn(
            Track::tableName(),
            'is_marked',
            $this->boolean()->comment('Избранное')
        );
    }

}