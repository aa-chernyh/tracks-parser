<?php

use common\components\MetaMigration;

class m201129_151401_add_integration_table extends MetaMigration
{

    public function safeUp()
    {
        $this->createTable('integration', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->unique()->notNull()->comment('Константа интеграции из integrations\common\BaseIntegration'),
            'name' => $this->string()->notNull()->comment('Название для отображения'),
            'description' => $this->string(1023)->comment('Описание'),
            'configuration' => $this->json()->comment('Конфигурация')
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('integration');
    }

}
