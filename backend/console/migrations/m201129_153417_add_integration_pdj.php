<?php

use yii\db\Migration;
use common\models\Integration;
use common\integrations\common\BaseIntegration;

class m201129_153417_add_integration_pdj extends Migration
{

    public function safeUp()
    {
        $pdjIntegration = new Integration();
        $pdjIntegration->key = BaseIntegration::PROMODJ;
        $pdjIntegration->name = 'PromoDJ';
        $pdjIntegration->description = 'Cпециализированный сайт для диджеев, музыкантов и любителей электронной музыки, сочетающий в себе возможности файл-хостинга и социальной сети.';

        $pdjIntegration->insert();
    }

    public function safeDown()
    {
        $pdjIntegration = Integration::findOne(['key' => BaseIntegration::PROMODJ]);

        if($pdjIntegration) {
            $pdjIntegration->delete();
        }
    }

}
