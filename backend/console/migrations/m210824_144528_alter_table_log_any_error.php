<?php

use common\components\MetaMigration;
use common\models\LogAnyError;

class m210824_144528_alter_table_log_any_error extends MetaMigration
{

    public function up()
    {
        $this->dropColumn(LogAnyError::tableName(), 'method');
    }

    public function down()
    {
        $this->addColumn(
            LogAnyError::tableName(),
            'method',
            $this->string(1023)->comment('Класс::Метод')
        );
    }

}