<?php

use \common\helpers\Utils;
use socket\Socket;

$db = require __DIR__ . '/../../common/config/db.php';
$common = require __DIR__ . '/../../common/config/common.php';

$config = [
    'id' => 'tracks-parser-console',
    'timeZone' => 'Europe/Moscow',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'console\\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'templateFile' => '@app/migrations/templates/Common.php',
        ],
    ],
    'bootstrap' => [
        'socket',
    ],
    'components' => [
        'db' => $db,
        'formatter' => [
            'dateFormat' => 'php:Y-m-d',
            'dateTimeFormat' => 'php:' . Utils::getDefaultDbDatetimeFormat(),
            'timeFormat' => 'php:H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'defaultTimeZone' => 'UTC',
            'timeZone' => 'Europe/Moscow',
            'locale' => 'ru-RU'
        ],
        'socket' => [
            'class' => Socket::class,
        ],
    ],
];

return array_merge_recursive($config, $common);
