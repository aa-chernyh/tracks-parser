<?php

use common\components\MetaMutex;
use queue\Queue;
use yii\base\Application;
use yii\redis\Connection;

/**
 * @property Queue $queue
 * @property MetaMutex $mutex
 * @property Connection $redis
 */
abstract class BaseApplication extends Application {}
