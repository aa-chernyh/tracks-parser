<?php

namespace api\v1\components;

use common\exceptions\LogicException;
use common\helpers\LogHelper;
use api\v1\forms\BaseApiResponse;
use Throwable;
use yii\web\Controller;
use Yii;

class ApiController extends Controller
{

    /**
     * @var array
     */
    protected $post;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->post = Yii::$app->request->post();
    }

    /**
     * @param string $id
     * @param array $params
     * @return BaseApiResponse
     * @throws Throwable
     */
    public function runAction($id, $params = [])
    {
        try {
            return new BaseApiResponse([
                'isSuccess' => true,
                'data' => parent::runAction($id, $params)
            ]);
        } catch (LogicException $exception) {
            return new BaseApiResponse([
                'isSuccess' => false,
                'error' => 'LogicException: ' . $exception->getMessage() . ' Stacktrace: ' . $exception->getTraceAsString()
            ]);
        } catch(Throwable $exception) {
            LogHelper::writeException($exception);

            return new BaseApiResponse([
                'isSuccess' => false,
                'error' => $exception->getMessage() . ' Stacktrace: ' . $exception->getTraceAsString()
            ]);
        }
    }
}