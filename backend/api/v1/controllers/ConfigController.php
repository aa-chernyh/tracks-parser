<?php

namespace api\v1\controllers;

use api\v1\components\ApiController;
use ClientInterface\Base\PhpDocReader\AnnotationException;
use common\exceptions\IntegrationConfigException;
use common\exceptions\ValidatorException;
use common\helpers\ValidationHelper;
use common\integrations\configs\AbstractConfig;
use common\managers\IntegrationManager;
use common\managers\PromodjManager;
use PromodjSDK\enums\Kinds;
use ReflectionException;
use Yii;
use yii\base\InvalidConfigException;

class ConfigController extends ApiController
{

    /**
     * @var PromodjManager
     */
    private $pdjManager;

    /**
     * @var IntegrationManager
     */
    private $integrationManager;

    /**
     * ConfigController constructor.
     * @param $id
     * @param $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->pdjManager = new PromodjManager();
        $this->integrationManager = new IntegrationManager();
    }

    /**
     * @return array
     */
    public function actionGetIntegrationList(): array
    {
        return $this->integrationManager->getList();
    }

    /**
     * @param string $integrationId
     * @return AbstractConfig
     * @throws IntegrationConfigException
     * @throws ValidatorException
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public function actionGetConfigByIntegrationId(string $integrationId): AbstractConfig
    {
        ValidationHelper::checkIntegrationId($integrationId);
        return $this->integrationManager->getIntegrationConfig($integrationId);
    }

    /**
     * @param string $integrationId
     * @return bool
     * @throws AnnotationException
     * @throws InvalidConfigException
     * @throws ReflectionException
     * @throws ValidatorException
     */
    public function actionSetConfigByIntegrationId(string $integrationId): bool
    {
        ValidationHelper::checkIntegrationId($integrationId);
        $configDTO = $this->integrationManager->convertArrayToIntegrationConfig(
            $integrationId,
            Yii::$app->request->post('config')
        );
        return $this->integrationManager->setIntegrationConfig($configDTO, $integrationId);
    }

    public function actionGetPromodjKindList(): array
    {
        return Kinds::LIST;
    }
}
