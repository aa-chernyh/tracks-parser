<?php

namespace api\v1\controllers;

use api\v1\components\ApiController;
use common\exceptions\LogicException;
use common\exceptions\ProjectException;
use common\exceptions\ValidatorException;
use common\helpers\ValidationHelper;
use api\v1\forms\player\TracklistFiltersForm;
use api\v1\forms\player\PlayerFilter;
use api\v1\forms\player\Tracklist;
use common\managers\PlayerFiltersManager;
use common\managers\PlayerManager;
use Yii;
use Yii\web\Response;

class PlayerController extends ApiController
{

    /**
     * @var PlayerManager
     */
    private $playerManager;

    /**
     * @var PlayerFiltersManager
     */
    private $playerFiltersManager;

    /**
     * PlayerController constructor.
     * @param $id
     * @param $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->playerManager = new PlayerManager();
        $this->playerFiltersManager = new PlayerFiltersManager();
    }

    /**
     * @return PlayerFilter[]
     */
    public function actionGetFilters(): array
    {
        return $this->playerFiltersManager->getAll();
    }

    /**
     * @return Tracklist
     * @throws LogicException
     */
    public function actionGetTracklist(): Tracklist
    {
        $form = new TracklistFiltersForm();
        $form->loadFromActiveFilterList($this->post);
        if(!$form->validate()) {
            $result = new Tracklist();
            $result->errors = $form->getFirstErrors();
            return $result;
        }

        return $this->playerManager->getTracklist($form);
    }

    /**
     * @param int $trackId
     * @return bool
     */
    public function actionCheckIsAllowDownloadTrack(int $trackId): bool
    {
        try {
            ValidationHelper::checkIsTrackExists($trackId);
            $this->playerManager->getTrackPathById($trackId);
            return true;
        } catch (ProjectException $exception) {
            return false;
        }
    }

    /**
     * @param int $trackId
     * @return Response
     * @throws ValidatorException
     * @throws LogicException
     */
    public function actionDownloadTrackById(int $trackId): Response
    {
        ValidationHelper::checkIsTrackExists($trackId);
        $trackPath = $this->playerManager->getTrackPathById($trackId);

        return Yii::$app->response->sendFile($trackPath);
    }

    /**
     * @param string $path
     * @return Response
     * @throws LogicException
     */
    public function actionDownloadAndRemoveFile(string $path): Response
    {
        if(!is_file($path)) {
            throw new LogicException('Файл не существует');
        }

        $response = Yii::$app->response->sendFile($path);
        unlink($path);
        return $response;
    }
}
