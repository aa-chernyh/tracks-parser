<?php

use common\helpers\Utils;
use yii\web\Response;

$db = require __DIR__ . '/../../../common/config/db.php';
$common = require __DIR__ . '/../../../common/config/common.php';

$config = [
    'id' => 'tracks-parser-api-v1',
    'timeZone' => 'Europe/Moscow',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\\v1\\controllers',
    'components' => [
        'request' => [
            'cookieValidationKey' => 'AlexBlack897',
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'format' => Response::FORMAT_JSON,
            'charset' => 'UTF-8',
            'on beforeSend' => function($event) {
                $event->sender->headers->add('Access-Control-Allow-Origin', '*');
                $event->sender->headers->add('Access-Control-Allow-Methods', '*');
                $event->sender->headers->add('Access-Control-Allow-Headers', '*');
                $event->sender->headers->add('Access-Control-Request-Headers', '*');
                $event->sender->headers->add('Access-Control-Allow-Credentials', 'true');
            },
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [],
        ],
        'formatter' => [
            'dateFormat' => 'php:Y-m-d',
            'dateTimeFormat' => 'php:' . Utils::getDefaultDbDatetimeFormat(),
            'timeFormat' => 'php:H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'defaultTimeZone' => 'UTC',
            'timeZone' => 'Europe/Moscow',
            'locale' => 'ru-RU'
        ],
    ]
];

return array_merge_recursive($config, $common);
