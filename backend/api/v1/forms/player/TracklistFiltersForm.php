<?php

namespace api\v1\forms\player;

use common\exceptions\LogicException;
use common\models\activeQuery\TrackActiveQuery;
use api\v1\forms\player\PlayerFilter;
use common\models\Track;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\Expression;

class TracklistFiltersForm extends Model
{

    /**
     * @var string
     */
    public $authorName;

    /**
     * @var string
     */
    public $trackName;

    /**
     * @var integer
     */
    public $trackYear;

    /**
     * @var boolean
     */
    public $isMastered;

    /**
     * @var boolean
     */
    public $isMarked;

    /**
     * @var string
     */
    public $duration;

    /**
     * @var string
     */
    public $publicationDate;

    /**
     * @var string
     */
    public $style;

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $source;

    /**
     * @var string
     */
    public $extension;

    /**
     * @var string
     */
    public $bpm;

    /**
     * @var string
     */
    public $size;

    public function rules(): array
    {
        return [
            [
                [
                    'authorName',
                    'trackName',
                    'duration',
                    'bpm',
                    'size',
                    'style',
                    'source',
                    'publicationDate'
                ],
                'string',
                'max' => 255
            ],
            [['isMastered', 'isMarked'], 'boolean'],
            [['trackYear'], 'integer'],
            [['key', 'extension'], 'string', 'max' => 4],
        ];
    }

    public function attributeLabels(): array
    {
        return PlayerFilter::FILTER_TO_NAME_MAP;
    }

    /**
     * @param array $activeFilterList
     */
    public function loadFromActiveFilterList(array $activeFilterList): void
    {
        foreach ($activeFilterList as $activeFilter) {
            if(property_exists($this, $activeFilter['key'])) {
                $this->{$activeFilter['key']} = $activeFilter['value'];
            }
        }
    }

    /**
     * @param bool $isLimited
     * @return TrackActiveQuery
     * @throws LogicException
     * @throws InvalidConfigException
     */
    public function getTracksQuery(bool $isLimited = true): TrackActiveQuery
    {
        $query = Track::find()
            ->whereAuthorName($this->authorName)
            ->whereTrackName($this->trackName)
            ->whereYear($this->trackYear)
            ->whereIsMastered($this->isMastered)
            ->whereIsMarked($this->isMarked)
            ->whereDurationFilterPosition($this->duration)
            ->wherePublicationDateFilterPosition($this->publicationDate)
            ->whereStyle($this->style)
            ->whereKey($this->key)
            ->whereSource($this->source)
            ->whereExtension($this->extension)
            ->whereBpmFilterPosition($this->bpm)
            ->whereSizeFilterPosition($this->size)
            ->hasDuration()
            ->hasPath()
            ->orderBy(['track.insert_date' => SORT_DESC]);

        if($isLimited) {
            $query->limit(TrackActiveQuery::TRACKS_COUNT_LIMIT);
        }

        return $query;
    }

    public function getTracksArchivePrefix(): string
    {
        $archivePrefix = '';
        foreach ($this->attributes as $key => $value) {
            if($value) {
                $archivePrefix .= $key . '-' . $value . '_';
            }
        }

        if(!$archivePrefix) {
            $archivePrefix = 'no-filters_';
        }

        return $archivePrefix;
    }

}