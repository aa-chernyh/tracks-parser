<?php

namespace api\v1\forms\player;

use common\models\Track;

class Tracklist
{
    /**
     * @var Track[]
     */
    public $tracks = [];

    /**
     * @var string[]
     */
    public $errors = [];

    /**
     * @var int
     */
    public $shownTracksCount = 0;

    /**
     * @var int
     */
    public $foundTracksCount = 0;
}