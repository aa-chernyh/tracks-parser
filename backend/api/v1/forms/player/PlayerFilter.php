<?php

namespace api\v1\forms\player;

class PlayerFilter
{

    const FILTER_AUTHOR_NAME = 'authorName';
    const FILTER_TRACK_NAME = 'trackName';
    const FILTER_TRACK_YEAR = 'trackYear';
    const FILTER_IS_MASTERED = 'isMastered';
    const FILTER_IS_MARKED = 'isMarked';
    const FILTER_DURATION = 'duration';
    const FILTER_PUBLICATION_DATE = 'publicationDate';
    const FILTER_STYLE = 'style';
    const FILTER_KEY = 'key';
    const FILTER_SOURCE = 'source';
    const FILTER_EXTENSION = 'extension';
    const FILTER_BPM = 'bpm';
    const FILTER_SIZE = 'size';

    const FILTER_NAME_AUTHOR_NAME = 'Автор';
    const FILTER_NAME_TRACK_NAME = 'Название';
    const FILTER_NAME_TRACK_YEAR = 'Год';
    const FILTER_NAME_IS_MASTERED = 'Мастеринг';
    const FILTER_NAME_IS_MARKED = 'Избранное';
    const FILTER_NAME_DURATION = 'Продолжительность';
    const FILTER_NAME_PUBLICATION_DATE = 'Дата публикации';
    const FILTER_NAME_STYLE = 'Стиль';
    const FILTER_NAME_KEY = 'Тональность';
    const FILTER_NAME_SOURCE = 'Источник';
    const FILTER_NAME_EXTENSION = 'Расширение';
    const FILTER_NAME_BPM = 'BPM';
    const FILTER_NAME_SIZE = 'Размер файла';

    const FILTER_TO_NAME_MAP = [
        self::FILTER_AUTHOR_NAME => self::FILTER_NAME_AUTHOR_NAME,
        self::FILTER_TRACK_NAME => self::FILTER_NAME_TRACK_NAME,
        self::FILTER_TRACK_YEAR => self::FILTER_NAME_TRACK_YEAR,
        self::FILTER_IS_MASTERED => self::FILTER_NAME_IS_MASTERED,
        self::FILTER_IS_MARKED => self::FILTER_NAME_IS_MARKED,
        self::FILTER_DURATION => self::FILTER_NAME_DURATION,
        self::FILTER_PUBLICATION_DATE => self::FILTER_NAME_PUBLICATION_DATE,
        self::FILTER_STYLE => self::FILTER_NAME_STYLE,
        self::FILTER_KEY => self::FILTER_NAME_KEY,
        self::FILTER_SOURCE => self::FILTER_NAME_SOURCE,
        self::FILTER_EXTENSION => self::FILTER_NAME_EXTENSION,
        self::FILTER_BPM => self::FILTER_NAME_BPM,
        self::FILTER_SIZE => self::FILTER_NAME_SIZE,
    ];

    const FILTER_TYPE_SELECT = 'select';
    const FILTER_TYPE_TEXT = 'text';
    const FILTER_TYPE_CHECKBOX = 'checkbox';

    const FILTER_TO_TYPE_MAP = [
        self::FILTER_AUTHOR_NAME => self::FILTER_TYPE_TEXT,
        self::FILTER_TRACK_NAME => self::FILTER_TYPE_TEXT,
        self::FILTER_TRACK_YEAR => self::FILTER_TYPE_TEXT,
        self::FILTER_IS_MASTERED => self::FILTER_TYPE_CHECKBOX,
        self::FILTER_IS_MARKED => self::FILTER_TYPE_CHECKBOX,
        self::FILTER_DURATION => self::FILTER_TYPE_SELECT,
        self::FILTER_PUBLICATION_DATE => self::FILTER_TYPE_SELECT,
        self::FILTER_STYLE => self::FILTER_TYPE_SELECT,
        self::FILTER_KEY => self::FILTER_TYPE_SELECT,
        self::FILTER_SOURCE => self::FILTER_TYPE_SELECT,
        self::FILTER_EXTENSION => self::FILTER_TYPE_SELECT,
        self::FILTER_BPM => self::FILTER_TYPE_SELECT,
        self::FILTER_SIZE => self::FILTER_TYPE_SELECT,
    ];

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $type;

    /**
     * @var PlayerFilterPosition[]|null
     */
    public $positions;

    /**
     * @param string $key
     * @param array|null $positionToNameMap
     * @return static
     */
    public static function getSelf(string $key, ?array $positionToNameMap = null): self
    {
        $result = new self();
        $result->key = $key;
        $result->name = self::FILTER_TO_NAME_MAP[$key];
        $result->type = self::FILTER_TO_TYPE_MAP[$key];

        if($result->type == self::FILTER_TYPE_SELECT) {
            foreach($positionToNameMap as $positionKey => $positionName) {
                $result->positions[] = PlayerFilterPosition::getSelf($positionKey, $positionName);
            }
        }

        return $result;
    }

}