<?php

namespace api\v1\forms\player;

class PlayerFilterPosition
{

    /**
     * Duration
     */
    const POSITION_DURATION_LESS_THEN_MINUTE = 'lessThenMinute';
    const POSITION_DURATION_LESS_THEN_10_MINUTES = 'lessThen10Minutes';
    const POSITION_DURATION_LESS_THEN_30_MINUTES = 'lessThen30Minutes';
    const POSITION_DURATION_LESS_THEN_HOUR = 'lessThenHour';
    const POSITION_DURATION_MORE_THEN_HOUR = 'moreThenHour';

    const POSITION_DURATION_NAME_LESS_THEN_MINUTE = 'Меньше минуты';
    const POSITION_DURATION_NAME_LESS_THEN_10_MINUTES = 'Меньше 10 минут';
    const POSITION_DURATION_NAME_LESS_THEN_30_MINUTES = 'Меньше 30 минут';
    const POSITION_DURATION_NAME_LESS_THEN_HOUR = 'Меньше часа';
    const POSITION_DURATION_NAME_MORE_THEN_HOUR = 'Больше часа';

    const DURATION_POSITIONS_MAP = [
        self::POSITION_DURATION_LESS_THEN_MINUTE => self::POSITION_DURATION_NAME_LESS_THEN_MINUTE,
        self::POSITION_DURATION_LESS_THEN_10_MINUTES => self::POSITION_DURATION_NAME_LESS_THEN_10_MINUTES,
        self::POSITION_DURATION_LESS_THEN_30_MINUTES => self::POSITION_DURATION_NAME_LESS_THEN_30_MINUTES,
        self::POSITION_DURATION_LESS_THEN_HOUR => self::POSITION_DURATION_NAME_LESS_THEN_HOUR,
        self::POSITION_DURATION_MORE_THEN_HOUR => self::POSITION_DURATION_NAME_MORE_THEN_HOUR,
    ];
    /**
     * /Duration
     */

    /**
     * Publication date
     */
    const POSITION_PUBLICATION_DATE_TODAY = 'today';
    const POSITION_PUBLICATION_DATE_LAST_3_DAYS = 'last3Days';
    const POSITION_PUBLICATION_DATE_LAST_WEEK = 'lastWeek';
    const POSITION_PUBLICATION_DATE_LAST_MONTH = 'lastMonth';
    const POSITION_PUBLICATION_DATE_LAST_3_MONTHS = 'last3Months';
    const POSITION_PUBLICATION_DATE_LAST_6_MONTHS = 'last6Months';
    const POSITION_PUBLICATION_DATE_LAST_YEAR = 'lastYear';

    const POSITION_PUBLICATION_DATE_NAME_TODAY = 'Сегодня';
    const POSITION_PUBLICATION_DATE_NAME_LAST_3_DAYS = 'Последние 3 дня';
    const POSITION_PUBLICATION_DATE_NAME_LAST_WEEK = 'Последняя неделя';
    const POSITION_PUBLICATION_DATE_NAME_LAST_MONTH = 'Последний месяц';
    const POSITION_PUBLICATION_DATE_NAME_LAST_3_MONTHS = 'Последние 3 месяца';
    const POSITION_PUBLICATION_DATE_NAME_LAST_6_MONTHS = 'Последние 6 месяцев';
    const POSITION_PUBLICATION_DATE_NAME_LAST_YEAR = 'Последний год';

    const PUBLICATION_DATE_POSITIONS_MAP = [
        self::POSITION_PUBLICATION_DATE_TODAY => self::POSITION_PUBLICATION_DATE_NAME_TODAY,
        self::POSITION_PUBLICATION_DATE_LAST_3_DAYS => self::POSITION_PUBLICATION_DATE_NAME_LAST_3_DAYS,
        self::POSITION_PUBLICATION_DATE_LAST_WEEK => self::POSITION_PUBLICATION_DATE_NAME_LAST_WEEK,
        self::POSITION_PUBLICATION_DATE_LAST_MONTH => self::POSITION_PUBLICATION_DATE_NAME_LAST_MONTH,
        self::POSITION_PUBLICATION_DATE_LAST_3_MONTHS => self::POSITION_PUBLICATION_DATE_NAME_LAST_3_MONTHS,
        self::POSITION_PUBLICATION_DATE_LAST_6_MONTHS => self::POSITION_PUBLICATION_DATE_NAME_LAST_6_MONTHS,
        self::POSITION_PUBLICATION_DATE_LAST_YEAR => self::POSITION_PUBLICATION_DATE_NAME_LAST_YEAR,
    ];
    /**
     * /Publication date
     */

    /**
     * Key
     */
    const POSITION_KEY_C = 'C';
    const POSITION_KEY_CM = 'Cm';
    const POSITION_KEY_DB = 'Db';
    const POSITION_KEY_DBM = 'Dbm';
    const POSITION_KEY_D = 'D';
    const POSITION_KEY_DM = 'Dm';
    const POSITION_KEY_EB = 'Eb';
    const POSITION_KEY_EBM = 'Ebm';
    const POSITION_KEY_E = 'E';
    const POSITION_KEY_EM = 'Em';
    const POSITION_KEY_F = 'F';
    const POSITION_KEY_FM = 'Fm';
    const POSITION_KEY_GB = 'Gb';
    const POSITION_KEY_GBM = 'Gbm';
    const POSITION_KEY_G = 'G';
    const POSITION_KEY_GM = 'Gm';
    const POSITION_KEY_AB = 'Ab';
    const POSITION_KEY_ABM = 'Abm';
    const POSITION_KEY_A = 'A';
    const POSITION_KEY_AM = 'Am';
    const POSITION_KEY_BB = 'Bb';
    const POSITION_KEY_BBM = 'Bbm';
    const POSITION_KEY_B = 'B';
    const POSITION_KEY_BM = 'Bm';

    const POSITION_KEY_NAME_C = 'C';
    const POSITION_KEY_NAME_CM = 'Cm';
    const POSITION_KEY_NAME_DB = 'Db';
    const POSITION_KEY_NAME_DBM = 'Dbm';
    const POSITION_KEY_NAME_D = 'D';
    const POSITION_KEY_NAME_DM = 'Dm';
    const POSITION_KEY_NAME_EB = 'Eb';
    const POSITION_KEY_NAME_EBM = 'Ebm';
    const POSITION_KEY_NAME_E = 'E';
    const POSITION_KEY_NAME_EM = 'Em';
    const POSITION_KEY_NAME_F = 'F';
    const POSITION_KEY_NAME_FM = 'Fm';
    const POSITION_KEY_NAME_GB = 'Gb';
    const POSITION_KEY_NAME_GBM = 'Gbm';
    const POSITION_KEY_NAME_G = 'G';
    const POSITION_KEY_NAME_GM = 'Gm';
    const POSITION_KEY_NAME_AB = 'Ab';
    const POSITION_KEY_NAME_ABM = 'Abm';
    const POSITION_KEY_NAME_A = 'A';
    const POSITION_KEY_NAME_AM = 'Am';
    const POSITION_KEY_NAME_BB = 'Bb';
    const POSITION_KEY_NAME_BBM = 'Bbm';
    const POSITION_KEY_NAME_B = 'B';
    const POSITION_KEY_NAME_BM = 'Bm';

    const KEY_POSITIONS_MAP = [
        self::POSITION_KEY_C => self::POSITION_KEY_NAME_C,
        self::POSITION_KEY_CM => self::POSITION_KEY_NAME_CM,
        self::POSITION_KEY_DB => self::POSITION_KEY_NAME_DB,
        self::POSITION_KEY_DBM => self::POSITION_KEY_NAME_DBM,
        self::POSITION_KEY_D => self::POSITION_KEY_NAME_D,
        self::POSITION_KEY_DM => self::POSITION_KEY_NAME_DM,
        self::POSITION_KEY_EB => self::POSITION_KEY_NAME_EB,
        self::POSITION_KEY_EBM => self::POSITION_KEY_NAME_EBM,
        self::POSITION_KEY_E => self::POSITION_KEY_NAME_E,
        self::POSITION_KEY_EM => self::POSITION_KEY_NAME_EM,
        self::POSITION_KEY_F => self::POSITION_KEY_NAME_F,
        self::POSITION_KEY_FM => self::POSITION_KEY_NAME_FM,
        self::POSITION_KEY_GB => self::POSITION_KEY_NAME_GB,
        self::POSITION_KEY_GBM => self::POSITION_KEY_NAME_GBM,
        self::POSITION_KEY_G => self::POSITION_KEY_NAME_G,
        self::POSITION_KEY_GM => self::POSITION_KEY_NAME_GM,
        self::POSITION_KEY_AB => self::POSITION_KEY_NAME_AB,
        self::POSITION_KEY_ABM => self::POSITION_KEY_NAME_ABM,
        self::POSITION_KEY_A => self::POSITION_KEY_NAME_A,
        self::POSITION_KEY_AM => self::POSITION_KEY_NAME_AM,
        self::POSITION_KEY_BB => self::POSITION_KEY_NAME_BB,
        self::POSITION_KEY_BBM => self::POSITION_KEY_NAME_BBM,
        self::POSITION_KEY_B => self::POSITION_KEY_NAME_B,
        self::POSITION_KEY_BM => self::POSITION_KEY_NAME_BM,
    ];
    /**
     * /Key
     */

    /**
     * BPM
     */
    const POSITION_BPM_0_90 = '0-90';
    const POSITION_BPM_91_119 = '91-119';
    const POSITION_BPM_120_132 = '120-132';
    const POSITION_BPM_133_160 = '133-160';
    const POSITION_BPM_161_MAX = '161-*';

    const POSITION_BPM_NAME_0_90 = '0-90';
    const POSITION_BPM_NAME_91_119 = '91-119';
    const POSITION_BPM_NAME_120_132 = '120-132';
    const POSITION_BPM_NAME_133_160 = '133-160';
    const POSITION_BPM_NAME_161_MAX = '161-∞';

    const BPM_POSITIONS_MAP = [
        self::POSITION_BPM_0_90 => self::POSITION_BPM_NAME_0_90,
        self::POSITION_BPM_91_119 => self::POSITION_BPM_NAME_91_119,
        self::POSITION_BPM_120_132 => self::POSITION_BPM_NAME_120_132,
        self::POSITION_BPM_133_160 => self::POSITION_BPM_NAME_133_160,
        self::POSITION_BPM_161_MAX => self::POSITION_BPM_NAME_161_MAX,
    ];
    /**
     * /BPM
     */

    /**
     * Size
     */
    const POSITION_SIZE_0_15 = '0-15';
    const POSITION_SIZE_16_60 = '16-60';
    const POSITION_SIZE_61_120 = '61-120';
    const POSITION_SIZE_121_MAX = '121-*';
    const POSITION_SIZE_0_60 = '0-60';

    const POSITION_SIZE_NAME_0_15 = '0-15 Мб';
    const POSITION_SIZE_NAME_16_60 = '16-60 Мб';
    const POSITION_SIZE_NAME_61_120 = '61-120 Мб';
    const POSITION_SIZE_NAME_121_MAX = '121-∞ Мб';
    const POSITION_SIZE_NAME_0_60 = '0-60 Мб';

    const SIZE_POSITIONS_MAP = [
        self::POSITION_SIZE_0_15 => self::POSITION_SIZE_NAME_0_15,
        self::POSITION_SIZE_16_60 => self::POSITION_SIZE_NAME_16_60,
        self::POSITION_SIZE_61_120 => self::POSITION_SIZE_NAME_61_120,
        self::POSITION_SIZE_121_MAX => self::POSITION_SIZE_NAME_121_MAX,
        self::POSITION_SIZE_0_60 => self::POSITION_SIZE_NAME_0_60,
    ];
    /**
     * /Size
     */

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $name;

    /**
     * @param string $key
     * @param string $name
     * @return static
     */
    public static function getSelf(string $key, string $name): self
    {
        $result = new self();
        $result->key = $key;
        $result->name = $name;

        return $result;
    }

}