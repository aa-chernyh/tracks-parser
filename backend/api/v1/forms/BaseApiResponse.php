<?php

namespace api\v1\forms;

use yii\base\BaseObject;

class BaseApiResponse extends BaseObject
{

    /**
     * @var boolean
     */
    public $isSuccess;

    /**
     * @var string
     */
    public $error;

    /**
     * @var mixed
     */
    public $data;

}