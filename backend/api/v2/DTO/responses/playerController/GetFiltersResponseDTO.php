<?php

namespace api\v2\DTO\responses\playerController;

use api\v2\DTO\responses\base\ApiResponseDataInterface;

class GetFiltersResponseDTO implements ApiResponseDataInterface
{
    public ?array $FiltersForm = null;
}