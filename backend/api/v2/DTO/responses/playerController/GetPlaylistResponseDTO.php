<?php

namespace api\v2\DTO\responses\playerController;

use api\v2\DTO\responses\base\ApiResponseDataInterface;
use api\v2\forms\TrackForm;
use common\forms\PaginationForm;

class GetPlaylistResponseDTO implements ApiResponseDataInterface
{

    /**
     * @var TrackForm[]
     */
    public ?array $tracks = [];
    public ?PaginationForm $PaginationForm = null;
}