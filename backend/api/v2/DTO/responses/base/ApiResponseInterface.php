<?php

namespace api\v2\DTO\responses\base;

interface ApiResponseInterface
{
    public function setError(string $message): void;
    public function setErrors(array $messages): void;
    public function setValidationErrors(array $errors, ?string $field = null): void;
    public function setData(ApiResponseDataInterface $dataDTO): void;
}