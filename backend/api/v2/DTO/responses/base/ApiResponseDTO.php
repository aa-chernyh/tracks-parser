<?php

namespace api\v2\DTO\responses\base;

class ApiResponseDTO implements ApiResponseInterface
{

    /** @var string[] */
    public array $errors = [];
    public bool $isSuccess = true;
    public array $validationErrors = [];
    public ?ApiResponseDataInterface $data = null;

    public function setError(string $message): void
    {
        $this->isSuccess = false;
        $this->errors[] = $message;
    }

    public function setErrors(array $messages): void
    {
        foreach ($messages as $message) {
            $this->setError($message);
        }
    }

    public function setValidationErrors(array $errors, ?string $field = null): void
    {
        $this->isSuccess = false;

        if($field) {
            $this->validationErrors[$field] = $errors;
        } else {
            $this->validationErrors = $errors;
        }
    }

    public function setData(ApiResponseDataInterface $dataDTO): void
    {
        $this->data = $dataDTO;
    }
}