<?php

namespace api\v2\DTO\responses\base;

use Throwable;

class ExceptionApiResponseDTO extends ApiResponseDTO
{
    public ?string $stacktrace = null;
    public ?string $file = null;
    public ?int $lineNumber = null;

    public function setException(Throwable $exception): void
    {
        $this->setError($exception->getMessage());
        if(YII_ENV == YII_ENV_DEV) {
            $this->stacktrace = $exception->getTraceAsString();
            $this->file = $exception->getFile();
            $this->lineNumber = $exception->getLine();
        }
    }
}