<?php

namespace api\v2\services;

use api\v2\forms\FiltersForm;
use api\v2\forms\inputs\SelectorOptionForm;
use api\v2\services\resultData\GetTrackQueryByFiltersFormServiceResult;
use common\enums\SortingOrderEnum;
use common\helpers\Utils;
use common\modelFacades\track\TrackFacade;
use common\models\activeQuery\TrackActiveQuery;
use common\models\Track;
use common\services\base\ServiceInterface;

class GetTrackQueryByFiltersFormService implements ServiceInterface
{

    private FiltersForm $form;

    public function __construct(FiltersForm $form)
    {
        $this->form = $form;
    }

    public function service(): GetTrackQueryByFiltersFormServiceResult
    {
        $result = new GetTrackQueryByFiltersFormServiceResult();
        $rawQuery = $this->getRawQuery();
        $result->query = $this->filterByFileParams($rawQuery);
        return $result;
    }

    private function getRawQuery(): TrackActiveQuery
    {
        $query = Track::find()->withAuthors()->withStyles()->withIntegration();

        $query->whereAuthorName($this->form->AuthorNameFilterForm->value);
        $query->whereTrackName($this->form->TrackNameFilterForm->value);
        $query->whereReleaseDateIn(
            $this->getDateInDbFormat($this->form->TrackReleaseDateFilterForm->valueFrom),
            $this->getDateInDbFormat($this->form->TrackReleaseDateFilterForm->valueTo),
        );
        $query->whereInsertDateIn(
            $this->getDateInDbFormat($this->form->TrackInsertDateFilterForm->valueFrom),
            $this->getDateInDbFormat($this->form->TrackInsertDateFilterForm->valueTo),
        );
        if($this->form->TrackDurationFilterForm->isUse) {
            $query->whereDurationIn(
                $this->form->TrackDurationFilterForm->RangeForm->min,
                $this->form->TrackDurationFilterForm->RangeForm->max
            );
        }
        if($this->form->TrackBpmFilterForm->isUse) {
            $query->whereBpmIn(
                $this->form->TrackBpmFilterForm->RangeForm->min,
                $this->form->TrackBpmFilterForm->RangeForm->max,
            );
        }
        if($this->form->TrackSizeFilterForm->isUse) {
            $query->whereSizeIn(
                $this->form->TrackSizeFilterForm->RangeForm->min,
                $this->form->TrackSizeFilterForm->RangeForm->max,
            );
        }
        $query->whereStyleIn($this->getOptionValues($this->form->StyleFilterForm->selected));
        $query->whereKeyIn($this->getOptionValues($this->form->TrackKeyFilterForm->selected));
        $query->whereExtensionIn($this->getOptionValues($this->form->TrackExtensionFilterForm->selected));
        $query->whereSourceIn($this->getOptionValues($this->form->SourceFilterForm->selected));

        $order = $this->form->SortingOrderFilterForm->value
            ? SortingOrderEnum::ID_DESC
            : SortingOrderEnum::ID_ASC;

        if($this->form->TrackSortByFilterForm->selected) {
            $query->order($this->getOptionValues($this->form->TrackSortByFilterForm->selected), $order);
        }

        return $query;
    }

    /**
     * @param SelectorOptionForm[] $optionForms
     * @return array
     */
    private function getOptionValues(?array $optionForms = []): array
    {
        return array_map(function(SelectorOptionForm $form) {
            return $form->value;
        }, $optionForms ?? []);
    }

    private function filterByFileParams(TrackActiveQuery $rawQuery): TrackActiveQuery
    {
        $query = clone $rawQuery;

        $allowedTrackIds = [];
        foreach ($rawQuery->select(['track.id', 'track.path'])->each() as $track) {
            $trackReadable = new TrackFacade($track);
            if($trackReadable->isDownloaded()) {
                $allowedTrackIds[] = $trackReadable->getId();
            }
        }

        return $query->whereIdIn($allowedTrackIds);
    }

    private function getDateInDbFormat(?string $date): ?string
    {
        if(!$date) {
            return null;
        }

        return Utils::getDateTimeByApiDateString($date)->format(Utils::getDefaultDbDatetimeFormat());
    }
}