<?php

namespace api\v2\services;

use api\v2\forms\FiltersForm;
use api\v2\forms\filters\types\BaseFilterForm;
use api\v2\forms\inputs\SelectorOptionForm;
use api\v2\services\resultData\SetDefaultFiltersDataServiceResult;
use common\collections\StylesCollection;
use common\enums\KeysEnum;
use common\enums\TrackOrderByEnum;
use common\modelFacades\integration\IntegrationReadableInterface;
use common\modelFacades\style\StyleReadableInterface;
use common\queryFacades\IntegrationQueryFacade;
use common\queryFacades\TrackQueryFacade;
use common\services\base\ServiceInterface;
use yii\base\InvalidConfigException;

class SetDefaultFiltersDataService implements ServiceInterface
{

    private FiltersForm $filters;
    private TrackQueryFacade $trackQueryFacade;
    private StylesCollection $stylesCollection;
    private IntegrationQueryFacade $integrationQueryFacade;

    public function __construct(FiltersForm $filters)
    {
        $this->filters = $filters;
        $this->trackQueryFacade = new TrackQueryFacade();
        $this->stylesCollection = StylesCollection::getInstance();
        $this->integrationQueryFacade = new IntegrationQueryFacade();
    }

    public function service(): SetDefaultFiltersDataServiceResult
    {
        $result = new SetDefaultFiltersDataServiceResult();

        try {
            $this->setAuthorName();
            $this->setTrackName();
            $this->setTrackReleaseDate();
            $this->setTrackInsertDate();
            $this->setTrackDuration();
            $this->setTrackBpm();
            $this->setTrackSize();
            $this->setStyle();
            $this->setTrackKey();
            $this->setTrackExtension();
            $this->setSource();
            $this->setTrackSortBy();
            $this->setSortingOrder();

            $this->setDefaults();

            $result->filters = $this->filters;
        } catch (InvalidConfigException $exception) {
            $result->setError($exception->getMessage());
        }

        return $result;
    }

    private function setAuthorName(): void
    {
        $this->filters->AuthorNameFilterForm->label = BaseFilterForm::NAME_AUTHOR_NAME;
    }

    private function setTrackName(): void
    {
        $this->filters->TrackNameFilterForm->label = BaseFilterForm::NAME_TRACK_NAME;
    }

    private function setTrackReleaseDate(): void
    {
        $this->filters->TrackReleaseDateFilterForm->labelFrom = BaseFilterForm::NAME_TRACK_RELEASE_DATE_FROM;
        $this->filters->TrackReleaseDateFilterForm->labelTo = BaseFilterForm::NAME_TRACK_RELEASE_DATE_TO;
    }

    private function setTrackInsertDate(): void
    {
        $this->filters->TrackInsertDateFilterForm->labelFrom = BaseFilterForm::NAME_TRACK_INSERT_DATE_FROM;
        $this->filters->TrackInsertDateFilterForm->labelTo = BaseFilterForm::NAME_TRACK_INSERT_DATE_TO;
    }

    /**
     * @throws InvalidConfigException
     */
    private function setTrackDuration(): void
    {
        $this->filters->TrackDurationFilterForm->label = BaseFilterForm::NAME_TRACK_DURATION;
        $this->filters->TrackDurationFilterForm->minValue = $this->trackQueryFacade->getMinimumDuration();
        $this->filters->TrackDurationFilterForm->maxValue = $this->trackQueryFacade->getMaximumDuration();
    }

    /**
     * @throws InvalidConfigException
     */
    private function setTrackBpm(): void
    {
        $this->filters->TrackBpmFilterForm->label = BaseFilterForm::NAME_TRACK_BPM;
        $this->filters->TrackBpmFilterForm->minValue = $this->trackQueryFacade->getMinimumBPM();
        $this->filters->TrackBpmFilterForm->maxValue = $this->trackQueryFacade->getMaximumBPM();
    }

    /**
     * @throws InvalidConfigException
     */
    private function setTrackSize(): void
    {
        $this->filters->TrackSizeFilterForm->label = BaseFilterForm::NAME_TRACK_SIZE;
        $this->filters->TrackSizeFilterForm->minValue = $this->trackQueryFacade->getMinimumSize();
        $this->filters->TrackSizeFilterForm->maxValue = $this->trackQueryFacade->getMaximumSize();
    }

    private function setStyle(): void
    {
        $this->filters->StyleFilterForm->label = BaseFilterForm::NAME_STYLE;
        $this->filters->StyleFilterForm->options = array_map(function(StyleReadableInterface $style) {
            return SelectorOptionForm::getSelf($style->getId(), $style->getName());
        }, $this->stylesCollection->all());
    }

    private function setTrackKey(): void
    {
        $this->filters->TrackKeyFilterForm->label = BaseFilterForm::NAME_TRACK_KEY;
        $options = [];
        foreach (KeysEnum::getIdToNameMapping() as $value => $label) {
            $options[] = SelectorOptionForm::getSelf($value, $label);
        }

        $this->filters->TrackKeyFilterForm->options = $options;
    }

    /**
     * @throws InvalidConfigException
     */
    private function setTrackExtension(): void
    {
        $this->filters->TrackExtensionFilterForm->label = BaseFilterForm::NAME_TRACK_EXTENSION;
        $this->filters->TrackExtensionFilterForm->options = array_map(function(string $extension) {
            return SelectorOptionForm::getSelf($extension, $extension);
        }, $this->trackQueryFacade->getExtensionsList());
    }

    private function setSource(): void
    {
        $this->filters->SourceFilterForm->label = BaseFilterForm::NAME_SOURCE;
        $this->filters->SourceFilterForm->options = array_map(function(IntegrationReadableInterface $integrationReadable) {
            return SelectorOptionForm::getSelf($integrationReadable->getId(), $integrationReadable->getName());
        }, $this->integrationQueryFacade->getAll());
    }

    private function setTrackSortBy(): void
    {
        $this->filters->TrackSortByFilterForm->label = BaseFilterForm::NAME_SORT_BY;

        $options = [];
        foreach (TrackOrderByEnum::getIdToNameMapping() as $value => $label) {
            $options[] = SelectorOptionForm::getSelf($value, $label);
        }
        $this->filters->TrackSortByFilterForm->options = $options;
    }

    private function setSortingOrder(): void
    {
        $this->filters->SortingOrderFilterForm->label = BaseFilterForm::NAME_SORTING_ORDER;
    }

    private function setDefaults(): void
    {
        $this->filters->TrackDurationFilterForm->RangeForm->min = $this->filters->TrackDurationFilterForm->minValue;
        $this->filters->TrackDurationFilterForm->RangeForm->max = $this->filters->TrackDurationFilterForm->maxValue;

        $this->filters->TrackBpmFilterForm->RangeForm->min = $this->filters->TrackBpmFilterForm->minValue;
        $this->filters->TrackBpmFilterForm->RangeForm->max = $this->filters->TrackBpmFilterForm->maxValue;

        $this->filters->TrackSizeFilterForm->RangeForm->min = $this->filters->TrackSizeFilterForm->minValue;
        $this->filters->TrackSizeFilterForm->RangeForm->max = $this->filters->TrackSizeFilterForm->maxValue;

        $this->filters->TrackSortByFilterForm->selected = [SelectorOptionForm::getSelf(
            TrackOrderByEnum::ID_INSERT_DATE,
            TrackOrderByEnum::NAME_INSERT_DATE
        )];
        $this->filters->SortingOrderFilterForm->value = true;
    }
}