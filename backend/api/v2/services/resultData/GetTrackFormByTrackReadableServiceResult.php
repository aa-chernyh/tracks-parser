<?php

namespace api\v2\services\resultData;

use api\v2\forms\TrackForm;
use common\services\base\BaseServiceResult;

class GetTrackFormByTrackReadableServiceResult extends BaseServiceResult
{
    public TrackForm $trackForm;
}