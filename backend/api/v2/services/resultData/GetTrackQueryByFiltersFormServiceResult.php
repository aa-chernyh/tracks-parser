<?php

namespace api\v2\services\resultData;

use common\models\activeQuery\TrackActiveQuery;
use common\services\base\BaseServiceResult;

class GetTrackQueryByFiltersFormServiceResult extends BaseServiceResult
{
    public TrackActiveQuery $query;
}