<?php

namespace api\v2\services\resultData;

use api\v2\forms\FiltersForm;
use common\services\base\BaseServiceResult;

class SetDefaultFiltersDataServiceResult extends BaseServiceResult
{
    public FiltersForm $filters;
}