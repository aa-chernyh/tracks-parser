<?php

namespace api\v2\services;

use api\v2\forms\AuthorForm;
use api\v2\forms\IntegrationForm;
use api\v2\forms\StyleForm;
use api\v2\forms\TrackForm;
use api\v2\services\resultData\GetTrackFormByTrackReadableServiceResult;
use common\helpers\Utils;
use common\modelFacades\author\AuthorReadableInterface;
use common\modelFacades\style\StyleReadableInterface;
use common\modelFacades\track\TrackReadableInterface;
use common\services\base\ServiceInterface;

class GetTrackFormByTrackReadableService implements ServiceInterface
{

    private TrackReadableInterface $trackReadable;

    public function __construct(TrackReadableInterface $trackReadable)
    {
        $this->trackReadable = $trackReadable;
    }

    public function service(): GetTrackFormByTrackReadableServiceResult
    {
        $result = new GetTrackFormByTrackReadableServiceResult();
        $result->trackForm = $this->getTrackForm();
        return $result;
    }

    private function getTrackForm(): TrackForm
    {
        $form = new TrackForm();
        $form->id = $this->trackReadable->getId();
        $form->name = $this->trackReadable->getName();
        $form->extension = $this->trackReadable->getExtension();
        $form->pageUrl = $this->trackReadable->getPageUrl();
        $form->downloadUrl = $this->trackReadable->getDownloadUrl();
        $form->duration = $this->trackReadable->getDuration();
        $form->bpm = $this->trackReadable->getBPM();
        $form->key = $this->trackReadable->getSize();
        $form->size = $this->trackReadable->getSize();
        $form->releaseDate = $this->trackReadable->getReleaseDate()->format(Utils::getDefaultApiDateFormat());
        $form->bitrate = $this->trackReadable->getBitrate();
        $form->insertDate = $this->trackReadable->getInsertDate()->format(Utils::getDefaultApiDateFormat());

        $form->IntegrationForm = $this->getIntegrationForm();
        $form->authors = $this->getAuthorForms();
        $form->styles = $this->getStyleForms();

        return $form;
    }

    private function getIntegrationForm(): IntegrationForm
    {
        $integration = $this->trackReadable->getIntegration();

        $form = new IntegrationForm();
        $form->id = $integration->getId();
        $form->name = $integration->getName();
        $form->description = $integration->getDescription();

        return $form;
    }

    /**
     * @return AuthorForm[]
     */
    private function getAuthorForms(): array
    {
        return array_map(function (AuthorReadableInterface $author) {
            $form = new AuthorForm();
            $form->id = $author->getId();
            $form->name = $author->getName();
            $form->url = $author->getUrl();
            $form->realName = $author->getRealName();

            return $form;
        }, $this->trackReadable->getAuthors());
    }

    /**
     * @return StyleForm[]
     */
    private function getStyleForms(): array
    {
        return array_map(function (StyleReadableInterface $style) {
            $form = new StyleForm();
            $form->value = $style->getId();
            $form->label = $style->getName();

            return $form;
        }, $this->trackReadable->getStyles());
    }
}