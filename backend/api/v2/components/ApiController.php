<?php

namespace api\v2\components;

use api\v2\DTO\responses\base\ApiResponseInterface;
use api\v2\DTO\responses\base\ExceptionApiResponseDTO;
use common\exceptions\ProjectException;
use common\helpers\LogHelper;
use \Throwable;
use yii\web\Controller;

class ApiController extends Controller
{

    /**
     * @param string $id
     * @param array $params
     * @return mixed
     * @throws Throwable
     */
    public function runAction($id, $params = [])
    {
        try {
            return parent::runAction($id, $params);
        } catch (ProjectException $projectException) {
            return $this->getExceptionResponse($projectException);
        } catch(Throwable $exception) {
            LogHelper::writeException($exception);
            return $this->getExceptionResponse($exception);
        }
    }

    private function getExceptionResponse(Throwable $exception): ApiResponseInterface
    {
        $result = new ExceptionApiResponseDTO();
        $result->setException($exception);
        return $result;
    }
}