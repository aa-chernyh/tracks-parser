<?php

namespace api\v2\controllers;

use api\v2\components\ApiController;
use api\v2\DTO\responses\playerController\GetPlaylistResponseDTO;
use api\v2\forms\FiltersForm;
use api\v2\DTO\responses\base\ApiResponseDTO;
use api\v2\DTO\responses\base\ApiResponseInterface;
use api\v2\DTO\responses\playerController\GetFiltersResponseDTO;
use api\v2\forms\GetPlaylistForm;
use api\v2\services\GetTrackFormByTrackReadableService;
use api\v2\services\GetTrackQueryByFiltersFormService;
use api\v2\services\SetDefaultFiltersDataService;
use common\components\QueryPaginationDecorator;
use common\exceptions\LogicException;
use common\modelFacades\track\TrackReadableInterface;
use common\queryFacades\TrackQueryFacade;
use Yii;

class PlayerController extends ApiController
{

    public function actionGetFilters(): ApiResponseInterface
    {
        $response = new ApiResponseDTO();

        $service = new SetDefaultFiltersDataService(new FiltersForm());
        $serviceResult = $service->service();

        if($serviceResult->getIsSuccess()) {
            $getFiltersResponseDTO = new GetFiltersResponseDTO();
            $getFiltersResponseDTO->FiltersForm = $serviceResult->filters->toFormArray();
            $response->setData($getFiltersResponseDTO);
        } else {
            $response->setErrors($serviceResult->getErrors());
        }

        return $response;
    }

    public function actionGetPlaylist(): ApiResponseInterface
    {
        $response = new ApiResponseDTO();

        $form = new GetPlaylistForm();
        $form->load(Yii::$app->request->post(), '');
        if(!$form->validate()) {
            $response->setValidationErrors($form->getErrorsAsInputName());
            return $response;
        }

        $service = new GetTrackQueryByFiltersFormService($form->FiltersForm);
        $query = $service->service()->query;
        $queryPaginationDecorator = new QueryPaginationDecorator($query, $form->PaginationForm);

        $tracks = array_map(function(TrackReadableInterface $trackReadable) {
            $service = new GetTrackFormByTrackReadableService($trackReadable);
            return $service->service()->trackForm->toFormArray();
        }, (new TrackQueryFacade())->getByQuery($queryPaginationDecorator));

        $responseData = new GetPlaylistResponseDTO();
        $responseData->tracks = $tracks;
        $responseData->PaginationForm = $form->PaginationForm;
        $responseData->PaginationForm->totalCount = $queryPaginationDecorator->totalCount('track.id');

        $response->setData($responseData);
        return $response;
    }

    /**
     * @param int $trackId
     * @throws LogicException
     */
    public function actionDownloadTrackById(int $trackId): void
    {
        $path = (new TrackQueryFacade())->getPathById($trackId);
        if(!$path) {
            throw new LogicException("Нет трека с id {$trackId}");
        }

        Yii::$app->response->sendFile($path);
    }
}