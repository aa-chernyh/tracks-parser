<?php

namespace api\v2\forms;

use common\components\CompositeForm;
use common\forms\PaginationForm;

/**
 * @property FiltersForm $FiltersForm
 * @property PaginationForm $PaginationForm
 */
class GetPlaylistForm extends CompositeForm
{

    public function init()
    {
        $this->FiltersForm = new FiltersForm();
        $this->PaginationForm = new PaginationForm();
    }

    public function internalForms(): array
    {
        return ['FiltersForm', 'PaginationForm'];
    }
}