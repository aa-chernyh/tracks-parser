<?php

namespace api\v2\forms;

use common\components\CompositeForm;
use common\enums\KeysEnum;
use common\helpers\Utils;
use common\models\Integration;

/**
 * @property IntegrationForm $IntegrationForm
 * @property AuthorForm[] $authors
 * @property StyleForm[] $styles
 */
class TrackForm extends CompositeForm
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $extension = null;
    public ?string $pageUrl = null;
    public ?string $downloadUrl = null;
    public ?float $duration = null;
    public ?int $bpm = null;
    public ?string $key = null;
    public ?int $size = null;
    public ?string $releaseDate = null;
    public ?int $bitrate = null;
    public ?string $insertDate = null;

    public function internalForms(): array
    {
        return ['IntegrationForm', 'authors', 'styles'];
    }

    public function init(): void
    {
        $this->IntegrationForm = new IntegrationForm();
        $this->authors = [];
        $this->styles = [];
    }

    public function load($data, $formName = null): bool
    {
        $this->authors = [new AuthorForm()];
        $this->styles = [new StyleForm()];

        $result = parent::load($data, $formName);

        $this->unsetInnerFormArray('authors');
        $this->unsetInnerFormArray('styles');

        return $result;
    }

    public function rules(): array
    {
        return [
            [
                [
                    'id',
                    'duration',
                    'bpm',
                    'size',
                    'bitrate',
                ],
                'safe',
            ],
            [
                [
                    'integration',
                    'name',
                ],
                'string',
                'max' => 255,
            ],
            [
                [
                    'path',
                    'pageUrl',
                    'downloadUrl',
                ],
                'string',
                'max' => 1023,
            ],
            [
                [
                    'extension',
                    'key',
                ],
                'string',
                'max' => 4
            ],
            [
                [
                    'releaseDate',
                    'insertDate',
                ],
                'datetime',
                'format' => 'php:' . Utils::getDefaultDbDatetimeFormat(),
            ],
            [
                [
                    'pageUrl',
                    'downloadUrl',
                ],
                'url',
            ],
            ['integration', 'in', 'range' => Integration::ID_LIST],
            ['key', 'in', 'range' => KeysEnum::getIdList()],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'integration' => 'Интеграция',
            'name' => 'Название',
            'path' => 'Путь',
            'extension' => 'Расширение',
            'pageUrl' => 'Страница',
            'downloadUrl' => 'Ссылка на скачивание',
            'duration' => 'Продолжительность',
            'bpm' => 'BPM',
            'key' => 'Тональность',
            'size' => 'Размер в байтах',
            'releaseDate' => 'Дата публикации',
            'bitrate' => 'Битрейт',
            'insertDate' => 'Дата добавления',
        ];
    }
}