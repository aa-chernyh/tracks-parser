<?php

namespace api\v2\forms\inputs;

use yii\base\Model;

class RangeForm extends Model
{

    private const ERROR_MAX_LESS_THEN_MIN = 'Максимальное значение не может быть меньше минимального';

    public ?int $min = null;
    public ?int $max = null;

    public function rules(): array
    {
        return [
            [['min', 'max'], function(string $attribute) {
                if($this->min && $this->max && ($this->max < $this->min)) {
                    $this->addError($attribute, self::ERROR_MAX_LESS_THEN_MIN);
                }
            }],
        ];
    }
}