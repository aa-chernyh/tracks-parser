<?php

namespace api\v2\forms\inputs;

use yii\base\Model;

class SelectorOptionForm extends Model
{
    public ?string $value = null;
    public ?string $label = null;

    public static function getSelf(string $value, string $label): self
    {
        $self = new self();
        $self->value = $value;
        $self->label = $label;

        return $self;
    }

    public function rules(): array
    {
        return [
            [['value', 'label'], 'required'],
        ];
    }
}