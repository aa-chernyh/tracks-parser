<?php

namespace api\v2\forms;

use common\models\Integration;
use yii\base\Model;

class IntegrationForm extends Model
{
    public ?string $id = null;
    public ?string $name = null;
    public ?string $description = null;

    public function rules(): array
    {
        return [
            [
                [
                    'id',
                    'name',
                ],
                'string',
                'max' => 255,
            ],
            ['description', 'string', 'max' => 1023],
            ['id', 'in', 'range' => Integration::ID_LIST],
        ];
    }
}