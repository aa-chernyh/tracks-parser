<?php

namespace api\v2\forms;

use api\v2\forms\filters\AuthorNameFilterForm;
use api\v2\forms\filters\TrackSortByFilterForm;
use api\v2\forms\filters\SortingOrderFilterForm;
use api\v2\forms\filters\SourceFilterForm;
use api\v2\forms\filters\StyleFilterForm;
use api\v2\forms\filters\TrackBpmFilterForm;
use api\v2\forms\filters\TrackDurationFilterForm;
use api\v2\forms\filters\TrackExtensionFilterForm;
use api\v2\forms\filters\TrackInsertDateFilterForm;
use api\v2\forms\filters\TrackKeyFilterForm;
use api\v2\forms\filters\TrackNameFilterForm;
use api\v2\forms\filters\TrackReleaseDateFilterForm;
use api\v2\forms\filters\TrackSizeFilterForm;
use common\components\CompositeForm;

/**
 * @property AuthorNameFilterForm $AuthorNameFilterForm
 * @property TrackNameFilterForm $TrackNameFilterForm
 * @property TrackReleaseDateFilterForm $TrackReleaseDateFilterForm
 * @property TrackInsertDateFilterForm $TrackInsertDateFilterForm
 * @property TrackDurationFilterForm $TrackDurationFilterForm
 * @property TrackBpmFilterForm $TrackBpmFilterForm
 * @property TrackSizeFilterForm $TrackSizeFilterForm
 * @property StyleFilterForm $StyleFilterForm
 * @property TrackKeyFilterForm $TrackKeyFilterForm
 * @property TrackExtensionFilterForm $TrackExtensionFilterForm
 * @property SourceFilterForm $SourceFilterForm
 * @property TrackSortByFilterForm $TrackSortByFilterForm
 * @property SortingOrderFilterForm $SortingOrderFilterForm
 */
class FiltersForm extends CompositeForm
{

    public function init() {
        $this->AuthorNameFilterForm = new AuthorNameFilterForm();
        $this->TrackNameFilterForm = new TrackNameFilterForm();
        $this->TrackReleaseDateFilterForm = new TrackReleaseDateFilterForm();
        $this->TrackInsertDateFilterForm = new TrackInsertDateFilterForm();
        $this->TrackDurationFilterForm = new TrackDurationFilterForm();
        $this->TrackBpmFilterForm = new TrackBpmFilterForm();
        $this->TrackSizeFilterForm = new TrackSizeFilterForm();
        $this->StyleFilterForm = new StyleFilterForm();
        $this->TrackKeyFilterForm = new TrackKeyFilterForm();
        $this->TrackExtensionFilterForm = new TrackExtensionFilterForm();
        $this->SourceFilterForm = new SourceFilterForm();
        $this->TrackSortByFilterForm = new TrackSortByFilterForm();
        $this->SortingOrderFilterForm = new SortingOrderFilterForm();
    }

    public function internalForms(): array
    {
        return [
            'AuthorNameFilterForm',
            'TrackNameFilterForm',
            'TrackReleaseDateFilterForm',
            'TrackInsertDateFilterForm',
            'TrackDurationFilterForm',
            'TrackBpmFilterForm',
            'TrackSizeFilterForm',
            'StyleFilterForm',
            'TrackKeyFilterForm',
            'TrackExtensionFilterForm',
            'SourceFilterForm',
            'TrackSortByFilterForm',
            'SortingOrderFilterForm',
        ];
    }
}