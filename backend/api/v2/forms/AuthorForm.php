<?php

namespace api\v2\forms;

use yii\base\Model;

class AuthorForm extends Model
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $url = null;
    public ?string $realName = null;

    public function rules(): array
    {
        return [
            [
                [
                    'name',
                    'realName',
                ],
                'string',
                'max' => 255,
            ],
            ['id', 'safe'],
            ['url', 'string', 'max' => 1023],
            ['url', 'url'],
        ];
    }
}