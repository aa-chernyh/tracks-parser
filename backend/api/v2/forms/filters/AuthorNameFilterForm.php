<?php

namespace api\v2\forms\filters;

use api\v2\forms\filters\types\BaseFilterForm;
use api\v2\forms\filters\types\InputFilterForm;

class AuthorNameFilterForm extends InputFilterForm
{

    public function attributeLabels(): array
    {
        return [
            'value' => BaseFilterForm::NAME_AUTHOR_NAME,
        ];
    }
}