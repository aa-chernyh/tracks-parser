<?php

namespace api\v2\forms\filters;

use api\v2\forms\filters\types\BaseFilterForm;
use api\v2\forms\filters\types\DateRangeFilterForm;

class TrackReleaseDateFilterForm extends DateRangeFilterForm
{
    public function attributeLabels(): array
    {
        return [
            'valueFrom' => BaseFilterForm::NAME_TRACK_RELEASE_DATE_FROM,
            'valueTo' => BaseFilterForm::NAME_TRACK_RELEASE_DATE_TO,
        ];
    }
}