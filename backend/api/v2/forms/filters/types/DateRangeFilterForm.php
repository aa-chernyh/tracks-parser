<?php

namespace api\v2\forms\filters\types;

use common\helpers\Utils;
use DateTime;

class DateRangeFilterForm extends BaseFilterForm
{

    private const ERROR_DATE_LESS_THEN_1900 = 'Год не может быть меньше 1900';
    private const ERROR_DATE_MORE_THEN_TODAY = 'Дата не может быть больше, чем сегодня';
    private const ERROR_DATE_FROM_MORE_THEN_DATE_TO = '"Дата от" не может быть больше "Даты до"';

    public string $labelFrom = '';
    public string $labelTo = '';
    public string $valueFrom = '';
    public string $valueTo = '';

    public function init()
    {
        $this->type = self::TYPE_DATE_RANGE;
    }

    public function rules(): array
    {
        return array_merge([
            [['labelFrom', 'labelTo'], 'safe'],
            [
                ['valueFrom', 'valueTo'],
                'datetime',
                'format' => 'php:' . Utils::getDefaultApiDateFormat()
            ],
            ['valueFrom', function(string $attribute) {
                if($this->valueFrom && Utils::getDateTimeByApiDateString($this->valueFrom)->format('Y') < 1900) {
                    $this->addError($attribute, self::ERROR_DATE_LESS_THEN_1900);
                }
            }],
            ['valueTo', function(string $attribute) {
                if($this->valueTo) {
                    $dateTo = Utils::getDateTimeByApiDateString($this->valueTo);
                    $tomorrow = (new DateTime('+1 day'))->setTime(0, 0);
                    if($dateTo->getTimestamp() > $tomorrow->getTimestamp()) {
                        $this->addError($attribute, self::ERROR_DATE_MORE_THEN_TODAY);
                    }
                }
            }],
            [['valueFrom', 'valueTo'], function(string $attribute) {
                if($this->valueTo && $this->valueFrom) {
                    $dateFrom = Utils::getDateTimeByApiDateString($this->valueFrom);
                    $dateTo = Utils::getDateTimeByApiDateString($this->valueTo);
                    if($dateFrom->getTimestamp() > $dateTo->getTimestamp()) {
                        $this->addError($attribute, self::ERROR_DATE_FROM_MORE_THEN_DATE_TO);
                    }
                }
            }],
        ], parent::rules());
    }
}