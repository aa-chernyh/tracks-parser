<?php

namespace api\v2\forms\filters\types;

class InputFilterForm extends BaseFilterForm
{

    public string $label = '';
    public string $value = '';

    public function init()
    {
        $this->type = self::TYPE_INPUT;
    }

    public function rules(): array
    {
        return array_merge([
            [['label', 'value'], 'safe'],
        ], parent::rules());
    }
}