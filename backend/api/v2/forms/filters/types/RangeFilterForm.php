<?php

namespace api\v2\forms\filters\types;

use api\v2\forms\inputs\RangeForm;

/**
 * @property RangeForm $RangeForm
 */
class RangeFilterForm extends BaseFilterForm
{

    private const ERROR_RANGE_MIN_LESS_THEN_MIN = 'Минимальное значение диапазона не может быть меньше минимально возможного';
    private const ERROR_RANGE_MAX_MORE_THEN_MAX = 'Максимальное значение диапазона не может быть больше максимально возможного';
    private const ERROR_MIN_MORE_THEN_MAX = 'Минимально возможное значение не может быть больше максимального';

    public string $label = '';
    public bool $isUse = false;
    public int $minValue = 0;
    public int $maxValue = 0;

    public function init()
    {
        $this->type = self::TYPE_RANGE;
        $this->RangeForm = new RangeForm();
    }

    public function rules(): array
    {
        return array_merge([
            [['label', 'isUse'], 'safe'],
            [['minValue'], function(string $attribute) {
                if($this->minValue && $this->RangeForm->min && ($this->RangeForm->min < $this->minValue)) {
                    $this->addError($attribute, self::ERROR_RANGE_MIN_LESS_THEN_MIN);
                }
            }],
            [['maxValue'], function(string $attribute) {
                if($this->maxValue && $this->RangeForm->max && ($this->RangeForm->max > $this->maxValue)) {
                    $this->addError($attribute, self::ERROR_RANGE_MAX_MORE_THEN_MAX);
                }
            }],
            [['minValue', 'maxValue'], function(string $attribute) {
                if($this->minValue && $this->maxValue && ($this->minValue > $this->maxValue)) {
                    $this->addError($attribute, self::ERROR_MIN_MORE_THEN_MAX);
                }
            }],
        ], parent::rules());
    }

    public function internalForms(): array
    {
        return ['RangeForm'];
    }
}