<?php

namespace api\v2\forms\filters\types;

use common\components\CompositeForm;

abstract class BaseFilterForm extends CompositeForm
{

    public const TYPE_INPUT = 'input';
    public const TYPE_DATE_RANGE = 'dateRange';
    public const TYPE_RANGE = 'range';
    public const TYPE_MULTIPLE_SELECTOR = 'multipleSelector';
    public const TYPE_CHECKBOX = 'checkbox';

    public const NAME_AUTHOR_NAME = 'Автор';
    public const NAME_TRACK_NAME = 'Название';
    public const NAME_TRACK_DURATION = 'Время';
    public const NAME_TRACK_RELEASE_DATE_FROM = 'Публикация от';
    public const NAME_TRACK_RELEASE_DATE_TO = 'Публикация до';
    public const NAME_TRACK_INSERT_DATE_FROM = 'Добавление от';
    public const NAME_TRACK_INSERT_DATE_TO = 'Добавление до';
    public const NAME_STYLE = 'Стиль';
    public const NAME_TRACK_KEY = 'Тональность';
    public const NAME_SOURCE = 'Источник';
    public const NAME_TRACK_EXTENSION = 'Расширение';
    public const NAME_TRACK_BPM = 'BPM';
    public const NAME_TRACK_SIZE = 'Размер';
    public const NAME_SORT_BY = 'Сортировать по';
    public const NAME_SORTING_ORDER = 'Обратная сортировка';

    public string $type;

    public function rules(): array
    {
        return [
            ['type', 'safe']
        ];
    }

    public function internalForms(): array
    {
        return [];
    }
}