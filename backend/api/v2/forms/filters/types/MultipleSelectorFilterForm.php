<?php

namespace api\v2\forms\filters\types;

use api\v2\forms\inputs\SelectorOptionForm;

/**
 * @property SelectorOptionForm[] $options
 * @property SelectorOptionForm[] $selected
 */
class MultipleSelectorFilterForm extends BaseFilterForm
{

    public string $label = '';

    public function init()
    {
        $this->type = self::TYPE_MULTIPLE_SELECTOR;
        $this->options = [];
        $this->selected = [];
    }

    public function load($data, $formName = null): bool
    {
        $this->options = [new SelectorOptionForm()];
        $this->selected = [new SelectorOptionForm()];

        $result = parent::load($data, $formName);

        $this->unsetInnerFormArray('options');
        $this->unsetInnerFormArray('selected');

        return $result;
    }

    public function rules(): array
    {
        return array_merge([
            [['label'], 'safe'],
        ], parent::rules());
    }

    public function internalForms(): array
    {
        return ['options', 'selected'];
    }
}