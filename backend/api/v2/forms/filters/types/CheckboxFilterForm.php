<?php

namespace api\v2\forms\filters\types;

class CheckboxFilterForm extends BaseFilterForm
{

    public ?string $label = null;
    public bool $value = false;

    public function init()
    {
        $this->type = self::TYPE_CHECKBOX;
    }

    public function rules(): array
    {
        return array_merge([
            [['label', 'value'], 'safe'],
        ], parent::rules());
    }
}