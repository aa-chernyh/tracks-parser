<?php

namespace api\v2\forms\filters;

use api\v2\forms\filters\types\BaseFilterForm;
use api\v2\forms\filters\types\DateRangeFilterForm;

class TrackInsertDateFilterForm extends DateRangeFilterForm
{
    public function attributeLabels(): array
    {
        return [
            'valueFrom' => BaseFilterForm::NAME_TRACK_INSERT_DATE_FROM,
            'valueTo' => BaseFilterForm::NAME_TRACK_INSERT_DATE_TO,
        ];
    }
}