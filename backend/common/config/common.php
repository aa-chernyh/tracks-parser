<?php

use common\components\MetaMutex;

return [
    'language' => 'ru-RU',
    'aliases' => [
        '@projectRoot' => '/var/www/tracks-parser',
        '@tracksRoot' => '/var/www/files',
        '@runtime' => '/var/www/files/runtime',
    ],
    'bootstrap' => [
        'queue',
    ],
    'components' => [
        'queue' => [
            'class' => queue\Queue::class,
            'pathToYii' => '/var/www/tracks-parser/backend/console/yii',
            'attempts' => 3,
            'maxSecondsToCompleteTask' => 3600,
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'redis',
            'port' => 6379,
            'database' => 0,
        ],
        'mutex' => [
            'class' => MetaMutex::class,
            'redis' => 'redis',
        ],
    ],
];