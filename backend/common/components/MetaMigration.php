<?php

namespace common\components;

use yii\db\Migration;

/** @var $this Migration */
class MetaMigration extends Migration
{

    /**
     * @param string $table
     * @param array $columns
     * @param null $options
     */
    public function createTable($table, $columns, $options = null)
    {
        parent::createTable($table, $columns, $options ?: $this->getCharsetTableOptions());
    }

    /**
     * @return string
     */
    private function getCharsetTableOptions(): string
    {
        $tableOptions = '';
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        return $tableOptions;
    }

}