<?php

namespace common\components;

use common\exceptions\LogicException;
use common\helpers\LogHelper;
use Throwable;
use yii\console\Controller;

class MetaConsoleController extends Controller
{

    /**
     * @param string $id
     * @param array $params
     * @return mixed|null
     * @throws Throwable
     */
    public function runAction($id, $params = [])
    {
        try {
            return parent::runAction($id, $params);
        } catch (LogicException $exception) {
            //do nothing
        } catch(Throwable $exception) {
            LogHelper::writeException($exception);
        }
    }
}