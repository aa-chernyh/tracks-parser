<?php

namespace common\components;

use common\exceptions\LogicException;
use yii\redis\Mutex;

class MetaMutex extends Mutex
{

    private const DEFAULT_TIMEOUT = 604800; // Временное значение для первого сканирования (7 дней). todo заменить на 10800 (3 часа)

    /**
     * @param string $name
     * @param int $timeout
     * @throws LogicException
     */
    public function lock(string $name, int $timeout = self::DEFAULT_TIMEOUT): void
    {
        //Если не установить expire, возникнут проблемы.
        //Спустя 30 секунд lock будет перехвачен другим потоком из-за кода пакета Mutex
        $this->expire = $timeout;

        if (!$this->acquireLock($name, $timeout)) {
            throw new LogicException('Не удалось получить блокировку ' . $name);
        }
    }

    public function unlock(string $name): void
    {
        if(!$this->releaseLock($name)) {
            throw new LogicException('Не удалось снять блокировку ' . $name);
        }
    }
}