<?php

/**
 * Класс ActiveRecord
 * Создан для удобства управления json-полями: преобразует массив в json и обратно
 * ВНИМАНИЕ!!! Json-поля должны иметь правило валидации safe!!!
 */

namespace common\components;

use yii\db\ActiveRecord;

/** @var $this ActiveRecord */
abstract class JsonFieldsAsArray extends ActiveRecord
{
    /**
     * @return array
     */
    abstract protected function getJsonFields(): array;

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        if($this->isJsonField($name)) {
            $value = json_encode($value);
        }
        parent::__set($name, $value);
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function __get($name)
    {
        $val = parent::__get($name);
        if($this->isJsonField($name)) {
            $val = json_decode($val);
        }

        return $val;
    }

    /**
     * @param string $name
     * @return bool
     */
    private function isJsonField(string $name): bool
    {
        return in_array($name, $this->getJsonFields());
    }
}