<?php

namespace common\components;

abstract class SingletonCollection
{
    private static ?self $instance = null;

    protected array $items = [];

    /**
     * @return static
     */
    public static function getInstance(): self
    {
        if(!self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->items = $this->getItems();
    }

    abstract protected function getItems(): array;

    public function all(): array
    {
        return $this->items;
    }

    /**
     * @param callable $callback
     * @param mixed|null $default
     * @return mixed
     */
    public function first(callable $callback, $default = null)
    {
        foreach ($this->all() as $key => $value) {
            if ($callback($value, $key)) {
                return $value;
            }
        }

        return $default;
    }
}