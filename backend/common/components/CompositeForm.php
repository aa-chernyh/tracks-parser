<?php

namespace common\components;

use Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

abstract class CompositeForm extends Model
{

    /**
     * @var Model[]|array[]
     */
    private array $forms = [];

    /**
     * @return array of internal forms like ['meta', 'values']
     */
    abstract public function internalForms(): array;

    /**
     * @param array $data
     * @param string|null $formName
     * @return bool
     */
    public function load($data, $formName = null): bool
    {
        try {
            $scope = $formName === null ? $this->formName() : $formName;
            if(isset($data[$scope])) {
                $data = $data[$scope];
            }

            $success = parent::attributes() ? parent::load($data, '') : !empty($data);
            foreach ($this->forms as $name => $form) {
                if (is_array($form)) {
                    $success = $this->loadMultipleForms($data, $formName, $form, $name, $success);
                } else {
                    $success = $form->load(ArrayHelper::getValue($data, $name, []), '') && $success;
                }
            }

            return $success;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * @param string[]|null $attributeNames
     * @param bool $clearErrors
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true): bool
    {
        try {
            if ($attributeNames !== null) {
                $parentNames = array_filter($attributeNames, 'is_string');
                $success = $parentNames ? parent::validate($parentNames, $clearErrors) : true;
            } else {
                $success = parent::validate(null, $clearErrors);
            }
            foreach ($this->forms as $name => $form) {
                $success = $this->validateForm($attributeNames, $clearErrors, $name, $form, $success);
            }
            return $success;
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * @param string|null $attribute
     * @return bool
     */
    public function hasErrors($attribute = null): bool
    {
        if ($attribute !== null && mb_strpos($attribute, '.') === false) {
            return parent::hasErrors($attribute);
        }
        if (parent::hasErrors($attribute)) {
            return true;
        }
        foreach ($this->forms as $name => $form) {
            if (is_array($form)) {
                foreach ($form as $i => $item) {
                    if ($attribute === null) {
                        if ($item->hasErrors()) {
                            return true;
                        }
                    } elseif (mb_strpos($attribute, $name . '.' . $i . '.') === 0) {
                        if ($item->hasErrors(mb_substr($attribute, mb_strlen($name . '.' . $i . '.')))) {
                            return true;
                        }
                    }
                }
            } else {
                if ($attribute === null) {
                    if ($form->hasErrors()) {
                        return true;
                    }
                } elseif (mb_strpos($attribute, $name . '.') === 0) {
                    if ($form->hasErrors(mb_substr($attribute, mb_strlen($name . '.')))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function getErrorsAsInputName(): array
    {
        $result = parent::getErrors();
        foreach ($this->forms as $form) {
            try {
                if (is_array($form)) {
                    /** @var Model[] $form */
                    foreach ($form as $i => $item) {
                        $name = $item->formName();
                        foreach ($this->getErrorsAsInputNameByModel($item) as $attr => $errors) {
                            $result[$name][$i][$attr] = $errors;
                        }
                    }
                } else {
                    $name = $form->formName();
                    foreach ($this->getErrorsAsInputNameByModel($form) as $attr => $errors) {
                        $result[$name][$attr] = $errors;
                    }
                }
            } catch (InvalidConfigException $exception) {
                continue;
            }
        }
        return $result;
    }

    private function getErrorsAsInputNameByModel(Model $model): array
    {
        return ($model instanceof CompositeForm)
            ? $model->getErrorsAsInputName()
            : $model->getErrors();
    }

    /**
     * @param string|null $attribute
     * @return array
     */
    public function getErrors($attribute = null): array
    {
        $result = parent::getErrors($attribute);
        foreach ($this->forms as $name => $form) {
            if (is_array($form)) {
                foreach ($form as $i => $item) {
                    foreach ($item->getErrors() as $attr => $errors) {
                        $errorAttr = $name . '.' . $i . '.' . $attr;
                        if ($attribute === null) {
                            foreach ($errors as $error) {
                                $result[$errorAttr][] = $error;
                            }
                        } elseif ($errorAttr === $attribute) {
                            foreach ($errors as $error) {
                                $result[] = $error;
                            }
                        }
                    }
                }
            } else {
                foreach ($form->getErrors() as $attr => $errors) {
                    $errorAttr = $name . '.' . $attr;
                    if ($attribute === null) {
                        foreach ($errors as $error) {
                            $result[$errorAttr][] = $error;
                        }
                    } elseif ($errorAttr === $attribute) {
                        foreach ($errors as $error) {
                            $result[] = $error;
                        }
                    }
                }
            }
        }
        return $result;
    }

    public function getFirstErrors(): array
    {
        $result = parent::getFirstErrors();
        foreach ($this->forms as $name => $form) {
            if (is_array($form)) {
                foreach ($form as $i => $item) {
                    foreach ($item->getFirstErrors() as $attr => $error) {
                        $result[$name . '.' . $i . '.' . $attr] = $error;
                    }
                }
            } else {
                foreach ($form->getFirstErrors() as $attr => $error) {
                    $result[$name . '.' . $attr] = $error;
                }
            }
        }
        return $result;
    }

    public function __get($name)
    {
        if (isset($this->forms[$name])) {
            return $this->forms[$name];
        }
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if (in_array($name, $this->internalForms(), true)) {
            $this->forms[$name] = $value;
            if (!is_array($value)) {
                $this->setSelfAsParentForm($value);
            } else {
                foreach ($value as $item) {
                    $this->setSelfAsParentForm($item);
                }
            }
        } else {
            parent::__set($name, $value);
        }
    }

    protected function setSelfAsParentForm(Model $form): void
    {
        if (property_exists($form, 'parentForm')) {
            $form->parentForm = $this;
        }
    }

    public function __isset($name): bool
    {
        return isset($this->forms[$name]) || parent::__isset($name);
    }

    /**
     * @param array|null $attributeNames
     * @param bool $clearErrors
     * @param string $name
     * @param Model|Model[] $form
     * @param bool $success
     * @return bool
     * @throws Exception
     */
    public function validateForm(?array $attributeNames, bool $clearErrors, string $name, $form, bool $success): bool
    {
        if ($attributeNames === null || array_key_exists($name, $attributeNames) || in_array(
                $name, $attributeNames,
                true
            )
        ) {
            $innerNames = ArrayHelper::getValue($attributeNames, $name);
            if (is_array($form)) {
                $success = Model::validateMultiple($form, $innerNames) && $success;
            } else {
                $success = $form->validate($innerNames, $clearErrors) && $success;
            }
        }
        return $success;
    }

    /**
     * @param array $data
     * @param string|null $formName
     * @param Model[] $forms
     * @param string $name
     * @param bool $success
     * @return bool
     * @throws InvalidConfigException
     */
    public function loadMultipleForms(array $data, ?string $formName, array $forms, string $name, bool $success): bool
    {
        /* @var $first Model|false */
        $first = reset($forms);

        if ($formName === null) {
            if ($first === false) {
                return false;
            }
            $formName = $first->formName();
        } else {
            $formName = $name;
        }

        if (!empty($data[$formName]) && (array_diff_key($forms, $data[$formName]) || array_diff_key($data[$formName], $forms))) {
            $keys = array_keys($data[$formName]);
            $forms = [];
            foreach ($keys as $key) {
                $forms[$key] = method_exists($first, 'cloneForm') ? $first->cloneForm() : clone $first;
            }
            $this->{$name} = $forms;
        }

        return Model::loadMultiple($forms, $data, $formName) && $success;
    }

    public function toFormArray(): array
    {
        $internalArrays = [];
        foreach ($this->internalForms() as $formName) {
            if (isset($this->forms[$formName])) {
                if(is_array($this->forms[$formName])) {
                    foreach ($this->forms[$formName] as $key => $value) {
                        $internalArrays[$formName][$key] = $this->getFormArray($value);
                    }

                    if(!count($this->forms[$formName])) {
                        $internalArrays[$formName] = [];
                    }
                } else {
                    $internalArrays[$formName] = $this->getFormArray($this->forms[$formName]);
                }
            }
        }
        return array_merge($this->toArray(), $internalArrays);
    }

    private function getFormArray(Model $model): array
    {
        return ($model instanceof CompositeForm)
            ? $model->toFormArray()
            : $model->toArray();
    }

    protected function unsetInnerFormArray(string $field): void
    {
        if(count($this->{$field}) === 1) {
            $arrayElement = $this->{$field}[0];

            $isEmpty = true;
            foreach (get_object_vars($arrayElement) as $key => $value) {
                $isEmpty &= is_null($value);
            }

            if($isEmpty) {
                $this->{$field} = [];
            }
        }
    }
}