<?php

namespace common\components;

use common\forms\PaginationForm;
use yii\db\ActiveQueryInterface;

class QueryPaginationDecorator implements ActiveQueryInterface
{

    private ActiveQueryInterface $query;
    private ActiveQueryInterface $defaultQuery;
    private PaginationForm $pagination;

    public function __construct(ActiveQueryInterface $query, PaginationForm $pagination)
    {
        $this->query = $query;
        $this->defaultQuery = clone $query;
        $this->pagination = $pagination;

        $this->init();
    }

    private function init(): void
    {
        $offset = $this->pagination->perPage * ($this->pagination->currentPage - 1);
        $this->query->offset($offset);
        $this->query->limit($this->pagination->perPage);
    }

    public function asArray($value = true)
    {
        return $this->query->asArray($value);
    }

    public function one($db = null)
    {
        return $this->query->one($db);
    }

    public function indexBy($column)
    {
        return $this->query->indexBy($column);
    }

    public function with()
    {
        return $this->query->with();
    }

    public function via($relationName, callable $callable = null)
    {
        return $this->query->via($relationName, $callable);
    }

    public function findFor($name, $model)
    {
        return $this->query->findFor($name, $model);
    }

    public function all($db = null)
    {
        return $this->query->all($db);
    }

    public function count($q = '*', $db = null)
    {
        return $this->query->count($q, $db);
    }

    public function exists($db = null)
    {
        return $this->query->exists($db);
    }

    public function where($condition)
    {
        return $this->query->where($condition);
    }

    public function andWhere($condition)
    {
        return $this->query->andWhere($condition);
    }

    public function orWhere($condition)
    {
        return $this->query->orWhere($condition);
    }

    public function filterWhere(array $condition)
    {
        return $this->query->filterWhere($condition);
    }

    public function andFilterWhere(array $condition)
    {
        return $this->query->andFilterWhere($condition);
    }

    public function orFilterWhere(array $condition)
    {
        return $this->query->orFilterWhere($condition);
    }

    public function orderBy($columns)
    {
        return $this->query->orderBy($columns);
    }

    public function addOrderBy($columns)
    {
        return $this->query->addOrderBy($columns);
    }

    public function limit($limit)
    {
        return $this->query->limit($limit);
    }

    public function offset($offset)
    {
        return $this->query->offset($offset);
    }

    public function emulateExecution($value = true)
    {
        return $this->query->emulateExecution($value);
    }

    public function totalCount($q = '*', $db = null): int
    {
        return $this->defaultQuery->count($q, $db);
    }
}