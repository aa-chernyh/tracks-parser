<?php

namespace common\components;

use Exception;
use yii\base\UnknownPropertyException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/** @var $this ActiveRecord */
abstract class MetaActiveRecord extends ActiveRecord
{

    /**
     * @return string
     */
    protected function getJsonFieldsAttribute(): string
    {
        return 'meta';
    }

    /**
     * @return array
     */
    protected abstract function getJsonAttributes(): array;

    /**
     * @return array
     */
    public function safeAttributes(): array
    {
        return array_merge(parent::safeAttributes(), $this->getJsonAttributes());
    }

    /**
     * @return array
     */
    public function getJsonFields(): array
    {
        $attr = $this->getJsonFieldsAttribute();
        return $this->$attr ?: [];
    }

    /**
     * @param array $fields
     */
    public function setJsonFields(array $fields): void
    {
        $attr = $this->getJsonFieldsAttribute();
        $this->$attr = $fields;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     * @throws Exception
     */
    public function getJsonFieldAttribute($key, $default = null)
    {
        $attr = $this->getJsonFieldsAttribute();
        return ArrayHelper::getValue($this->$attr, $key, $default);
    }

    /**
     * @param $key
     * @param $value
     */
    public function setJsonFieldAttribute($key, $value): void
    {
        $attr = $this->getJsonFieldsAttribute();
        if (!is_array($this->$attr)) {
            $this->$attr = [];
        }
        $this->$attr = array_merge($this->$attr, [$key => $value]);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        $attr = $this->getJsonFieldsAttribute();
        if (isset($this->$attr[$name])) {
            return true;
        }
        return parent::__isset($name);
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        try {
            parent::__set($name, $value);
        } catch (UnknownPropertyException $exception) {
            $this->setJsonFieldAttribute($name, $value);
        }
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function __get($name)
    {
        $val = null;
        try {
            $val = parent::__get($name);
        } catch (UnknownPropertyException $exception) {
            $attr = $this->getJsonFieldsAttribute();
            $val = $this->$attr[$name] ?? null;
        }
        return $val;
    }

    /**
     * Вынимает meta-атрибуты и помещает в общий массив
     * @param null $names
     * @param array $except
     * @return array
     */
    public function getAttributes($names = null, $except = [])
    {
        $result = parent::getAttributes($names, $except);
        if(isset($result[$this->getJsonFieldsAttribute()])) {
            foreach($result[$this->getJsonFieldsAttribute()] as $key => $value) {
                $result[$key] = $value;
            }

            unset($result[$this->getJsonFieldsAttribute()]);
        }

        return $result;
    }

    /**
     * Помещает meta-атрибуты в meta-поле
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true)
    {
        if(isset($values[$this->getJsonFieldsAttribute()])) {
            foreach ($this->getJsonAttributes() as $attribute) {
                if (isset($values[$attribute])) {
                    $values[$this->getJsonFieldsAttribute()][$attribute] = $values[$attribute];
                    unset($values[$attribute]);
                }
            }
        }

        parent::setAttributes($values, $safeOnly);
    }
}
