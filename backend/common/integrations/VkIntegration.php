<?php

namespace common\integrations;

use common\integrations\common\IntegrationServiceResult;
use common\integrations\configs\VkConfig;
use common\integrations\vk\jobs\LoadPopularJob;
use common\integrations\vk\jobs\LoadRecommendationsJob;
use common\integrations\vk\jobs\LoadUserAudioListJob;
use common\integrations\vk\operations\LoadPopularOperation;
use common\integrations\vk\operations\LoadUserRecommendationsOperation;
use common\integrations\vk\operations\LoadUserTrackListOperation;
use common\integrations\vk\services\GetTracksFromAudioDTOListService;
use common\integrations\vk\services\GetUserIdByUrlService;
use common\services\GetUniqueTracksService;
use common\services\SaveTrackListWithRelationsService;
use Throwable;
use VKSDK\dto\AudioDTO;
use Yii;
use yii\base\InvalidConfigException;

/**
 * @property VkConfig $config
 */
class VkIntegration extends common\BaseIntegration
{

    public const REQUEST_LOCK_KEY = 'vk_request_lock';

    protected function getIntegrationName(): string
    {
        return self::VK;
    }

    public function service(): IntegrationServiceResult
    {
        $this->pushLoadUsersAudioListJobs();
        $this->pushLoadRecommendationsJobs();
        $this->pushLoadPopularJob();

        return new IntegrationServiceResult();
    }

    private function pushLoadUsersAudioListJobs(): void
    {
        if($this->config->isLoadUsersAudioList) {
            foreach ($this->config->userUrlList ?? [] as $url) {
                Yii::$app->queue->push(new LoadUserAudioListJob([
                    'userUrl' => $url
                ]));
            }
        }
    }

    private function pushLoadRecommendationsJobs(): void
    {
        if($this->config->isLoadRecommendations) {
            foreach ($this->config->userUrlList ?? [] as $url) {
                Yii::$app->queue->push(new LoadRecommendationsJob([
                    'userUrl' => $url
                ]));
            }
        }
    }

    private function pushLoadPopularJob(): void
    {
        if($this->config->isLoadPopular) {
            Yii::$app->queue->push(new LoadPopularJob());
        }
    }

    /**
     * @param string $userUrl
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function loadUserAudioList(string $userUrl): void
    {
        $service = new GetUserIdByUrlService($userUrl, $this->config);
        $userId = $service->service()->userId;

        $operation = new LoadUserTrackListOperation($this->config, $userId);
        $this->formatAndSaveTracks($operation->service()->audios);
    }

    /**
     * @param string $userUrl
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function loadRecommendations(string $userUrl): void
    {
        $service = new GetUserIdByUrlService($userUrl, $this->config);
        $userId = $service->service()->userId;

        $operation = new LoadUserRecommendationsOperation($this->config, $userId);
        $this->formatAndSaveTracks($operation->service()->audios);
    }

    /**
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function loadPopular(): void
    {
        $operation = new LoadPopularOperation($this->config);
        $this->formatAndSaveTracks($operation->service()->audios);
    }

    /**
     * @param AudioDTO[] $audioDTOList
     * @throws InvalidConfigException
     */
    private function formatAndSaveTracks(array $audioDTOList): void
    {
        $getTracksFromAudioDTOListOperation = new GetTracksFromAudioDTOListService($audioDTOList);
        $tracks = $getTracksFromAudioDTOListOperation->service()->tracks;

        $uniqueTrackService = new GetUniqueTracksService($tracks);
        $tracks = $uniqueTrackService->service()->tracks;

        $saveTrackService = new SaveTrackListWithRelationsService($tracks);
        $saveTrackService->service();
    }
}