<?php

namespace common\integrations\promodj\jobs;

use common\integrations\common\BaseIntegration;
use common\integrations\common\IntegrationFactory;
use common\integrations\PromodjIntegration;
use common\jobs\BaseJob;

class ParseNewJob extends BaseJob
{

    /**
     * @var integer
     */
    public $pageNumber;

    public function run(): void
    {
        $integrationFactory = new IntegrationFactory(BaseIntegration::PROMODJ);
        /** @var PromodjIntegration $integration */
        $integration = $integrationFactory->getIntegration();
        $integration->parseNewFromPage($this->pageNumber);
    }
}