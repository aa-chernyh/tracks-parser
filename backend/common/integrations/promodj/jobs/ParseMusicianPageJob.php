<?php

namespace common\integrations\promodj\jobs;

use common\integrations\common\IntegrationFactory;
use common\integrations\common\BaseIntegration;
use common\integrations\PromodjIntegration;
use common\jobs\BaseJob;
use Throwable;

class ParseMusicianPageJob extends BaseJob
{

    /**
     * @var string
     */
    public $musicianUrl;

    /**
     * @throws Throwable
     */
    public function run(): void
    {
        $integrationFactory = new IntegrationFactory(BaseIntegration::PROMODJ);
        /** @var PromodjIntegration $integration */
        $integration = $integrationFactory->getIntegration();
        $integration->parseNewTrackListFromMusicianPage($this->musicianUrl);
    }
}