<?php

namespace common\integrations\promodj\services;

use common\helpers\CollectionsAdapter;
use common\helpers\Utils;
use common\integrations\promodj\serviceResults\GetTrackFromTrackDataServiceResult;
use common\modelFacades\style\StyleFacade;
use common\modelFacades\style\StyleReadableInterface;
use common\modelFacades\track\TrackFacade;
use common\models\Style;
use common\models\Track;
use common\integrations\common\BaseIntegration;
use common\services\base\ServiceInterface;
use Exception;
use PromodjSDK\models\track\DownloadAnchor;
use PromodjSDK\models\track\Sources;
use PromodjSDK\responses\GetTrackDataResponse;
use yii\helpers\ArrayHelper;

class GetTrackFromTrackDataService implements ServiceInterface
{

    private GetTrackDataResponse $trackData;

    /**
     * @var StyleFacade[]
     */
    private array $stylesMapping = [];
    
    public function __construct(GetTrackDataResponse $trackData)
    {
        $this->trackData = $trackData;
    }

    /**
     * @throws Exception
     */
    public function service(): GetTrackFromTrackDataServiceResult
    {
        $track = new TrackFacade(new Track());
        $track->setName($this->trackData->common->name);
        $track->setPageUrl($this->trackData->links->shortLink);
        $track->setBPM($this->getBpmFromTrackData($this->trackData));
        $track->setKey($this->trackData->description->key);
        $track->setReleaseDate(Utils::getDateTimeByDbDateString($this->trackData->description->publicationOnDate));

        $downloadAnchor = $this->getMp3DownloadAnchor();
        if($downloadAnchor) {
            $track->setDownloadUrl($downloadAnchor->href);
            $track->setBitrate($downloadAnchor->bitrate);
            $track->setExtension($downloadAnchor->format);
        }

        $source = $this->getMp3Source();
        if($source) {
            $track->setDuration($source->length);
            $track->setSize($source->size);
            if(!$downloadAnchor->href) {
                $track->setDownloadUrl($source->URL);
            }
        }

        $track->setIntegrationId(BaseIntegration::PROMODJ);
        $track->setStyles(CollectionsAdapter::getStyles($this->getStyles()));

        $result = new GetTrackFromTrackDataServiceResult();
        $result->track = $track;
        return $result;
    }

    private function getMp3DownloadAnchor(): ?DownloadAnchor
    {
        foreach ($this->trackData->description->downloadAnchors as $downloadAnchor) {
            if($downloadAnchor->format == 'mp3') {
                return $downloadAnchor;
            }
        }

        return null;
    }

    private function getMp3Source(): ?Sources
    {
        foreach ($this->trackData->playerInitializeData->sources as $source) {
            if(stripos($source->URL, 'mp3') !== false) {
                return $source;
            }
        }
    }

    private function getBpmFromTrackData(GetTrackDataResponse $trackData): ?int
    {
        if($trackData->description->bpm) {
            return intval($trackData->description->bpm);
        }

        if($trackData->description->bpmTo && $trackData->description->bpmFrom) {
            return intdiv($trackData->description->bpmTo + $trackData->description->bpmFrom, 2);
        }

        return null;
    }

    /**
     * @return StyleFacade[]
     * @throws Exception
     */
    private function getStyles(): array
    {
        $styles = [];
        foreach ($this->trackData->description->styles as $styleString) {
            $style = ArrayHelper::getValue($this->getStylesMapping(), $styleString);
            if($style) {
                $styles[] = $style;
            }
        }

        return $styles;
    }

    /**
     * @return StyleFacade[]
     */
    private function getStylesMapping(): array
    {
        if(!count($this->stylesMapping)) {
            $stylesReadable = CollectionsAdapter::getStyleFacades(Style::find()->all());
            $this->stylesMapping = ArrayHelper::index($stylesReadable, function (StyleReadableInterface $styleReadable) {
                return $styleReadable->getName();
            });
        }

        return $this->stylesMapping;
    }
}