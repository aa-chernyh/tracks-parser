<?php

namespace common\integrations\promodj\services;

use common\integrations\promodj\serviceResults\GetTracksServiceResult;
use common\modelFacades\author\AuthorFacade;
use common\modelFacades\track\TrackFacade;
use common\models\Author;
use common\integrations\configs\PromodjConfig;
use common\integrations\promodj\operations\GetAuthorOperation;
use common\integrations\promodj\operations\GetTrackDataOperation;
use common\services\base\ServiceInterface;
use Exception;
use PromodjSDK\responses\GetTrackDataResponse;
use Throwable;

class GetTracksService implements ServiceInterface
{

    /**
     * @var string[]
     */
    private array $trackUrls;

    private PromodjConfig $config;

    /**
     * @var AuthorFacade[]
     */
    private array $authorsCache = [];

    public function __construct(array $trackUrls, PromodjConfig $config)
    {
        $this->trackUrls = $trackUrls;
        $this->config = $config;
    }

    /**
     * @throws Throwable
     */
    public function service(): GetTracksServiceResult
    {
        $tracks = [];
        foreach ($this->trackUrls as $trackUrl) {
            $trackData = $this->getTrackData($trackUrl);
            if($trackData && $this->checkTrackData($trackData)) {
                $track = $this->getTrackFromData($trackData);
                $author = $this->getAuthor($trackData->musician->url);
                $track->setAuthors([$author]);
                $tracks[] = $track;
            }
        }

        $result = new GetTracksServiceResult();
        $result->trackShorts = $tracks;
        return $result;
    }

    /**
     * @param string $musicianUrl
     * @return AuthorFacade
     * @throws Throwable
     */
    private function getAuthor(string $musicianUrl): AuthorFacade
    {
        if(isset($this->authorsCache[$musicianUrl])) {
            return $this->authorsCache[$musicianUrl];
        }

        $authorModel = Author::findOne(['url' => $musicianUrl]);
        if(!$authorModel) {
            $operation = new GetAuthorOperation($musicianUrl);
            $author = $operation->service()->author;
        } else {
            $author = new AuthorFacade($authorModel);
        }

        $this->authorsCache[$musicianUrl] = $author;
        return $author;
    }

    /**
     * @param string $url
     * @return GetTrackDataResponse
     * @throws Throwable
     */
    private function getTrackData(string $url): ?GetTrackDataResponse
    {
        $operation = new GetTrackDataOperation($url);
        return $operation->service()->trackDataResponse;
    }

    private function checkTrackData(GetTrackDataResponse $trackData): bool
    {
        $operation = new CheckTrackDataService($trackData, $this->config);
        return $operation->service()->getIsSuccess();
    }

    /**
     * @param GetTrackDataResponse $trackData
     * @return TrackFacade
     * @throws Exception
     */
    private function getTrackFromData(GetTrackDataResponse $trackData): TrackFacade
    {
        $operation = new GetTrackFromTrackDataService($trackData);
        return $operation->service()->track;
    }
}