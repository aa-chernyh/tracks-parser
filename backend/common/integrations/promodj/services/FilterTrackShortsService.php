<?php

namespace common\integrations\promodj\services;

use common\integrations\promodj\serviceResults\FilterTrackShortsServiceResult;
use common\models\Track;
use common\integrations\configs\PromodjConfig;
use common\services\base\ServiceInterface;
use PromodjSDK\models\common\TrackShort;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class FilterTrackShortsService implements ServiceInterface
{

    /**
     * @var TrackShort[]
     */
    private $trackShorts;

    /**
     * @var PromodjConfig
     */
    private $config;

    /**
     * @param TrackShort[] $trackShorts
     * @param PromodjConfig $config
     */
    public function __construct(array $trackShorts, PromodjConfig $config)
    {
        $this->trackShorts = $trackShorts;
        $this->config = $config;
    }

    /**
     * @throws InvalidConfigException
     */
    public function service(): FilterTrackShortsServiceResult
    {
        $this->filterTrackShorts();
        $this->uniqueTrackShort();

        $result = new FilterTrackShortsServiceResult();
        $result->trackShorts = $this->trackShorts;
        return $result;
    }

    private function filterTrackShorts(): void
    {
        $result = [];
        foreach ($this->trackShorts as $trackShort) {
            if($this->checkTrackShort($trackShort)) {
                $result[] = $trackShort;
            }
        }

        $this->trackShorts = $result;
    }

    private function checkTrackShort(TrackShort $track): bool
    {
        $reqs = $this->config->trackRequirements;
        return !$track->isVideo
            && (!$reqs->minPromorank || $track->promorank > $reqs->minPromorank)
            && (!$reqs->minCountOfDownloads || $track->countOfDownloads > $reqs->minCountOfDownloads)
            && (!$reqs->minCountOfListens || $track->countOfListens > $reqs->minCountOfListens)
            && (!$reqs->minCountOfComments || $track->countOfComments > $reqs->minCountOfComments);
    }

    /**
     * @throws InvalidConfigException
     */
    private function uniqueTrackShort(): void
    {
        $trackShorts = ArrayHelper::index($this->trackShorts, 'href'); //Убираем дубликаты по url
        foreach ($trackShorts as $url => $trackShort) {
            if(stripos($url, '/extra/panasonic')) {
                unset($trackShorts[$url]);
            }
        }

        $trackShorts = ArrayHelper::index($trackShorts, 'name'); //Убираем дубликаты по имени
        $existsTrackNames = Track::find()->select(['name'])->where(['name' => array_keys($trackShorts)])->column();

        $result = [];
        foreach ($trackShorts as $name => $trackShort) {
            if(!in_array($name, $existsTrackNames)) {
                $result[] = $trackShort;
            }
        }

        $this->trackShorts = $result;
    }
}