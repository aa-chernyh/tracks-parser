<?php

namespace common\integrations\promodj\services;

use common\integrations\configs\DTO\PromodjTrackRequirements;
use common\integrations\configs\PromodjConfig;
use common\integrations\promodj\serviceResults\CheckTrackDataServiceResult;
use common\services\base\ServiceInterface;
use PromodjSDK\enums\Styles;
use PromodjSDK\responses\GetTrackDataResponse;

class CheckTrackDataService implements ServiceInterface
{

    /**
     * @var GetTrackDataResponse
     */
    private $trackData;

    /**
     * @var PromodjTrackRequirements
     */
    private $reqs;

    public function __construct(GetTrackDataResponse $trackData, PromodjConfig $config)
    {
        $this->trackData = $trackData;
        $this->reqs = $config->trackRequirements;
    }

    public function service(): CheckTrackDataServiceResult
    {
        $result = new CheckTrackDataServiceResult();
        $checkResult = $this->checkOnlyTop100()
            && $this->checkOnlyPdjFm()
            && $this->checkIsDownloadable()
            && $this->checkMinPromorank()
            && $this->checkMinLength()
            && $this->checkMaxLength()
            && $this->checkIsInCompetition()
            && $this->checkKinds()
            && $this->checkMinCountOfDownloads()
            && $this->checkMinCountOfListens()
            && $this->checkStyles();

        if(!$checkResult) {
            $result->setError('Трек не валиден');
        }

        return $result;
    }
    
    private function checkOnlyTop100(): bool
    {
        return !$this->reqs->onlyTop100 || $this->trackData->common->isInTop100;
    }

    private function checkOnlyPdjFm(): bool
    {
        return !$this->reqs->onlyPdjFm || $this->trackData->common->isOnPdjFM;
    }

    private function checkIsDownloadable(): bool
    {
        return $this->trackData->playerInitializeData->downloadable
            && $this->trackData->playerInitializeData->downloadURL;
    }

    private function checkMinPromorank(): bool
    {
        return !$this->reqs->minPromorank
            || $this->trackData->common->promorank > $this->reqs->minPromorank;
    }

    private function checkMinLength(): bool
    {
        return !$this->reqs->minLengthInSeconds
            || $this->trackData->description->length >= $this->reqs->minLengthInSeconds;
    }

    private function checkMaxLength(): bool
    {
        return !$this->reqs->maxLengthInSeconds
            || $this->trackData->description->length <= $this->reqs->maxLengthInSeconds;
    }

    private function checkIsInCompetition(): bool
    {
        return !$this->reqs->disallowTracksFromCompetition
            || !$this->trackData->description->isInCompetition;
    }

    private function checkKinds(): bool
    {
        return !$this->reqs->allowedKindList
            || (
                is_array($this->reqs->allowedKindList)
                && in_array($this->trackData->playerInitializeData->kind, $this->reqs->allowedKindList)
            );
    }

    private function checkMinCountOfDownloads(): bool
    {
        return !$this->reqs->minCountOfDownloads
            || $this->trackData->description->countOfDownloads > $this->reqs->minCountOfDownloads;
    }

    private function checkMinCountOfListens(): bool
    {
        return !$this->reqs->minCountOfListens
            || $this->trackData->description->countOfListens > $this->reqs->minCountOfListens;
    }

    private function checkStyles(): bool
    {
        if(is_array($this->trackData->description->styles)) {
            foreach ($this->trackData->description->styles as $style) {
                if(!in_array($style, Styles::NAME_LIST)) {
                    return false;
                }
            }
        }

        return true;
    }
}