<?php

namespace common\integrations\promodj\services;

use common\integrations\promodj\operations\GetGroupTracksOperation;
use common\integrations\promodj\serviceResults\GetTrackShortsFromGroupsServiceResult;
use common\services\base\ServiceInterface;
use PromodjSDK\models\musicianTracks\Group;
use Throwable;

class GetTrackShortsFromGroupsService implements ServiceInterface
{

    private const COUNT_OF_TRACKS_IN_HIDDEN_GROUP = 20;

    /**
     * @var Group[]
     */
    private $groups;

    /**
     * @param Group[] $groups
     */
    public function __construct(array $groups)
    {
        $this->groups = $groups;
    }

    /**
     * @throws Throwable
     */
    public function service(): GetTrackShortsFromGroupsServiceResult
    {
        $trackShorts = [];
        foreach ($this->groups as $group) {
            for($page = 1; $page <= $this->getCountOfPagesInGroup($group); $page++) {
                $operation = new GetGroupTracksOperation($group->href, $page);
                $trackShorts = array_merge($operation->service()->trackShorts, $trackShorts);
            }
        }

        $result = new GetTrackShortsFromGroupsServiceResult();
        $result->trackShorts = $trackShorts;
        return $result;
    }

    private function getCountOfPagesInGroup(Group $group): int
    {
        return ceil($group->count / self::COUNT_OF_TRACKS_IN_HIDDEN_GROUP);
    }
}