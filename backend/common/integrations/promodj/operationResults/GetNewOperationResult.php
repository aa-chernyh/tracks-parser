<?php

namespace common\integrations\promodj\operationResults;

use common\services\base\BaseServiceResult;
use PromodjSDK\models\common\TrackShort;

class GetNewOperationResult extends BaseServiceResult
{
    /**
     * @var TrackShort[]
     */
    public array $trackShorts = [];
}