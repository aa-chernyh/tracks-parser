<?php

namespace common\integrations\promodj\operationResults;

use common\services\base\BaseServiceResult;
use PromodjSDK\responses\GetMusicianTracksResponse;

class GetMusicianTracksOperationResult extends BaseServiceResult
{
    public ?GetMusicianTracksResponse $musicianTracksResponse = null;
}