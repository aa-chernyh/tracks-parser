<?php

namespace common\integrations\promodj\operationResults;

use common\modelFacades\author\AuthorFacade;
use common\services\base\BaseServiceResult;

class GetAuthorOperationResult extends BaseServiceResult
{
    public ?AuthorFacade $author = null;
}