<?php

namespace common\integrations\promodj\operationResults;

use common\services\base\BaseServiceResult;
use PromodjSDK\responses\GetTrackDataResponse;

class GetTrackDataOperationResult extends BaseServiceResult
{
    public ?GetTrackDataResponse $trackDataResponse = null;
}