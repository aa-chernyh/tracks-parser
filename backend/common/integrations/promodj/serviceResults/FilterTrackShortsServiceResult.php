<?php

namespace common\integrations\promodj\serviceResults;

use common\services\base\BaseServiceResult;
use PromodjSDK\models\common\TrackShort;

class FilterTrackShortsServiceResult extends BaseServiceResult
{
    /**
     * @var TrackShort[]
     */
    public array $trackShorts = [];
}