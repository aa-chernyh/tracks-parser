<?php

namespace common\integrations\promodj\serviceResults;

use common\modelFacades\track\TrackFacade;
use common\services\base\BaseServiceResult;

class GetTrackFromTrackDataServiceResult extends BaseServiceResult
{
    public ?TrackFacade $track = null;
}