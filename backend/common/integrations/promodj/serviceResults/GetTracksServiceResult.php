<?php

namespace common\integrations\promodj\serviceResults;

use common\modelFacades\track\TrackFacade;
use common\services\base\BaseServiceResult;

class GetTracksServiceResult extends BaseServiceResult
{
    /**
     * @var TrackFacade[]
     */
    public array $trackShorts = [];
}