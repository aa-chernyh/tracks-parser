<?php

namespace common\integrations\promodj\operations;

use ClientInterface\Exception\ValidationException;
use common\integrations\promodj\operationResults\GetMusicianTracksOperationResult;
use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\exceptions\AccessException;
use PromodjSDK\exceptions\RequestException;
use PromodjSDK\exceptions\ValidateException;
use PromodjSDK\requests\GetMusicianTracksRequest;
use PromodjSDK\responses\GetMusicianTracksResponse;

/**
 * @property GetMusicianTracksRequest $request
 * @property GetMusicianTracksResponse $response
 * @property GetMusicianTracksOperationResult $result
 * @method GetMusicianTracksOperationResult service()
 */
class GetMusicianTracksOperation extends PromodjApiOperation
{

    private const GET_MUSICIAN_TRACKS_URL_ENDING = '/music';

    public function __construct(string $url)
    {
        $this->url = $url . self::GET_MUSICIAN_TRACKS_URL_ENDING;
    }

    protected function initResult(): void
    {
        $this->result = new GetMusicianTracksOperationResult();
    }

    protected function buildRequest(): void
    {
        $this->request = new GetMusicianTracksRequest($this->url);
    }

    /**
     * @throws ValidationException
     * @throws InvalidSelectorException
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     */
    protected function sendRequestInner(): void
    {
        $this->response = $this->getClient()->getMusicianTracks($this->request);
    }

    protected function fillResult(): void
    {
        $this->result->musicianTracksResponse = $this->response;
    }

    protected function getLogType(): string
    {
        return self::TYPE_GET_MUSICIAN_TRACKS;
    }
}