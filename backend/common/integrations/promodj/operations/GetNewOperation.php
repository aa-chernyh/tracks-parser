<?php

namespace common\integrations\promodj\operations;

use ClientInterface\Exception\ValidationException;
use common\integrations\promodj\operationResults\GetNewOperationResult;
use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\exceptions\AccessException;
use PromodjSDK\exceptions\RequestException;
use PromodjSDK\exceptions\ValidateException;
use PromodjSDK\requests\MusicSearchRequest;
use PromodjSDK\responses\MusicSearchResponse;
use yii\helpers\ArrayHelper;

/**
 * @property MusicSearchRequest $request
 * @property MusicSearchResponse $response
 * @property GetNewOperationResult $result
 * @method GetNewOperationResult service()
 */
class GetNewOperation extends PromodjApiOperation
{

    /**
     * @var integer
     */
    private $pageNumber;

    public function __construct(int $pageNumber)
    {
        $this->pageNumber = $pageNumber;
    }

    protected function initResult(): void
    {
        $this->result = new GetNewOperationResult();
    }

    protected function buildRequest(): void
    {
        $this->request = new MusicSearchRequest();
        $this->request->setSortBy(MusicSearchRequest::SORT_BY_FRESH);
        $this->request->setPeriodLast('1m');
        $this->request->setIsDownloadable();
        $this->request->setPage($this->pageNumber);
    }

    /**
     * @throws ValidationException
     * @throws InvalidSelectorException
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     */
    protected function sendRequestInner(): void
    {
        $this->response = $this->getClient()->musicSearch($this->request);
    }

    protected function fillResult(): void
    {
        $this->result->trackShorts = ArrayHelper::getValue($this->response, 'tracks', []);
    }

    protected function getLogType(): string
    {
        return self::TYPE_GET_NEW;
    }
}