<?php

namespace common\integrations\promodj\operations;

use ClientInterface\Exception\ValidationException;
use common\integrations\promodj\operationResults\GetGroupTracksOperationResult;
use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\exceptions\AccessException;
use PromodjSDK\exceptions\RequestException;
use PromodjSDK\exceptions\ValidateException;
use PromodjSDK\requests\GetGroupTracksRequest;
use PromodjSDK\responses\GetGroupTracksResponse;
use yii\helpers\ArrayHelper;

/**
 * @property GetGroupTracksRequest $request
 * @property GetGroupTracksResponse $response
 * @property GetGroupTracksOperationResult $result
 * @method GetGroupTracksOperationResult service()
 */
class GetGroupTracksOperation extends PromodjApiOperation
{

    /**
     * @var integer
     */
    private $page;

    public function __construct(string $url, int $page = 1)
    {
        $this->url = $url;
        $this->page = $page;
    }

    protected function initResult(): void
    {
        $this->result = new GetGroupTracksOperationResult();
    }

    protected function buildRequest(): void
    {
        $this->request = new GetGroupTracksRequest($this->url);
        $this->request->page = $this->page;
    }

    /**
     * @throws ValidationException
     * @throws InvalidSelectorException
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     */
    protected function sendRequestInner(): void
    {
        $this->response = $this->getClient()->getGroupTracks($this->request);
    }

    protected function fillResult(): void
    {
        $this->result->trackShorts = ArrayHelper::getValue($this->response, 'tracks', []);
    }

    protected function getLogType(): string
    {
        return self::TYPE_GET_GROUP_TRACKS;
    }
}