<?php

namespace common\integrations\promodj\operations;

use common\exceptions\ProjectException;
use common\helpers\LogHelper;
use common\integrations\common\AbstractApiService;
use common\integrations\common\BaseIntegration;
use common\integrations\PromodjIntegration;
use PromodjSDK\Client;
use Throwable;
use Yii;

abstract class PromodjApiOperation extends AbstractApiService
{

    /**
     * @var string
     */
    protected $url;

    /**
     * @var Client|null;
     */
    private $client;

    protected function getClient(): Client
    {
        if(!$this->client) {
            $this->client = new Client();
        }

        return $this->client;
    }

    protected function sendRequest(): void
    {
        try {
            // Выполняем только один запрос к сервису из всех потоков, иначе словим бан по IP
            Yii::$app->mutex->lock(PromodjIntegration::REQUEST_LOCK_KEY);
            $this->sendRequestInner();
        } catch (ProjectException $exception) {
            //do nothing
        } catch (Throwable $exception) {
            LogHelper::writeException($exception);
        } finally {
            Yii::$app->mutex->unlock(PromodjIntegration::REQUEST_LOCK_KEY);

            LogHelper::writeIntegration(
                $this->getLogType(),
                BaseIntegration::PROMODJ,
                $this->response ? $this->response->isSuccess() : false,
                $this->getClient()->getLastUrl(),
                $this->jsonEncodeToDb($this->getClient()->getLastRequest()),
                $this->jsonEncodeToDb($this->getClient()->getLastResponse()),
                $this->jsonEncodeToDb($this->getClient()->getLastRequestHeaders()),
                $this->jsonEncodeToDb($this->getClient()->getLastResponseHeaders()),
                $this->response ? $this->response->getErrors() : null,
            );
        }
    }

    abstract protected function sendRequestInner(): void;

    protected function calculate(): void {}
}