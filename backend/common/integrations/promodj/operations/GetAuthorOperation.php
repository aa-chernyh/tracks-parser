<?php

namespace common\integrations\promodj\operations;

use ClientInterface\Exception\ValidationException;
use common\integrations\promodj\operationResults\GetAuthorOperationResult;
use common\modelFacades\author\AuthorFacade;
use common\models\Author;
use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\exceptions\AccessException;
use PromodjSDK\exceptions\RequestException;
use PromodjSDK\exceptions\ValidateException;
use PromodjSDK\requests\GetMusicianDataRequest;
use PromodjSDK\responses\GetMusicianDataResponse;

/**
 * @property GetMusicianDataRequest|null $request
 * @property GetMusicianDataResponse|null $response
 * @property GetAuthorOperationResult|null $result
 * @method GetAuthorOperationResult service()
 */
class GetAuthorOperation extends PromodjApiOperation
{

    private const MUSICIAN_DATA_URL_ENDING = '/contact';

    public function __construct(string $url)
    {
        $this->url = $url . self::MUSICIAN_DATA_URL_ENDING;
    }

    protected function getLogType(): string
    {
        return self::TYPE_GET_MUSICIAN_DATA;
    }

    protected function initResult(): void
    {
        $this->result = new GetAuthorOperationResult();
    }

    protected function buildRequest(): void
    {
        $this->request = new GetMusicianDataRequest($this->url);
    }

    /**
     * @throws ValidationException
     * @throws InvalidSelectorException
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     */
    protected function sendRequestInner(): void
    {
        $this->response = $this->getClient()->getMusicianData($this->request);
    }

    protected function fillResult(): void
    {
        $this->result->author = new AuthorFacade(new Author());
        $this->result->author->setName($this->response->anchor->nickname);
        $this->result->author->setUrl($this->response->anchor->url);
        $this->result->author->setRealName($this->response->common->realName);
    }
}