<?php

namespace common\integrations\promodj\operations;

use ClientInterface\Exception\ValidationException;
use common\integrations\promodj\operationResults\GetTrackDataOperationResult;
use DiDom\Exceptions\InvalidSelectorException;
use PromodjSDK\exceptions\AccessException;
use PromodjSDK\exceptions\RequestException;
use PromodjSDK\exceptions\ValidateException;
use PromodjSDK\requests\GetTrackDataRequest;
use PromodjSDK\responses\GetTrackDataResponse;

/**
 * @property GetTrackDataRequest $request
 * @property GetTrackDataResponse $response
 * @property GetTrackDataOperationResult $result
 * @method GetTrackDataOperationResult service()
 */
class GetTrackDataOperation extends PromodjApiOperation
{

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    protected function initResult(): void
    {
        $this->result = new GetTrackDataOperationResult();
    }

    protected function buildRequest(): void
    {
        $this->request = new GetTrackDataRequest($this->url);
    }

    /**
     * @throws ValidationException
     * @throws InvalidSelectorException
     * @throws AccessException
     * @throws RequestException
     * @throws ValidateException
     */
    protected function sendRequestInner(): void
    {
        $this->response = $this->getClient()->getTrackData($this->request);
    }

    protected function fillResult(): void
    {
        $this->result->trackDataResponse = $this->response;
    }

    protected function getLogType(): string
    {
        return self::TYPE_GET_TRACK_DATA;
    }
}