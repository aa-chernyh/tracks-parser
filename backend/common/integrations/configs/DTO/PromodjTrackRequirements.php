<?php

namespace common\integrations\configs\DTO;

class PromodjTrackRequirements
{

    /**
     * @var float|null
     */
    public $minPromorank;

    /**
     * @var int|null
     */
    public $minCountOfDownloads;

    /**
     * @var int|null
     */
    public $minCountOfListens;

    /**
     * @var int|null
     */
    public $minCountOfComments;

    /**
     * @var bool|null
     */
    public $onlyTop100;

    /**
     * @var bool|null
     */
    public $onlyPdjFm;

    /**
     * @var string[]|null
     */
    public $allowedKindList;

    /**
     * @var bool|null
     */
    public $disallowTracksFromCompetition;

    /**
     * @var int|null
     */
    public $minLengthInSeconds;

    /**
     * @var int|null
     */
    public $maxLengthInSeconds;
}