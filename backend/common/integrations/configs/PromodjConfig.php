<?php

namespace common\integrations\configs;

use common\integrations\configs\DTO\PromodjTrackRequirements;

class PromodjConfig extends AbstractConfig
{
    /**
     * @var string[]
     */
    public $musicianPages = [];

    /**
     * @var PromodjTrackRequirements
     */
    public $trackRequirements;

    /**
     * @var boolean|null
     */
    public $isParseMusiciansFromList;

    /**
     * @var boolean|null
     */
    public $isParsePopular;

    /**
     * @var boolean|null
     */
    public $isParseNew;

    /**
     * @var integer|null
     */
    public $countPagesToParseNew;

    /**
     * @var integer|null
     */
    public $countPagesToParsePopular;

    public function __construct()
    {
        $this->trackRequirements = new PromodjTrackRequirements();
    }
}