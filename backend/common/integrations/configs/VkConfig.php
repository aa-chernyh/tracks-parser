<?php

namespace common\integrations\configs;

class VkConfig extends AbstractConfig
{

    /**
     * @var string|null
     */
    public $login;

    /**
     * @var string|null
     */
    public $password;

    /**
     * @var boolean|null
     */
    public $isLoadUsersAudioList;

    /**
     * @var boolean|null
     */
    public $isLoadRecommendations;

    /**
     * @var boolean|null
     */
    public $isLoadPopular;

    /**
     * @var string[]
     */
    public $userUrlList = [];

    /**
     * @var int|null
     */
    public $countOfPopularTracks;

    /**
     * @var int|null
     */
    public $countOfRecommendationTracks;
}