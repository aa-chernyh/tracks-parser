<?php

namespace common\integrations;

use common\integrations\common\BaseIntegration;
use common\integrations\common\IntegrationServiceResult;
use common\integrations\configs\PromodjConfig;
use common\integrations\promodj\operations\GetMusicianTracksOperation;
use common\integrations\promodj\operations\GetNewOperation;
use common\integrations\promodj\operations\GetPopularOperation;
use common\integrations\promodj\services\FilterTrackShortsService;
use common\integrations\promodj\services\GetTrackShortsFromGroupsService;
use common\integrations\promodj\services\GetTracksService;
use common\services\SaveTrackListWithRelationsService;
use PromodjSDK\models\common\TrackShort;
use PromodjSDK\responses\GetMusicianTracksResponse;
use common\integrations\promodj\jobs\ParseMusicianPageJob;
use common\integrations\promodj\jobs\ParseNewJob;
use common\integrations\promodj\jobs\ParsePopularJob;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * @property PromodjConfig $config
 */
class PromodjIntegration extends BaseIntegration
{

    public const REQUEST_LOCK_KEY = 'promodj_request_lock';

    private const DEFAULT_PAGE_COUNT_TO_PARSE_POPULAR = 5;
    private const DEFAULT_PAGE_COUNT_TO_PARSE_NEW = 10;

    /**
     * @return string
     */
    protected function getIntegrationName(): string
    {
        return BaseIntegration::PROMODJ;
    }

    public function service(): IntegrationServiceResult
    {
        $this->pushParseMusicianJobs();
        $this->pushParsePopularJobs();
        $this->pushParseNewJobs();

        return new IntegrationServiceResult();
    }

    private function pushParseMusicianJobs(): void
    {
        if($this->config->isParseMusiciansFromList) {
            foreach ($this->config->musicianPages ?? [] as $url) {
                Yii::$app->queue->push(new ParseMusicianPageJob([
                    'musicianUrl' => $url
                ]));
            }
        }
    }

    private function pushParsePopularJobs(): void
    {
        if($this->config->isParsePopular) {
            $pageCount = $this->config->countPagesToParsePopular ?? self::DEFAULT_PAGE_COUNT_TO_PARSE_POPULAR;
            for ($i = 1; $i <= $pageCount; $i++) {
                Yii::$app->queue->push(new ParsePopularJob([
                    'pageNumber' => $i
                ]));
            }
        }
    }

    private function pushParseNewJobs(): void
    {
        if($this->config->isParseNew) {
            $pageCount = $this->config->countPagesToParseNew ?? self::DEFAULT_PAGE_COUNT_TO_PARSE_NEW;
            for ($i = 1; $i <= $pageCount; $i++) {
                Yii::$app->queue->push(new ParseNewJob([
                    'pageNumber' => $i
                ]));
            }
        }
    }

    /**
     * @param string $musicianUrl
     * @throws Throwable
     */
    public function parseNewTrackListFromMusicianPage(string $musicianUrl): void
    {
        $operation = new GetMusicianTracksOperation($musicianUrl);
        $musicianTracksAndGroups = $operation->service()->musicianTracksResponse;
        if($musicianTracksAndGroups) {
            $operation = new GetTrackShortsFromGroupsService($musicianTracksAndGroups->groups);
            $trackShorts = array_merge($operation->service()->trackShorts, $musicianTracksAndGroups->tracks);

            $this->formatAndSaveTracks($trackShorts);
        }
    }

    /**
     * @param int $pageNumber
     * @throws Throwable
     * @throws InvalidConfigException
     */
    public function parsePopularFromPage(int $pageNumber): void
    {
        $operation = new GetPopularOperation($pageNumber);
        $this->formatAndSaveTracks($operation->service()->trackShorts);
    }

    /**
     * @param int $pageNumber
     * @throws InvalidConfigException
     * @throws Throwable
     */
    public function parseNewFromPage(int $pageNumber): void
    {
        $operation = new GetNewOperation($pageNumber);
        $this->formatAndSaveTracks($operation->service()->trackShorts);
    }

    /**
     * @param TrackShort[] $trackShorts
     * @throws InvalidConfigException
     * @throws Throwable
     */
    private function formatAndSaveTracks(array $trackShorts): void
    {
        $operation = new FilterTrackShortsService($trackShorts, $this->config);
        $trackShorts = $operation->service()->trackShorts;

        $trackUrls = ArrayHelper::getColumn($trackShorts, 'href');
        $operation = new GetTracksService($trackUrls, $this->config);
        $trackShorts = $operation->service()->trackShorts;

        $operation = new SaveTrackListWithRelationsService($trackShorts);
        $operation->service();
    }
}
