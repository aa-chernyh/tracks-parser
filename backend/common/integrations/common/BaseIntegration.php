<?php

namespace common\integrations\common;

use ClientInterface\Base\PhpDocReader\AnnotationException;
use common\integrations\configs\AbstractConfig;
use common\managers\IntegrationManager;
use common\services\base\ServiceInterface;
use ReflectionException;
use yii\base\InvalidConfigException;

abstract class BaseIntegration implements ServiceInterface
{
    const PROMODJ = 'Promodj';
    const VK = 'VK';

    const LIST = [
        self::PROMODJ,
        self::VK,
    ];

    /**
     * @var AbstractConfig
     */
    protected $config;

    /**
     * @var IntegrationManager
     */
    private $integrationManager;

    /**
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public function __construct()
    {
        $this->integrationManager = new IntegrationManager();
        $this->config = $this->integrationManager->getIntegrationConfig($this->getIntegrationName());
    }

    /**
     * @return string
     */
    abstract protected function getIntegrationName(): string;
}
