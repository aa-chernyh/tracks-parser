<?php

namespace common\integrations\common;

use common\exceptions\LogicException;
use common\integrations\PromodjIntegration;
use common\integrations\VkIntegration;
use common\services\base\ServiceInterface;
use common\services\base\ServiceResultInterface;

class IntegrationFactory implements ServiceInterface
{

    /**
     * @var BaseIntegration
     */
    private $integration;

    /**
     * @var string
     */
    private $integrationKey;

    /**
     * @param string $integrationKey BaseIntegration::INTEGRATION_...
     * @throws LogicException
     */
    public function __construct(string $integrationKey)
    {
        $this->integrationKey = $integrationKey;
        $this->integration = $this->getIntegrationByKey($this->integrationKey);
    }

    /**
     * @param string $integrationKey
     * @return BaseIntegration
     * @throws LogicException
     */
    private function getIntegrationByKey(string $integrationKey): BaseIntegration
    {
        switch($integrationKey) {
            case BaseIntegration::PROMODJ:
                return new PromodjIntegration();
            case BaseIntegration::VK:
                return new VkIntegration();
            default:
                $message = 'Ключ интеграции отсутствует';
                throw new LogicException($message);
        }
    }

    public function getIntegration(): BaseIntegration
    {
        return $this->integration;
    }

    public function service(): ServiceResultInterface
    {
        return $this->integration->service();
    }
}