<?php

namespace common\integrations\common;

use ClientInterface\Request;
use ClientInterface\Response;
use common\helpers\LogHelper;
use common\services\base\ServiceInterface;
use common\services\base\ServiceResultInterface;
use Throwable;

abstract class AbstractApiService implements ServiceInterface
{
    protected const TYPE_GET_MUSICIAN_DATA = 'get_musician_data';
    protected const TYPE_GET_MUSICIAN_TRACKS = 'get_musician_tracks';
    protected const TYPE_GET_TRACK_DATA = 'get_track_data';
    protected const TYPE_GET_POPULAR = 'get_popular';
    protected const TYPE_GET_NEW = 'get_new';
    protected const TYPE_SEARCH = 'search';
    protected const TYPE_GET_GROUP_TRACKS = 'get_group_tracks';
    protected const TYPE_GET_USER_AUDIO_LIST = 'get_user_audio_list';
    protected const TYPE_GET_RECOMMENDATIONS = 'get_recommendations';
    protected const TYPE_GET_USER_DATA = 'get_user_data';

    private const TYPE_NAME_GET_MUSICIAN_DATA = 'Получение данных о музыканте';
    private const TYPE_NAME_GET_MUSICIAN_TRACKS = 'Получение списка треков музыканта';
    private const TYPE_NAME_GET_TRACK_DATA = 'Получение данных о треке';
    private const TYPE_NAME_GET_POPULAR = 'Получение списка популярных треков';
    private const TYPE_NAME_GET_NEW = 'Получение списка новых треков';
    private const TYPE_NAME_SEARCH = 'Поиск треков';
    private const TYPE_NAME_GET_GROUP_TRACKS = 'Получение треков из группы';
    private const TYPE_NAME_GET_USER_AUDIO_LIST = 'Получение списка треков пользователя';
    private const TYPE_NAME_GET_RECOMMENDATIONS = 'Получение списка рекомендованных треков';
    private const TYPE_NAME_GET_USER_DATA = 'Получение информации о пользователе';

    public const TYPE_MAPPING = [
        self::TYPE_GET_MUSICIAN_DATA => self::TYPE_NAME_GET_MUSICIAN_DATA,
        self::TYPE_GET_MUSICIAN_TRACKS => self::TYPE_NAME_GET_MUSICIAN_TRACKS,
        self::TYPE_GET_TRACK_DATA => self::TYPE_NAME_GET_TRACK_DATA,
        self::TYPE_GET_POPULAR => self::TYPE_NAME_GET_POPULAR,
        self::TYPE_GET_NEW => self::TYPE_NAME_GET_NEW,
        self::TYPE_SEARCH => self::TYPE_NAME_SEARCH,
        self::TYPE_GET_GROUP_TRACKS => self::TYPE_NAME_GET_GROUP_TRACKS,
        self::TYPE_GET_USER_AUDIO_LIST => self::TYPE_NAME_GET_USER_AUDIO_LIST,
        self::TYPE_GET_RECOMMENDATIONS => self::TYPE_NAME_GET_RECOMMENDATIONS,
        self::TYPE_GET_USER_DATA => self::TYPE_NAME_GET_USER_DATA,
    ];

    /**
     * @var Request
     */
    protected $request = [];

    /**
     * @var Response|null
     */
    protected $response;

    /**
     * @var ServiceResultInterface|null
     */
    protected $result;

    abstract protected function initResult(): void;

    abstract protected function buildRequest(): void;

    abstract protected function sendRequest(): void;

    abstract protected function calculate(): void;

    abstract protected function fillResult(): void;

    abstract protected function getLogType(): string;

    /**
     * @throws Throwable
     */
    public function service(): ServiceResultInterface
    {
        $this->initResult();
        $this->buildRequest();
        try {
            $this->sendRequest();
            $this->calculate();
            $this->fillResult();
        } catch (Throwable $exception) {
            LogHelper::writeException($exception);
            $this->result->setError($exception->getMessage());
            throw $exception;
        }

        return $this->result;
    }

    protected function jsonEncodeToDb($value): ?string
    {
        return $value || $value === 0 || $value === false ? json_encode($value) : null;
    }
}
