<?php

namespace common\integrations\vk\services;

use common\exceptions\LogicException;
use common\integrations\vk\serviceResults\GetTracksFromAudioDTOListServiceResult;
use common\services\base\ServiceInterface;
use VKSDK\dto\AudioDTO;

class GetTracksFromAudioDTOListService implements ServiceInterface
{

    /**
     * @var AudioDTO[]
     */
    private array $audioDTOList;

    public function __construct(array $audioDTOList)
    {
        $this->audioDTOList = $audioDTOList;
    }

    public function service(): GetTracksFromAudioDTOListServiceResult
    {
        $tracks = [];
        foreach ($this->audioDTOList as $audioDTO) {
            try {
                $service = new GetTrackFromAudioDTOService($audioDTO);
                $tracks[] = $service->service()->track;
            } catch (LogicException $exception) {
                //do nothing
            }
        }

        $result = new GetTracksFromAudioDTOListServiceResult();
        $result->tracks = $tracks;
        return $result;
    }
}