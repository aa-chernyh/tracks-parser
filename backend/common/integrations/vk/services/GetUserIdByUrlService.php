<?php

namespace common\integrations\vk\services;

use common\exceptions\LogicException;
use common\integrations\configs\VkConfig;
use common\integrations\vk\operations\GetUserDataOperation;
use common\integrations\vk\serviceResults\GetUserIdByUrlServiceResult;
use common\services\base\ServiceInterface;
use Throwable;

class GetUserIdByUrlService implements ServiceInterface
{

    /**
     * @var string
     */
    private $userUrl;

    /**
     * @var VkConfig
     */
    private $config;

    public function __construct(string $userUrl, VkConfig $config)
    {
        $this->userUrl = $userUrl;
        $this->config = $config;
    }

    /**
     * @throws LogicException
     * @throws Throwable
     */
    public function service(): GetUserIdByUrlServiceResult
    {
        $result = new GetUserIdByUrlServiceResult();
        if($this->isUserUrlHasNickname()) {
            $userUrlNickname = $this->getUserNicknameFromUrl();
            $operation = new GetUserDataOperation($this->config, $userUrlNickname);
            $operationResult = $operation->service();

            if(!$operationResult->isSuccess || !$operationResult->userDTO->id) {
                throw new LogicException('Ошибка получения данных о пользователе ' . $userUrlNickname);
            }
            $result->userId = $operationResult->userDTO->id;
        } else {
            $result->userId = $this->getUserIdFromUrl();
        }

        return $result;
    }

    private function isUserUrlHasNickname(): bool
    {
        return stripos($this->userUrl, 'https://vk.com/id') === false;
    }

    private function getUserIdFromUrl(): string
    {
        return str_replace('https://vk.com/id', '', $this->userUrl);
    }

    private function getUserNicknameFromUrl(): string
    {
        return str_replace('https://vk.com/', '', $this->userUrl);
    }
}