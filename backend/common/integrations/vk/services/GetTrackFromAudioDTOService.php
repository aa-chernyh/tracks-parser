<?php

namespace common\integrations\vk\services;

use common\exceptions\LogicException;
use common\helpers\CollectionsAdapter;
use common\integrations\vk\serviceResults\GetTrackFromAudioDTOServiceResult;
use common\modelFacades\author\AuthorFacade;
use common\modelFacades\style\StyleFacade;
use common\modelFacades\track\TrackFacade;
use common\models\Author;
use common\models\Style;
use common\models\Track;
use common\services\base\ServiceInterface;
use DateTime;
use common\integrations\common\BaseIntegration;
use Exception;
use PromodjSDK\enums\Styles;
use VKSDK\dto\ArtistDTO;
use VKSDK\dto\AudioDTO;
use VKSDK\enum\Genre;
use yii\helpers\ArrayHelper;

class GetTrackFromAudioDTOService implements ServiceInterface
{

    private const STYLES_MAPPING = [
        Genre::ROCK => Styles::ROCK,
        Genre::POP => Styles::POP,
        Genre::RAP_AND_HIP_HOP => Styles::HIP_HOP,
        Genre::EASY_LISTENING => Styles::EASY_LISTENING,
        Genre::DANCE_AND_HOUSE => Styles::HOUSE,
        Genre::INSTRUMENTAL => Styles::INSTRUMENTAL,
        Genre::METAL => Style::ID_METAL,
        Genre::DUBSTEP => Styles::DUBSTEP,
        Genre::JAZZ_AND_BLUES => Styles::JAZZ,
        Genre::DRUM_AND_BASS => Styles::DNB,
        Genre::TRANCE => Styles::TRANCE,
        Genre::CHANSON => Style::ID_CHANSON,
        Genre::ETHNIC => Styles::ETHNIC,
        Genre::ACOUSTIC_AND_VOCAL => Styles::ACOUSTIC,
        Genre::REGGAE => Styles::REGGAE,
        Genre::CLASSICAL => Style::ID_CLASSICAL,
        Genre::INDIE_POP => Style::ID_INDIE_POP,
        Genre::OTHER => Style::ID_OTHER,
        Genre::SPEECH => Styles::TALK,
        Genre::ALTERNATIVE => Styles::ALTERNATIVE_ROCK,
        Genre::ELECTROPOP_AND_DISCO => Styles::DISCO,
    ];

    private const EXCLUDE_STYLE_LIST = [
        Genre::UNDEFINED_1,
        Genre::UNDEFINED_2,
        Genre::OTHER,
    ];

    private const DEFAULT_EXTENSION = 'mp3';
    private const HQ_BITRATE = 320;

    /**
     * @var StyleFacade[]
     */
    private array $stylesMapping = [];
    private AudioDTO $audioDTO;

    public function __construct(AudioDTO $audioDTO)
    {
        $this->audioDTO = $audioDTO;
    }

    /**
     * @throws LogicException
     */
    public function service(): GetTrackFromAudioDTOServiceResult
    {
        $result = new GetTrackFromAudioDTOServiceResult();
        $result->track = $this->getTrackWithRelations();
        return $result;
    }

    /**
     * @return TrackFacade
     * @throws LogicException
     */
    private function getTrackWithRelations(): TrackFacade
    {
        $track = new TrackFacade(new Track());
        $track->setName($this->getAudioName());
        $track->setDuration($this->audioDTO->duration);
        $track->setDownloadUrl($this->audioDTO->url ?: null);
        $track->setExtension(self::DEFAULT_EXTENSION);
        $track->setReleaseDate($this->getAudioInsertDate());
        $track->setBitrate($this->audioDTO->is_hq ? self::HQ_BITRATE : null);

        $track->setIntegrationId(BaseIntegration::VK);
        $track->setAuthors($this->getAudioAuthors());

        $style = $this->getAudioStyle();
        if($style) {
            $track->setStyles([$style->getStyle()]);
        }

        return $track;
    }

    private function getAudioName(): string
    {
        $name = $this->audioDTO->artist . ' - ' . $this->audioDTO->title;
        if($this->audioDTO->subtitle) {
            $name .= ' (' . $this->audioDTO->subtitle . ')';
        }

        return $name;
    }

    private function getAudioInsertDate(): DateTime
    {
        return (new DateTime())->setTimestamp($this->audioDTO->date);
    }

    /**
     * @return AuthorFacade[]
     */
    private function getAudioAuthors(): array
    {
        /** @var ArtistDTO[] $artists */
        $artists = array_merge($this->audioDTO->main_artists, $this->audioDTO->featured_artists);

        $authors = [];
        foreach ($artists as $artistDTO) {
            $author = new AuthorFacade(new Author());
            $author->setName($artistDTO->name);

            $authors[] = $author;
        }

        return $authors;
    }

    /**
     * @return StyleFacade
     * @throws LogicException
     * @throws Exception
     */
    private function getAudioStyle(): ?StyleFacade
    {
        if($this->audioDTO->genre_id && !in_array($this->audioDTO->genre_id, self::EXCLUDE_STYLE_LIST)) {
            if(!in_array($this->audioDTO->genre_id, array_keys(Genre::MAPPING))) {
                throw new LogicException('Нет стиля в интеграции VK. Id: ' . $this->audioDTO->genre_id);
            }

            if(!in_array($this->audioDTO->genre_id, array_keys(self::STYLES_MAPPING))) {
                throw new LogicException('Нет стиля в маппинге стилей для VK');
            }

            return ArrayHelper::getValue($this->getStylesMapping(), self::STYLES_MAPPING[$this->audioDTO->genre_id]);
        }

        return null;
    }

    /**
     * @return StyleFacade[]
     */
    private function getStylesMapping(): array
    {
        if(!count($this->stylesMapping)) {
            $stylables = CollectionsAdapter::getStyleFacades(Style::find()->all());
            $this->stylesMapping = ArrayHelper::index($stylables, function (StyleFacade $styleReadable) {
                return $styleReadable->getId();
            });
        }

        return $this->stylesMapping;
    }
}