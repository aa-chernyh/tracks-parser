<?php

namespace common\integrations\vk\operations;

use common\exceptions\LogicException;
use common\integrations\configs\VkConfig;
use common\integrations\vk\operationResults\GetUserDataOperationResult;
use VKSDK\exception\AuthException;
use VKSDK\exception\InvalidConfigException;
use VKSDK\request\UsersGetRequest;
use VKSDK\response\UserListResponse;

/**
 * @property UsersGetRequest $request
 * @property UserListResponse $response
 * @property GetUserDataOperationResult $result
 * @method GetUserDataOperationResult service()
 */
class GetUserDataOperation extends VkApiOperation
{

    /**
     * @var string
     */
    private $userUrl;

    public function __construct(VkConfig $config, string $userUrl)
    {
        parent::__construct($config);
        $this->userUrl = $userUrl;
    }

    protected function initResult(): void
    {
        $this->result = new GetUserDataOperationResult();
    }

    protected function buildRequest(): void
    {
        $this->request = new UsersGetRequest();
        $this->request->user_ids = $this->userUrl;
    }

    /**
     * @throws AuthException
     * @throws InvalidConfigException
     * @throws LogicException
     */
    protected function sendRequestInner(): void
    {
        $this->response = $this->getClient()->getUserList($this->request);
    }

    protected function fillResult(): void
    {
        if($this->response->response && count($this->response->response)) {
            $this->result->userDTO = $this->response->response[0];
        } else {
            $this->result = null;
            $this->result->setError('Пользователь не найден');
        }
    }

    protected function getLogType(): string
    {
        return self::TYPE_GET_USER_DATA;
    }
}