<?php

namespace common\integrations\vk\operations;

use common\exceptions\LogicException;
use common\integrations\vk\operationResults\LoadUserRecommendationsOperationResult;
use Exception;
use common\integrations\configs\VkConfig;
use VKSDK\exception\AuthException;
use VKSDK\exception\InvalidConfigException;
use VKSDK\request\GetRecommendationsRequest;
use VKSDK\response\AudioListResponse;
use yii\helpers\ArrayHelper;

/**
 * @property GetRecommendationsRequest $request
 * @property AudioListResponse $response
 * @property LoadUserRecommendationsOperationResult $result
 * @method LoadUserRecommendationsOperationResult service()
 */
class LoadUserRecommendationsOperation extends VkApiOperation
{

    /**
     * @var int
     */
    private $userId;

    public function __construct(VkConfig $config, int $userId)
    {
        parent::__construct($config);
        $this->userId = $userId;
    }

    protected function initResult(): void
    {
        $this->result = new LoadUserRecommendationsOperationResult();
    }

    protected function buildRequest(): void
    {
        $this->request = new GetRecommendationsRequest();
        $this->request->user_id = $this->userId;
        if($this->config->countOfRecommendationTracks) {
            $this->request->count = $this->config->countOfRecommendationTracks;
        }
    }

    /**
     * @throws AuthException
     * @throws InvalidConfigException
     * @throws LogicException
     */
    protected function sendRequestInner(): void
    {
        $this->response = $this->getClient()->getRecommendations($this->request);
    }

    /**
     * @throws Exception
     */
    protected function fillResult(): void
    {
        if($this->response->isSuccess()) {
            $this->result->audios = ArrayHelper::getValue($this->response, 'response.items', []);
        } else {
            $this->result->setError('Рекоммендации пользователя не найдены');
        }
    }

    protected function getLogType(): string
    {
        return self::TYPE_GET_RECOMMENDATIONS;
    }
}