<?php

namespace common\integrations\vk\operations;

use common\exceptions\LogicException;
use common\exceptions\ProjectException;
use common\helpers\LogHelper;
use common\integrations\common\AbstractApiService;
use common\integrations\common\BaseIntegration;
use common\integrations\configs\VkConfig;
use common\integrations\VkIntegration;
use Throwable;
use VKSDK\client\AuthClient;
use VKSDK\client\AuthConfig;
use VKSDK\client\Client;
use VKSDK\client\Config as ClientConfig;
use VKSDK\exception\AuthException;
use VKSDK\exception\InvalidConfigException;
use Yii;

abstract class VkApiOperation extends AbstractApiService
{

    private const CLIENT_CONFIG_CACHE_KEY = 'vk_client_config';

    /**
     * @var VkConfig
     */
    protected $config;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var ClientConfig
     */
    private $clientConfig;

    public function __construct(VkConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @return Client
     * @throws AuthException
     * @throws InvalidConfigException
     * @throws LogicException
     */
    protected function getClient(): Client
    {
        if(!$this->client) {
            $this->client = new Client($this->getClientConfig());
        }

        return $this->client;
    }

    /**
     * @return ClientConfig
     * @throws AuthException
     * @throws InvalidConfigException
     * @throws LogicException
     */
    private function getClientConfig(): ClientConfig
    {
        if($this->clientConfig) {
            return $this->clientConfig;
        }

        $configString = Yii::$app->redis->get(self::CLIENT_CONFIG_CACHE_KEY);
        if($configString) {
            $this->clientConfig = ClientConfig::createFromString($configString);
            if(!$this->clientConfig) {
                throw new LogicException('Ошибка получения конфигурации клиента ВК из кеша');
            }

            return $this->clientConfig;
        }

        $authConfig = new AuthConfig($this->config->login, $this->config->password);
        $authClient = new AuthClient($authConfig);
        $this->clientConfig = $authClient->getClientConfig();
        Yii::$app->redis->set(self::CLIENT_CONFIG_CACHE_KEY, $this->clientConfig->toString());
        return $this->clientConfig;
    }

    /**
     * @throws AuthException
     * @throws InvalidConfigException
     * @throws Throwable
     */
    protected function sendRequest(): void
    {
        try {
            // Выполняем только один запрос к сервису из всех потоков,
            // иначе может прилететь ошибка "Too many requests per second"
            Yii::$app->mutex->lock(VkIntegration::REQUEST_LOCK_KEY, 30);
            $this->sendRequestInner();
        } catch (ProjectException $exception) {
            //do nothing
        } catch (Throwable $exception) {
            LogHelper::writeException($exception);
        } finally {
            Yii::$app->mutex->unlock(VkIntegration::REQUEST_LOCK_KEY);

            LogHelper::writeIntegration(
                $this->getLogType(),
                BaseIntegration::VK,
                $this->response ? $this->response->isSuccess() : false,
                $this->getClient()->getLastUrl(),
                $this->jsonEncodeToDb($this->getClient()->getLastRequest()),
                $this->jsonEncodeToDb($this->getClient()->getLastResponse()),
                $this->jsonEncodeToDb($this->getClient()->getLastRequestHeaders()),
                $this->jsonEncodeToDb($this->getClient()->getLastResponseHeaders()),
                $this->response ? $this->response->getErrors() : null,
            );
        }
    }

    abstract protected function sendRequestInner(): void;

    protected function calculate(): void {}
}