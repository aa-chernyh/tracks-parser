<?php

namespace common\integrations\vk\operations;

use common\exceptions\LogicException;
use common\integrations\vk\operationResults\LoadPopularOperationResult;
use VKSDK\exception\AuthException;
use VKSDK\exception\InvalidConfigException;
use VKSDK\request\GetPopularRequest;
use VKSDK\response\GetPopularResponse;
use yii\helpers\ArrayHelper;

/**
 * @property GetPopularRequest $request
 * @property GetPopularResponse $response
 * @property LoadPopularOperationResult $result
 * @method LoadPopularOperationResult service()
 */
class LoadPopularOperation extends VkApiOperation
{

    protected function initResult(): void
    {
        $this->result = new LoadPopularOperationResult();
    }

    protected function buildRequest(): void
    {
        $this->request = new GetPopularRequest();
        if($this->config->countOfPopularTracks) {
            $this->request->count = $this->config->countOfPopularTracks;
        }
    }

    /**
     * @throws AuthException
     * @throws InvalidConfigException
     * @throws LogicException
     */
    protected function sendRequestInner(): void
    {
        $this->response = $this->getClient()->getPopular($this->request);
    }

    protected function fillResult(): void
    {
        $this->result->audios = ArrayHelper::getValue($this->response, 'response', []);
    }

    protected function getLogType(): string
    {
        return self::TYPE_GET_POPULAR;
    }
}