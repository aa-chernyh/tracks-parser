<?php

namespace common\integrations\vk\operations;

use common\exceptions\LogicException;
use common\integrations\vk\operationResults\LoadUserTrackListOperationResult;
use Exception;
use common\integrations\configs\VkConfig;
use VKSDK\exception\AuthException;
use VKSDK\exception\InvalidConfigException;
use VKSDK\request\AudioGetRequest;
use VKSDK\response\AudioListResponse;
use yii\helpers\ArrayHelper;

/**
 * @property AudioGetRequest $request
 * @property AudioListResponse $response
 * @property LoadUserTrackListOperationResult $result
 * @method LoadUserTrackListOperationResult service()
 */
class LoadUserTrackListOperation extends VkApiOperation
{

    /**
     * @var string
     */
    private $userId;

    public function __construct(VkConfig $config, int $userId)
    {
        parent::__construct($config);
        $this->userId = $userId;
    }

    protected function initResult(): void
    {
        $this->result = new LoadUserTrackListOperationResult();
    }

    protected function buildRequest(): void
    {
        $this->request = new AudioGetRequest();
        $this->request->setOwnerId($this->userId);
    }

    /**
     * @throws AuthException
     * @throws InvalidConfigException
     * @throws LogicException
     */
    protected function sendRequestInner(): void
    {
        $this->response = $this->getClient()->getAudioList($this->request);
    }

    /**
     * @throws Exception
     */
    protected function fillResult(): void
    {
        if($this->response->isSuccess()) {
            $this->result->audios = ArrayHelper::getValue($this->response, 'response.items', []);
        } else {
            $this->result->setError('Ошибка загрузки треклиста пользователя');
        }
    }

    protected function getLogType(): string
    {
        return self::TYPE_GET_USER_AUDIO_LIST;
    }
}