<?php

namespace common\integrations\vk\serviceResults;

use common\modelFacades\track\TrackFacade;
use common\services\base\BaseServiceResult;

class GetTracksFromAudioDTOListServiceResult extends BaseServiceResult
{
    /**
     * @var TrackFacade[]
     */
    public array $tracks = [];
}