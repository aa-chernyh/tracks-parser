<?php

namespace common\integrations\vk\serviceResults;

use common\services\base\BaseServiceResult;

class GetUserIdByUrlServiceResult extends BaseServiceResult
{
    public ?string $userId = null;
}