<?php

namespace common\integrations\vk\serviceResults;

use common\modelFacades\track\TrackFacade;
use common\services\base\BaseServiceResult;

class GetTrackFromAudioDTOServiceResult extends BaseServiceResult
{
    public ?TrackFacade $track = null;
}