<?php

namespace common\integrations\vk\jobs;

use common\integrations\VkIntegration;
use common\jobs\BaseJob;
use Throwable;
use yii\base\InvalidConfigException;

class LoadUserAudioListJob extends BaseJob
{

    /**
     * @var string
     */
    public $userUrl;

    /**
     * @throws Throwable
     * @throws InvalidConfigException
     */
    public function run(): void
    {
        $integration = new VkIntegration();
        $integration->loadUserAudioList($this->userUrl);
    }
}