<?php

namespace common\integrations\vk\jobs;

use common\integrations\VkIntegration;
use common\jobs\BaseJob;
use Throwable;
use yii\base\InvalidConfigException;

class LoadPopularJob extends BaseJob
{

    /**
     * @throws Throwable
     * @throws InvalidConfigException
     */
    public function run(): void
    {
        $integration = new VkIntegration();
        $integration->loadPopular();
    }
}