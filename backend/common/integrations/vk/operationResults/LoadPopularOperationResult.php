<?php

namespace common\integrations\vk\operationResults;

use common\services\base\BaseServiceResult;
use VKSDK\dto\AudioDTO;

class LoadPopularOperationResult extends BaseServiceResult
{
    /**
     * @var AudioDTO[]
     */
    public array $audios = [];
}