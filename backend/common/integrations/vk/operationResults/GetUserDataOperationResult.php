<?php

namespace common\integrations\vk\operationResults;

use common\services\base\BaseServiceResult;
use VKSDK\dto\UserDTO;

class GetUserDataOperationResult extends BaseServiceResult
{
    public ?UserDTO $userDTO = null;
}
