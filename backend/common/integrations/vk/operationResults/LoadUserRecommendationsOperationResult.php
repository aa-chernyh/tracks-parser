<?php

namespace common\integrations\vk\operationResults;

use common\services\base\BaseServiceResult;
use VKSDK\dto\AudioDTO;

class LoadUserRecommendationsOperationResult extends BaseServiceResult
{
    /**
     * @var AudioDTO[]
     */
    public array $audios = [];
}