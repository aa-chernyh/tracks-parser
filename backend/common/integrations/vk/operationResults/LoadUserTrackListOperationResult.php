<?php

namespace common\integrations\vk\operationResults;

use common\services\base\BaseServiceResult;
use VKSDK\dto\AudioDTO;

class LoadUserTrackListOperationResult extends BaseServiceResult
{
    /**
     * @var AudioDTO[]
     */
    public array $audios = [];
}