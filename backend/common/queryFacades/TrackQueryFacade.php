<?php

namespace common\queryFacades;

use common\helpers\CollectionsAdapter;
use common\modelFacades\track\TrackFacade;
use common\models\Track;
use yii\base\InvalidConfigException;
use yii\db\ActiveQueryInterface;

class TrackQueryFacade
{

    /**
     * @param ActiveQueryInterface $query
     * @return TrackFacade[]
     */
    public function getByQuery(ActiveQueryInterface $query): array
    {
        return CollectionsAdapter::getTrackFacades($query->all());
    }

    /**
     * @return float
     * @throws InvalidConfigException
     */
    public function getMinimumDuration(): float
    {
        return Track::find()->hasDuration()->min('duration') ?? 0;
    }

    /**
     * @return float
     * @throws InvalidConfigException
     */
    public function getMaximumDuration(): float
    {
        return Track::find()->hasDuration()->max('duration') ?? 0;
    }

    /**
     * @return string[]
     * @throws InvalidConfigException
     */
    public function getExtensionsList(): array
    {
        return Track::find()->hasExtension()->select('extension')->distinct()->column();
    }

    /**
     * @return int
     * @throws InvalidConfigException
     */
    public function getMinimumBPM(): int
    {
        return Track::find()->hasBPM()->min('bpm') ?? 0;
    }

    /**
     * @return int
     * @throws InvalidConfigException
     */
    public function getMaximumBPM(): int
    {
        return Track::find()->hasBPM()->max('bpm') ?? 0;
    }

    /**
     * @return float
     * @throws InvalidConfigException
     */
    public function getMinimumSize(): float
    {
        return Track::find()->hasSize()->min('size') ?? 0;
    }

    /**
     * @return float
     * @throws InvalidConfigException
     */
    public function getMaximumSize(): float
    {
        return Track::find()->hasSize()->max('size') ?? 0;
    }

    public function getPathById(int $id): ?string
    {
        /** @var Track|null $track */
        $track = Track::find()->whereIdIs($id)->onlyPath()->limit(1)->one();
        if(!$track) {
            return null;
        }

        return (new TrackFacade($track))->getPath();
    }
}