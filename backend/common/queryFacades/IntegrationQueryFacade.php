<?php

namespace common\queryFacades;

use common\helpers\CollectionsAdapter;
use common\modelFacades\integration\IntegrationFacade;
use common\models\Integration;

class IntegrationQueryFacade
{

    /**
     * @return IntegrationFacade[]
     */
    public function getAll(): array
    {
        $integrations = Integration::find()->all();
        return CollectionsAdapter::getIntegrationFacades($integrations);
    }
}