<?php

namespace common\modelFacades\author;

use common\modelFacades\track\TrackFacade;
use common\models\Author;

interface AuthorReadableInterface
{
    public function getAuthor(): Author;
    public function getId(): ?int;
    public function getName(): ?string;
    public function getUrl(): ?string;
    public function getRealName(): ?string;
    public function getUnique(): self;

    /** @return TrackFacade[] */
    public function getTracks(): array;
}