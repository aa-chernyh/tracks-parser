<?php

namespace common\modelFacades\author;

use common\helpers\CollectionsAdapter;
use common\modelFacades\track\TrackFacade;
use common\models\Author;

class AuthorFacade implements AuthorReadableInterface
{

    private Author $author;

    public function __construct(Author $author)
    {
        $this->author = $author;
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function getId(): ?int
    {
        return $this->author->id;
    }

    public function getName(): ?string
    {
        return $this->author->name;
    }

    public function getUrl(): ?string
    {
        return $this->author->url;
    }

    public function getRealName(): ?string
    {
        return $this->author->real_name;
    }
    
    public function getUnique(): self
    {
        $author = Author::findOne(['name' => $this->getName()]);
        if($author) {
            $this->author = $author;
        }
        
        return $this;
    }

    /**
     * @return TrackFacade[]
     */
    public function getTracks(): array
    {
        return CollectionsAdapter::getTrackFacades($this->author->tracks ?? []);
    }

    public function setName(?string $name): void
    {
        $this->author->name = $name;
    }

    public function setUrl(?string $url): void
    {
        $this->author->url = $url;
    }

    public function setRealName(?string $realName): void
    {
        $this->author->real_name = $realName;
    }
}