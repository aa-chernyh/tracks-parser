<?php

namespace common\modelFacades\style;

use common\helpers\CollectionsAdapter;
use common\modelFacades\track\TrackFacade;
use common\models\Style;

class StyleFacade implements StyleReadableInterface
{

    private Style $style;

    public function __construct(Style $style)
    {
        $this->style = $style;
    }

    public function getStyle(): Style
    {
        return $this->style;
    }

    public function getId(): string
    {
        return $this->style->id;
    }

    public function getName(): string
    {
        return $this->style->name;
    }

    /**
     * @return TrackFacade[]
     */
    public function getTracks(): array
    {
        return CollectionsAdapter::getTrackFacades($this->style->tracks ?? []);
    }
}