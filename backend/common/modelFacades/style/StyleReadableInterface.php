<?php

namespace common\modelFacades\style;

use common\modelFacades\track\TrackFacade;;
use common\models\Style;

interface StyleReadableInterface
{
    public function getStyle(): Style;
    public function getId(): string;
    public function getName(): string;

    /** @return TrackFacade[] */
    public function getTracks(): array;
}