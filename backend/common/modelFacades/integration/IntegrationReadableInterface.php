<?php

namespace common\modelFacades\integration;

use common\integrations\configs\AbstractConfig;
use common\modelFacades\track\TrackFacade;
use common\models\Integration;

interface IntegrationReadableInterface
{
    public function getIntegration(): Integration;
    public function getId(): string;
    public function getName(): string;
    public function getDescription(): string;
    public function getConfiguration(): ?string;
    public function getConfigurationObject(): ?AbstractConfig;

    /** @return TrackFacade[] */
    public function getTracks(): array;
}