<?php

namespace common\modelFacades\integration;

use ClientInterface\Base\PhpDocReader\AnnotationException;
use common\helpers\CollectionsAdapter;
use common\helpers\IntegrationConfigurationHelper;
use common\integrations\configs\AbstractConfig;
use common\modelFacades\track\TrackFacade;
use common\models\Integration;
use ReflectionException;
use yii\base\InvalidConfigException;

class IntegrationFacade implements IntegrationReadableInterface
{

    private Integration $integration;

    public function __construct(Integration $integration)
    {
        $this->integration = $integration;
    }

    public function getIntegration(): Integration
    {
        return $this->integration;
    }

    public function getId(): string
    {
        return $this->integration->id;
    }

    public function getName(): string
    {
        return $this->integration->name;
    }

    public function getDescription(): string
    {
        return $this->integration->description;
    }

    public function getConfiguration(): ?string
    {
        return $this->integration->configuration;
    }

    /**
     * @return AbstractConfig
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public function getConfigurationObject(): AbstractConfig
    {
        return IntegrationConfigurationHelper::convertJsonToConfigurationObject($this->getId(), $this->getConfiguration());
    }

    /**
     * @return TrackFacade[]
     */
    public function getTracks(): array
    {
        return CollectionsAdapter::getTrackFacades($this->integration->tracks ?? []);
    }

    public function setConfiguration(string $json): void
    {
        $this->integration->configuration = $json;
    }

    public function setConfigurationObject(AbstractConfig $config): void
    {
        $this->setConfiguration(IntegrationConfigurationHelper::convertConfigurationObjectToJson($config));
    }
}