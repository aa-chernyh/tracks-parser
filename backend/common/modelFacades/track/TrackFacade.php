<?php

namespace common\modelFacades\track;

use common\modelFacades\author\AuthorFacade;
use common\helpers\CollectionsAdapter;
use common\modelFacades\integration\IntegrationFacade;
use common\helpers\Utils;
use common\modelFacades\style\StyleFacade;
use common\models\Author;
use common\models\Style;
use common\models\Track;
use common\services\results\SaveTrackWithRelationsServiceResult;
use common\services\results\ValidateTrackWithRelationsServiceResult;
use common\services\SaveTrackWithRelationsService;
use common\services\SetTrackGeneratedParamsService;
use common\services\ValidateTrackWithRelationsService;
use DateTime;

class TrackFacade implements TrackReadableInterface
{

    public const MIN_TRACK_FILE_SIZE = 200000; // 200 КБайт

    private Track $track;

    public function __construct(Track $track)
    {
        $this->track = $track;
    }

    public function getTrack(): Track
    {
        return $this->track;
    }

    public function getId(): ?int
    {
        return $this->track->id;
    }

    public function getIntegrationId(): ?string
    {
        return $this->track->integration;
    }

    public function getName(): ?string
    {
        return $this->track->name;
    }

    public function getPath(): ?string
    {
        return $this->track->path;
    }

    public function getExtension(): ?string
    {
        return $this->track->extension;
    }

    public function getPageUrl(): ?string
    {
        return $this->track->page_url;
    }

    public function getDownloadUrl(): ?string
    {
        return $this->track->download_url;
    }

    public function getDuration(): ?float
    {
        return $this->track->duration;
    }

    public function getBPM(): ?int
    {
        return $this->track->bpm;
    }

    public function getKey(): ?string
    {
        return $this->track->key;
    }

    public function getSize(): ?float
    {
        return $this->track->size;
    }

    public function getReleaseDate(): ?DateTime
    {
        return Utils::getDateTimeByDbDateString($this->track->release_date);
    }

    public function getBitrate(): ?int
    {
        return $this->track->bitrate;
    }

    public function getInsertDate(): ?DateTime
    {
        return Utils::getDateTimeByDbDateString($this->track->insert_date);
    }

    public function isDownloaded(): bool
    {
        return $this->track->path && file_exists($this->track->path) && filesize($this->track->path) > self::MIN_TRACK_FILE_SIZE;
    }

    /**
     * @return AuthorFacade[]
     */
    public function getAuthors(): array
    {
        return CollectionsAdapter::getAuthorFacades($this->track->authors ?? []);
    }

    /**
     * @return StyleFacade[]
     */
    public function getStyles(): array
    {
        return CollectionsAdapter::getStyleFacades($this->track->styles ?? []);
    }

    public function getIntegration(): IntegrationFacade
    {
        return new IntegrationFacade($this->track->integrationAR);
    }

    public function setIntegrationId(string $id): void
    {
        $this->track->integration = $id;
    }

    public function setName(?string $name): void
    {
        $this->track->name = $name;
    }

    public function setExtension(?string $extension): void
    {
        $this->track->extension = $extension;
    }

    public function setPageUrl(?string $pageUrl): void
    {
        $this->track->page_url = $pageUrl;
    }

    public function setDownloadUrl(?string $downloadUrl): void
    {
        $this->track->download_url = $downloadUrl;
    }

    public function setDuration(?float $duration): void
    {
        $this->track->duration = $duration;
    }

    public function setBPM(?int $BPM): void
    {
        $this->track->bpm = $BPM;
    }

    public function setKey(?string $key): void
    {
        $this->track->key = $key;
    }

    public function setSize(?float $size): void
    {
        $this->track->size = $size;
    }

    public function setReleaseDate(?DateTime $dateTime): void
    {
        $this->track->release_date = Utils::getDbDateTimeStringByDateTime($dateTime);
    }

    public function setBitrate(?int $bitrate): void
    {
        $this->track->bitrate = $bitrate;
    }

    /**
     * @param AuthorFacade[] $authors
     */
    public function setAuthors(array $authors): void
    {
        $this->track->populateRelation('authors', CollectionsAdapter::getAuthors($authors));
    }

    /**
     * @param Style[] $styles
     */
    public function setStyles(array $styles): void
    {
        $this->track->populateRelation('styles', $styles);
    }

    public function validateWithRelations(): ValidateTrackWithRelationsServiceResult
    {
        $service = new ValidateTrackWithRelationsService(new TrackFacade($this->track));
        return $service->service();
    }

    public function saveWithRelations(): SaveTrackWithRelationsServiceResult
    {
        $service = new SaveTrackWithRelationsService(new TrackFacade($this->track));
        return $service->service();
    }

    public function setGeneratedParams(): void
    {
        $setTrackGeneratedParamsService = new SetTrackGeneratedParamsService($this->track);
        $this->track = $setTrackGeneratedParamsService->service()->track;
    }

    public function linkAuthor(Author $author): void
    {
        $this->track->link('authors', $author);
    }

    public function linkStyle(Style $style): void
    {
        $this->track->link('styles', $style);
    }
}