<?php

namespace common\modelFacades\track;

use common\modelFacades\author\AuthorFacade;
use common\modelFacades\integration\IntegrationFacade;
use common\modelFacades\style\StyleFacade;
use common\models\Track;
use DateTime;

interface TrackReadableInterface
{
    public function getTrack(): Track;
    public function getId(): ?int;
    public function getIntegrationId(): ?string;
    public function getName(): ?string;
    public function getPath(): ?string;
    public function getExtension(): ?string;
    public function getPageUrl(): ?string;
    public function getDownloadUrl(): ?string;
    public function getDuration(): ?float;
    public function getBPM(): ?int;
    public function getKey(): ?string;
    public function getSize(): ?float;
    public function getReleaseDate(): ?DateTime;
    public function getBitrate(): ?int;
    public function getInsertDate(): ?DateTime;
    public function isDownloaded(): bool;

    /** @return AuthorFacade[] */
    public function getAuthors(): array;
    /** @return StyleFacade[] */
    public function getStyles(): array;
    public function getIntegration(): IntegrationFacade;
}