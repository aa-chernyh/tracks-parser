<?php

namespace common\services;

use common\modelFacades\track\TrackFacade;
use common\services\base\ServiceInterface;
use common\services\results\ValidateTrackWithRelationsServiceResult;

class ValidateTrackWithRelationsService implements ServiceInterface
{

    private TrackFacade $track;
    private ValidateTrackWithRelationsServiceResult $result;

    public function __construct(TrackFacade $track)
    {
        $this->track = $track;
        $this->result = new ValidateTrackWithRelationsServiceResult();
    }

    public function service(): ValidateTrackWithRelationsServiceResult
    {
        $this->validateTrack();
        $this->validateAuthors();

        return $this->result;
    }

    private function validateTrack(): void
    {
        $trackModel = $this->track->getTrack();
        if(!$trackModel->validate()) {
            $this->result->setModelErrors('track', $trackModel->getErrors());
        }
    }

    private function validateAuthors(): void
    {
        foreach ($this->track->getAuthors() as $authorable) {
            $author = $authorable->getAuthor();
            if(!$author->validate()) {
                $this->result->setModelErrors('author', $author->getErrors());
            }
        }
    }
}