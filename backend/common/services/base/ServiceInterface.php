<?php

namespace common\services\base;

interface ServiceInterface
{
    public function service(): ServiceResultInterface;
}