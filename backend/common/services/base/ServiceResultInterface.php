<?php

namespace common\services\base;

interface ServiceResultInterface
{
    public function getIsSuccess(): bool;
    public function getErrors(): array;
    public function setError(string $message): void;
    public function setErrors(array $errors): void;
}