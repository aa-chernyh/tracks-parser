<?php

namespace common\services\base;

abstract class BaseServiceResult implements ServiceResultInterface
{
    /** @var string[] */
    public array $errors = [];
    public bool $isSuccess = true;

    public function getIsSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function setError(string $message): void
    {
        $this->isSuccess = false;
        $this->errors[] = $message;
    }

    public function setErrors(array $errors): void
    {
        foreach ($errors as $error) {
            $this->setError($error);
        }
    }
}