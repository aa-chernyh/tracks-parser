<?php

namespace common\services;

use common\modelFacades\track\TrackFacade;
use common\services\base\ServiceInterface;
use common\services\results\SaveTrackListWithRelationsServiceResult;

class SaveTrackListWithRelationsService implements ServiceInterface
{

    /**
     * @var TrackFacade[]
     */
    private array $tracks;

    /**
     * @param TrackFacade[] $tracks
     */
    public function __construct(array $tracks)
    {
        $this->tracks = $tracks;
    }

    public function service(): SaveTrackListWithRelationsServiceResult
    {
        foreach ($this->tracks as $track) {
            $track->setGeneratedParams();

            $validationResult = $track->validateWithRelations();
            if($validationResult->getIsSuccess()) {
                $track->saveWithRelations();
            }
        }

        return new SaveTrackListWithRelationsServiceResult();
    }
}