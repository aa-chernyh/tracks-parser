<?php

namespace common\services;

use common\modelFacades\track\TrackFacade;
use common\services\base\ServiceInterface;
use common\services\results\SaveTrackWithRelationsServiceResult;
use Throwable;
use Yii;

class SaveTrackWithRelationsService implements ServiceInterface
{

    private TrackFacade $track;

    public function __construct(TrackFacade $track)
    {
        $this->track = $track;
    }

    public function service(): SaveTrackWithRelationsServiceResult
    {
        $result = new SaveTrackWithRelationsServiceResult();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->track->getTrack()->save(false);
            $this->saveAndLinkAuthors();
            $this->linkStyles();
            $transaction->commit();
            return $result;
        } catch (Throwable $exception) {
            $transaction->rollBack();

            $result->setError($exception->getMessage());
            return $result;
        }
    }

    private function saveAndLinkAuthors(): void
    {
        foreach ($this->track->getAuthors() as $authorReadable) {
            $authorReadable = $authorReadable->getUnique();

            $author = $authorReadable->getAuthor();
            if($author->getIsNewRecord()) {
                $author->save(false);
            }

            $this->track->linkAuthor($authorReadable->getAuthor());
        }
    }

    private function linkStyles(): void
    {
        foreach ($this->track->getStyles() as $styleReadable) {
            $this->track->linkStyle($styleReadable->getStyle());
        }
    }
}