<?php

namespace common\services;

use common\modelFacades\track\TrackReadableInterface;
use common\modelFacades\track\TrackFacade;
use common\models\Track;
use common\services\base\ServiceInterface;
use common\services\results\GetUniqueTracksServiceResult;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class GetUniqueTracksService implements ServiceInterface
{

    /**
     * @var TrackFacade[]
     */
    private array $tracks;

    /**
     * @param TrackFacade[] $trackReadables
     */
    public function __construct(array $trackReadables)
    {
        $this->tracks = $trackReadables;
    }

    /**
     * @throws InvalidConfigException
     */
    public function service(): GetUniqueTracksServiceResult
    {
        $indexedTrackReadables = ArrayHelper::index($this->tracks, function(TrackReadableInterface $trackReadable) {
            return $trackReadable->getName();
        });
        $existsTrackNames = Track::find()->select(['name'])->where(['name' => array_keys($indexedTrackReadables)])->column();

        $result = new GetUniqueTracksServiceResult();
        foreach ($indexedTrackReadables as $name => $track) {
            if(!in_array($name, $existsTrackNames)) {
                $result->tracks[] = $track;
            }
        }

        return $result;
    }
}