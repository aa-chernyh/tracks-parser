<?php

namespace common\services;

use common\helpers\DownloadHelper;
use common\helpers\Utils;
use common\models\Track;
use common\services\base\ServiceInterface;
use common\services\results\SetTrackGeneratedParamsServiceResult;
use DateTime;

class SetTrackGeneratedParamsService implements ServiceInterface
{

    private Track $track;

    public function __construct(Track $track)
    {
        $this->track = $track;
    }

    public function service(): SetTrackGeneratedParamsServiceResult
    {
        $this->track->path = $this->getFilePath();
        $this->track->insert_date = $this->getInsertDate();

        return new SetTrackGeneratedParamsServiceResult($this->track);
    }

    private function getFilePath(): ?string
    {
        if ($this->track->name && $this->track->integration && $this->track->extension) {
            return
                DownloadHelper::getTracksPath() .
                $this->track->integration . DIRECTORY_SEPARATOR .
                Utils::escapeString($this->track->name) . '.' . $this->track->extension;
        }

        return null;
    }

    private function getInsertDate(): string
    {
        return Utils::getDbDateTimeStringByDateTime(new DateTime());
    }
}