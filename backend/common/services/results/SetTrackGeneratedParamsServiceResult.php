<?php

namespace common\services\results;

use common\models\Track;
use common\services\base\BaseServiceResult;

class SetTrackGeneratedParamsServiceResult extends BaseServiceResult
{
    public Track $track;

    public function __construct(Track $track)
    {
        $this->track = $track;
    }
}