<?php

namespace common\services\results\DTO;

class ModelErrors
{
    public string $model;
    public array $attributes;

    public static function getSelf(string $model, array $errors): self
    {
        $dto = new self();
        $dto->model = $model;
        foreach ($errors as $attribute => $messages) {
            $dto->attributes[] = AttributeErrorsDTO::getSelf($attribute, $messages);
        }

        return $dto;
    }
}