<?php

namespace common\services\results\DTO;

class AttributeErrorsDTO
{
    public string $attribute;
    /** @var string[] */
    public array $messages;

    /**
     * @param string $attribute
     * @param string[] $messages
     * @return static
     */
    public static function getSelf(string $attribute, array $messages): self
    {
        $dto = new self();
        $dto->attribute = $attribute;
        $dto->messages = $messages;

        return $dto;
    }
}