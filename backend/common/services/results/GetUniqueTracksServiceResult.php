<?php

namespace common\services\results;

use common\modelFacades\track\TrackFacade;
use common\services\base\BaseServiceResult;

class GetUniqueTracksServiceResult extends BaseServiceResult
{
    /**
     * @var TrackFacade[]
     */
    public array $tracks = [];
}