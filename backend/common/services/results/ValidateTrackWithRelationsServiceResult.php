<?php

namespace common\services\results;

use common\services\base\BaseServiceResult;
use common\services\results\DTO\ModelErrors;

class ValidateTrackWithRelationsServiceResult extends BaseServiceResult
{

    /**
     * @var ModelErrors[]
     */
    private array $validationErrors = [];

    /**
     * @return ModelErrors[]
     */
    public function getModelErrors(): array
    {
        return $this->validationErrors;
    }

    public function setModelErrors(string $model, array $errors): void
    {
        $this->validationErrors[] = ModelErrors::getSelf($model, $errors);
        $this->isSuccess = false;
    }
}