<?php

namespace common\services;

use common\modelFacades\track\TrackReadableInterface;
use common\models\Track;
use common\models\TrackToAuthor;
use common\models\TrackToStyle;
use common\services\base\ServiceInterface;
use common\services\results\RemoveTracksServiceResult;

class RemoveTracksService implements ServiceInterface
{

    /** @var int[] */
    private array $trackIds;

    /**
     * @param TrackReadableInterface[] $trackReadables
     */
    public function __construct(array $trackReadables)
    {
        $this->trackIds = $this->getTrackIds($trackReadables);
    }

    /**
     * @param TrackReadableInterface[] $trackReadables
     * @return int[]
     */
    private function getTrackIds(array $trackReadables): array
    {
        $ids = [];
        foreach ($trackReadables as $trackReadable) {
            if($trackReadable->getId()) {
                $ids[] = $trackReadable->getId();
            }
        }

        return $ids;
    }

    public function service(): RemoveTracksServiceResult
    {
        TrackToStyle::deleteAll(['track_id' => $this->trackIds]);
        TrackToAuthor::deleteAll(['track_id' => $this->trackIds]);
        Track::deleteAll(['id' => $this->trackIds]);

        return new RemoveTracksServiceResult();
    }
}