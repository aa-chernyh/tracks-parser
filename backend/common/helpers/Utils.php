<?php

namespace common\helpers;

use common\exceptions\LogicException;
use \DateTime;
use Exception;
use Yii;
use ZipArchive;

class Utils
{

    private const DEFAULT_DB_DATETIME_FORMAT = 'Y-m-d H:i:s';
    private const DEFAULT_API_DATETIME_FORMAT = 'd.m.Y';

    /**
     * @param string|null $dateTime
     * @return DateTime|null
     */
    public static function getDateTimeByDbDateString(?string $dateTime): ?DateTime
    {
        if($dateTime) {
            return DateTime::createFromFormat(self::getDefaultDbDatetimeFormat(), $dateTime);
        }

        return null;
    }

    /**
     * @param DateTime|null $dateTime
     * @return string|null
     */
    public static function getDbDateTimeStringByDateTime(?DateTime $dateTime): ?string
    {
        if($dateTime) {
            return $dateTime->format(self::DEFAULT_DB_DATETIME_FORMAT);
        }

        return null;
    }

    /**
     * @param string|null $date
     * @return DateTime|null
     */
    public static function getDateTimeByApiDateString(?string $date): ?DateTime
    {
        if($date) {
            return DateTime::createFromFormat(self::getDefaultApiDateFormat(), $date);
        }

        return null;
    }

    /**
     * @param DateTime|null $dateTime
     * @return string|null
     */
    public static function getApiDateStringByDateTime(?DateTime $dateTime): ?string
    {
        if($dateTime) {
            return $dateTime->format(self::getDefaultApiDateFormat());
        }

        return null;
    }

    /**
     * @param $path
     */
    public static function checkFolder($path) {
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        if($ext && preg_match("/^[A-z]+\d*$/", $ext)) {
            $path = dirname($path);
        }

        if(!is_dir($path)) {
            umask(0000);
            mkdir($path, 0777, true);
        }
    }

    /**
     * @return string
     */
    public static function getDefaultDbDatetimeFormat(): string
    {
        return self::DEFAULT_DB_DATETIME_FORMAT;
    }

    public static function getDefaultApiDateFormat(): string
    {
        return self::DEFAULT_API_DATETIME_FORMAT;
    }

    /**
     * @param string $string
     * @return string
     */
    public static function escapeString(string $string): string
    {
        $escapedSymbols = ['/', '\\', '"', '\'', '`'];
        foreach ($escapedSymbols as $escapedSymbol) {
            $string = str_replace($escapedSymbol, ' ', $string);
        }

        return $string;
    }

    /**
     * @param int $year
     * @return DateTime
     */
    public static function createDateTimeFromYear(int $year): DateTime
    {
        return DateTime::createFromFormat('d.m.Y H:i:s', "01.01.$year 00:00:00");
    }

    /**
     * @param string|null $datetime
     * @return DateTime
     * @throws Exception
     */
    public static function createDateTimeWithNullTime(?string $datetime = 'now'): DateTime
    {
        return DateTime::createFromFormat('d.m.Y H:i:s', (new DateTime($datetime))->format('d.m.Y') . ' 00:00:00');
    }

    /**
     * @param string|string[] $stringOrArray
     * @return string|string[]
     */
    public static function trim($stringOrArray)
    {
        if(!$stringOrArray) {
            return $stringOrArray;
        }

        if(is_array($stringOrArray)) {
            $result = [];
            foreach ($stringOrArray as $string) {
                $result[] = str_replace(' ', '', $string);
            }
            return $result;
        }

        return str_replace(' ', '', $stringOrArray);
    }

    /**
     * @param string[] $filePaths
     * @param string $filePrefix
     * @return string zip path
     * @throws LogicException
     */
    public static function createZip(array $filePaths, string $filePrefix = ''): string
    {
        $archivePath = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $filePrefix . uniqid() . '.zip';
        Utils::checkFolder($archivePath);

        $zip = new ZipArchive();
        if($zip->open($archivePath, ZipArchive::CREATE) !== true) {
            throw new LogicException('Невозможно создать архив');
        }

        foreach ($filePaths as $path) {
            if(!is_file($path)) {
                throw new LogicException('Файл ' . $path . ' не существует');
            }
            $zip->addFile($path, basename($path));
        }

        $zip->close();
        return $archivePath;
    }
    
}
