<?php

namespace common\helpers;

use common\modelFacades\author\AuthorFacade;
use common\modelFacades\integration\IntegrationFacade;
use common\modelFacades\style\StyleFacade;
use common\modelFacades\track\TrackFacade;
use common\models\Author;
use common\models\Integration;
use common\models\Style;
use common\models\Track;
use yii\db\BatchQueryResult;

class CollectionsAdapter
{

    /**
     * @param Track[]|BatchQueryResult $tracks
     * @return TrackFacade[]
     */
    public static function getTrackFacades(array $tracks): array
    {
        return array_map(function (Track $track) {
            return new TrackFacade($track);
        }, $tracks);
    }

    /**
     * @param Author[] $authors
     * @return AuthorFacade[]
     */
    public static function getAuthorFacades(array $authors): array
    {
        return array_map(function (Author $author) {
            return new AuthorFacade($author);
        }, $authors);
    }

    /**
     * @param Style[] $styles
     * @return StyleFacade[]
     */
    public static function getStyleFacades(array $styles): array
    {
        return array_map(function (Style $style) {
            return new StyleFacade($style);
        }, $styles);
    }

    /**
     * @param Integration[] $integrations
     * @return IntegrationFacade[]
     */
    public static function getIntegrationFacades(array $integrations): array
    {
        return array_map(function (Integration $integration) {
            return new IntegrationFacade($integration);
        }, $integrations);
    }

    /**
     * @param TrackFacade[] $trackFacades
     * @return Track[]
     */
    public static function getTracks(array $trackFacades): array
    {
        return array_map(function (TrackFacade $trackFacade) {
            return $trackFacade->getTrack();
        }, $trackFacades);
    }

    /**
     * @param AuthorFacade[] $authorFacades
     * @return Author[]
     */
    public static function getAuthors(array $authorFacades): array
    {
        return array_map(function (AuthorFacade $authorFacade) {
            return $authorFacade->getAuthor();
        }, $authorFacades);
    }

    /**
     * @param StyleFacade[] $styleFacades
     * @return Style[]
     */
    public static function getStyles(array $styleFacades): array
    {
        return array_map(function (StyleFacade $styleFacade) {
            return $styleFacade->getStyle();
        }, $styleFacades);
    }

    /**
     * @param IntegrationFacade[] $integrationFacades
     * @return Integration[]
     */
    public static function getIntegrations(array $integrationFacades): array
    {
        return array_map(function (IntegrationFacade $integrationFacade) {
            return $integrationFacade->getIntegration();
        }, $integrationFacades);
    }
}