<?php

namespace common\helpers;

use ClientInterface\Base\PhpDocReader\AnnotationException;
use ClientInterface\Base\StructureHelper;
use common\integrations\common\BaseIntegration;
use common\integrations\configs\AbstractConfig;
use common\integrations\configs\PromodjConfig;
use common\integrations\configs\VkConfig;
use ReflectionException;
use Yii;
use yii\base\InvalidConfigException;

class IntegrationConfigurationHelper
{

    private const INTEGRATION_TO_CONFIG_MAP = [
        BaseIntegration::PROMODJ => PromodjConfig::class,
        BaseIntegration::VK => VkConfig::class,
    ];

    /**
     * @param string $integrationId
     * @param string|null $json
     * @return AbstractConfig
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public static function convertJsonToConfigurationObject(string $integrationId, ?string $json = null): AbstractConfig
    {
        /** @var AbstractConfig $configDTO */
        $configDTO = Yii::createObject(self::INTEGRATION_TO_CONFIG_MAP[$integrationId]);
        if(!$json) {
            return $configDTO;
        }

        $configArray = json_decode($json, true);
        StructureHelper::fill($configDTO, $configArray, false);
        return $configDTO;
    }

    public static function convertConfigurationObjectToJson(AbstractConfig $config): string
    {
        return json_encode($config);
    }
}