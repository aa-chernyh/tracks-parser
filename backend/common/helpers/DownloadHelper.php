<?php

namespace common\helpers;

use common\exceptions\LogicException;
use Yii;

class DownloadHelper
{

    /**
     * @param string $url
     * @param string $path
     * @param bool $isUseProxy
     * @throws LogicException
     */
    public static function download(string $url, string $path, bool $isUseProxy = false): void
    {
        $command = ConsoleHelper::getDownloadCommand($url, $path, $isUseProxy);
        ConsoleHelper::runAndGetResult($command);
    }

    /**
     * @param string $url
     * @param string $path
     * @param bool $isUseProxy
     * @throws LogicException
     */
    public static function downloadM3U8(string $url, string $path, bool $isUseProxy = false): void
    {
        $command = ConsoleHelper::getDownloadM3U8Command($url, $path, $isUseProxy);
        ConsoleHelper::runAndGetResult($command);
    }

    /**
     * @return string
     */
    public static function getTracksPath(): string
    {
        return Yii::getAlias('@tracksRoot') . DIRECTORY_SEPARATOR;
    }
    
}
