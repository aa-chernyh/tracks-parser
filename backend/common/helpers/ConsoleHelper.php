<?php

namespace common\helpers;

use common\exceptions\LogicException;
use Throwable;

class ConsoleHelper
{

    const COMMAND_IS_PROCESS_RUNNING = 'ps -p %d | grep %d';
    const COMMAND_KILL_PROCESS = 'kill -9 %d';
    const COMMAND_DOWNLOAD = 'wget "%s" -O "%s"';
    const COMMAND_DOWNLOAD_WITH_PROXY = 'wget "%s" -O "%s" -e use_proxy=yes -e http_proxy=%s -e https_proxy=%s';
    const COMMAND_DOWNLOAD_M3U8 = 'ffmpeg -http_persistent false -i "%s" -c copy "%s"';
    const COMMAND_DOWNLOAD_M3U8_WITH_PROXY = 'ffmpeg -http_persistent false -i "%s" -c copy "%s" -e use_proxy=yes -e http_proxy=%s -e https_proxy=%s';

    const ASYNC_COMMAND_ENDING_RETURNS_PID = ' > /dev/null 2>&1 & echo $!; ';

    /**
     * @param string $command
     * @return string|null
     * @throws LogicException
     */
    public static function runAndGetResult(string $command): ?string
    {
        try {
            return shell_exec($command);
        } catch (Throwable $exception) {
            throw new LogicException('Ошибка выполнения запроса в консоль. Команда: ' . $command);
        }
    }

    /**
     * @param string $command
     * @return integer|null
     * @throws LogicException
     */
    public static function runAsyncAndGetPid(string $command): ?int
    {
        try {
            $command = $command . self::ASYNC_COMMAND_ENDING_RETURNS_PID;
            $result = exec($command, $output);
            return $result ? intval($result) : null;
        } catch (Throwable $exception) {
            throw new LogicException('Ошибка выполнения асинхронного запроса в консоль. Команда: ' . $command);
        }
    }

    /**
     * @param int $pid
     * @return bool
     * @throws LogicException
     */
    public static function isProcessRunning(int $pid): bool
    {
        $command = sprintf(self::COMMAND_IS_PROCESS_RUNNING, $pid, $pid);
        return !empty(self::runAndGetResult($command));
    }

    /**
     * @param int $pid
     * @throws LogicException
     */
    public static function killProcess(int $pid): void
    {
        $command = sprintf(self::COMMAND_KILL_PROCESS, $pid);
        self::runAndGetResult($command);
    }

    public static function getDownloadCommand(string $url, string $path, bool $isUseProxy = false): string
    {
        if(
            $isUseProxy
            && ($httpProxy = EnvHelper::getHttpProxy())
            && ($httpsProxy = EnvHelper::getHttpProxy())
        ) {
            return sprintf(self::COMMAND_DOWNLOAD_WITH_PROXY, $url, $path, $httpProxy, $httpsProxy);
        }

        return sprintf(self::COMMAND_DOWNLOAD, $url, $path);
    }

    public static function getDownloadM3U8Command(string $url, string $path, bool $isUseProxy = false): string
    {
        if(
            $isUseProxy
            && ($httpProxy = EnvHelper::getHttpProxy())
            && ($httpsProxy = EnvHelper::getHttpProxy())
        ) {
            return sprintf(self::COMMAND_DOWNLOAD_M3U8_WITH_PROXY, $url, $path, $httpProxy, $httpsProxy);
        }

        return sprintf(self::COMMAND_DOWNLOAD_M3U8, $url, $path);
    }
}