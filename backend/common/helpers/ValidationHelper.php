<?php

namespace common\helpers;

use common\exceptions\ValidatorException;
use common\models\Track;
use common\integrations\common\BaseIntegration;
use common\integrations\PromodjIntegration;

class ValidationHelper
{

    private const PROMODJ_DEFAULT_AUTHOR_URL_REGEX = '/^(https:\/\/promodj\.com\/){1}[^\/]{2,}$/';

    /**
     * @param string $integrationId
     * @throws ValidatorException
     */
    public static function checkIntegrationId(string $integrationId): void
    {
        if(!in_array($integrationId, BaseIntegration::LIST)) {
            throw new ValidatorException('Неверный ключ интеграции ' . $integrationId);
        }
    }

    /**
     * @param string $url
     * @throws ValidatorException
     */
    public static function checkPromodjMusicianUrl(string $url): void
    {
        if(!preg_match(self::PROMODJ_DEFAULT_AUTHOR_URL_REGEX, $url)) {
            throw new ValidatorException('Неверный URL музыканта');
        }
    }

    /**
     * @param array|null $musicianPages
     * @throws ValidatorException
     */
    public static function checkPromodjMusicianUrlList(?array $musicianPages): void
    {
        if(!$musicianPages || !is_array($musicianPages)) {
            throw new ValidatorException('Отсутствует список URL музыкантов в файле конфигурации');
        }

        foreach($musicianPages as $url) {
            self::checkPromodjMusicianUrl(Utils::trim($url));
        }
    }

    /**
     * @param int $trackId
     * @throws ValidatorException
     */
    public static function checkIsTrackExists(int $trackId): void
    {
        if(!Track::find()->whereIdIs($trackId)->exists()) {
            throw new ValidatorException("Трека с id $trackId не существует!");
        }
    }
}