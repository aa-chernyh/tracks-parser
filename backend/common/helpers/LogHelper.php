<?php

namespace common\helpers;

use common\models\LogAnyError;
use common\models\LogIntegration;
use DateTime;
use Throwable;

class LogHelper
{

    /**
     * @param Throwable $exception
     * @throws Throwable
     */
    public static function writeException(Throwable $exception): void
    {
        $log = new LogAnyError();
        $log->date = Utils::getDbDateTimeStringByDateTime(new DateTime());
        $log->message = $exception->getMessage();
        $log->exception_name = str_replace('common\exceptions\\', '', get_class($exception));
        $log->stacktrace = $exception->getTraceAsString();

        $log->insert();
    }

    /**
     * @param string $type
     * @param string $integration
     * @param bool $isSuccess
     * @param string $requestUrl
     * @param string|null $request
     * @param string|null $response
     * @param string|null $requestHeaders
     * @param string|null $responseHeaders
     * @param array|null $errors
     * @throws Throwable
     */
    public static function writeIntegration(
        string $type,
        string $integration,
        bool $isSuccess,
        string $requestUrl,
        ?string $request = null,
        ?string $response = null,
        ?string $requestHeaders = null,
        ?string $responseHeaders = null,
        ?array $errors = null
    ): void
    {
        $log = new LogIntegration();
        $log->type = $type;
        $log->integration = $integration;
        $log->is_success = $isSuccess;
        $log->request_url = $requestUrl;
        $log->request = $request;
        $log->response = $response;
        $log->request_headers = $requestHeaders;
        $log->response_headers = $responseHeaders;
        $log->errors = $errors ? json_encode($errors) : null;
        $log->date = Utils::getDbDateTimeStringByDateTime(new DateTime());

        $log->insert();
    }
}
