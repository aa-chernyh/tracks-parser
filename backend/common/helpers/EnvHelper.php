<?php

namespace common\helpers;

use yii\helpers\ArrayHelper;

class EnvHelper
{

    public static function getHttpProxy(): ?string
    {
        return ArrayHelper::getValue($_ENV, 'http_proxy_docker');
    }

    public static function getHttpsProxy(): ?string
    {
        return ArrayHelper::getValue($_ENV, 'https_proxy_docker');
    }

    public static function getCommonProxy(): ?string
    {
        $httpsProxy = self::getHttpsProxy();
        $proxy = str_replace('https://', '', $httpsProxy);

        if(substr($proxy, -1, 1) == '/') {
            $proxy = substr($proxy, 0, -1);
        }

        return $proxy;
    }
}