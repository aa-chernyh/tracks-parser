<?php

namespace common\managers;

use common\exceptions\LogicException;
use common\helpers\Utils;
use common\models\activeQuery\TrackActiveQuery;
use api\v1\forms\player\TracklistFiltersForm;
use api\v1\forms\player\Tracklist;
use common\models\Track;
use jobs\ArchiveJob;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class PlayerManager extends BaseManager
{

    /**
     * @param TracklistFiltersForm $form
     * @return Tracklist
     * @throws InvalidConfigException
     * @throws LogicException
     */
    public function getTracklist(TracklistFiltersForm $form): Tracklist
    {
        $result = new Tracklist();
        $result->tracks = $this->getDownloadedTracks($form->getTracksQuery());

        $result->shownTracksCount = count($result->tracks);
        $result->foundTracksCount = intval($form->getTracksQuery(false)->count());

        return $result;
    }

    private function getDownloadedTracks(TrackActiveQuery $query): array
    {
        $tracks = [];
        /** @var Track $track */
        foreach ($query->each() as $track) {
            if ($track->isDownloaded()) {
                $tracks[] = $track;
            }
        }

        return $tracks;
    }

    /**
     * @param int $trackId
     * @return string
     * @throws LogicException
     */
    public function getTrackPathById(int $trackId): string
    {
        $track = Track::findOne(['id' => $trackId]);

        if(!$track->path) {
            throw new LogicException("У трека с id $trackId нет пути");
        }

        if(!$track->isDownloaded()) {
            throw new LogicException("Трек с id $trackId не скачан");
        }

        return $track->path;
    }

    /**
     * @param TracklistFiltersForm $form
     * @return string
     * @throws LogicException
     * @throws InvalidConfigException
     */
    public function createTracksArchive(TracklistFiltersForm $form): string
    {
        $downloadedTracks = $this->getDownloadedTracks($form->getTracksQuery()->onlyPath());
        $trackPaths = ArrayHelper::getColumn($downloadedTracks, 'path');
        return Utils::createZip($trackPaths, $form->getTracksArchivePrefix());
    }

}