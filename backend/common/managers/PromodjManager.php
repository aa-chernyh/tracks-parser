<?php

namespace common\managers;

use ClientInterface\Base\PhpDocReader\AnnotationException;
use common\exceptions\IntegrationConfigException;
use common\models\Author;
use common\integrations\common\BaseIntegration;
use common\integrations\configs\PromodjConfig;
use ReflectionException;
use Throwable;
use yii\base\InvalidConfigException;

class PromodjManager extends BaseManager
{

    /**
     * @var PromodjConfig
     */
    private $pdjConfig;

    /**
     * @var IntegrationManager
     */
    private $integrationManager;

    /**
     * PromodjManager constructor.
     * @throws IntegrationConfigException
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public function __construct()
    {
        $this->integrationManager = new IntegrationManager();
        $this->pdjConfig = $this->integrationManager->getIntegrationConfig(BaseIntegration::PROMODJ);
    }

    /**
     * @param string $url
     * @param bool $isSave
     */
    public function addMusicianToConfig(string $url, bool $isSave = true): void
    {
        $isAllowToAddNewMusician = true;
        if(is_array($this->pdjConfig->musicianPages)) {
            foreach ($this->pdjConfig->musicianPages as $pageUrl) {
                if ($pageUrl == $url) {
                    $isAllowToAddNewMusician = false;
                    break;
                }
            }
        }

        if($isAllowToAddNewMusician) {
            $this->pdjConfig->musicianPages[] = $url;
            if($isSave) {
                $this->integrationManager->setIntegrationConfig($this->pdjConfig, BaseIntegration::PROMODJ);
            }
        }
    }

    /**
     * @param array $configFileMusicianPages
     */
    public function addMusicianListToConfig(array $configFileMusicianPages): void
    {
        foreach($configFileMusicianPages as $url) {
            $this->addMusicianToConfig($url, false);
        }
        $this->integrationManager->setIntegrationConfig($this->pdjConfig, BaseIntegration::PROMODJ);
    }

    /**
     * @param array $configFileMusicianPages
     */
    public function replaceMusicianListInConfig(array $configFileMusicianPages): void
    {
        $this->pdjConfig->musicianPages = $configFileMusicianPages;
        $this->integrationManager->setIntegrationConfig($this->pdjConfig, BaseIntegration::PROMODJ);
    }

    /**
     * @param string $url
     * @return bool
     */
    public function removeMusicianFromConfig(string $url): bool
    {
        if(is_array($this->pdjConfig->musicianPages)) {
            foreach($this->pdjConfig->musicianPages as $key => $pageUrl) {
                if($url == $pageUrl) {
                    unset($this->pdjConfig->musicianPages[$key]);
                    return $this->integrationManager->setIntegrationConfig($this->pdjConfig, BaseIntegration::PROMODJ);
                }
            }
        }

        return false;
    }

    /**
     * @return array
     * @throws Throwable
     */
    public function getMusicianList(): array
    {
        $musicianList = [];
        if(isset($this->pdjConfig->musicianPages)) {
            $authorsQuery = Author::find()->where(['url' => $this->pdjConfig->musicianPages]);
            if($authorsQuery->exists()) {
                /** @var Author $author */
                foreach($authorsQuery->all() as $author) {
                    $musicianList[] = [
                        'name' => $author->name ?? $author->url,
                        'pageUrl' => $author->url,
                    ];
                }
            } else {
                foreach($this->pdjConfig->musicianPages as $url) {
                    $musicianList[] = [
                        'name' => $url,
                        'pageUrl' => $url,
                    ];
                }
            }
        }

        return $musicianList;
    }
}