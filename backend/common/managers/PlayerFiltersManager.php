<?php

namespace common\managers;

use common\models\Integration;
use api\v1\forms\player\PlayerFilter;
use api\v1\forms\player\PlayerFilterPosition;
use common\models\Style;
use common\models\Track;
use yii\db\Expression;

class PlayerFiltersManager
{

    /**
     * @return PlayerFilter[]
     */
    public function getAll(): array
    {
        return [
            $this->getFilterAuthorName(),
            $this->getFilterTrackName(),
            $this->getFilterTrackYear(),
            $this->getDurationFilter(),
            $this->getPublicationDateFilter(),
            $this->getStyleFilter(),
            $this->getKeyFilter(),
            $this->getSourceFilter(),
            $this->getExtensionFilter(),
            $this->getBpmFilter(),
            $this->getSizeFilter(),
            $this->getFilterIsMarked(),
            $this->getFilterIsMastered(),
        ];
    }

    /**
     * @return PlayerFilter
     */
    private function getFilterAuthorName(): PlayerFilter
    {
        return PlayerFilter::getSelf(PlayerFilter::FILTER_AUTHOR_NAME);
    }

    /**
     * @return PlayerFilter
     */
    private function getFilterTrackName(): PlayerFilter
    {
        return PlayerFilter::getSelf(PlayerFilter::FILTER_TRACK_NAME);
    }

    /**
     * @return PlayerFilter
     */
    private function getFilterTrackYear(): PlayerFilter
    {
        return PlayerFilter::getSelf(PlayerFilter::FILTER_TRACK_YEAR);
    }

    /**
     * @return PlayerFilter
     */
    private function getFilterIsMastered(): PlayerFilter
    {
        return PlayerFilter::getSelf(PlayerFilter::FILTER_IS_MASTERED);
    }

    /**
     * @return PlayerFilter
     */
    private function getFilterIsMarked(): PlayerFilter
    {
        return PlayerFilter::getSelf(PlayerFilter::FILTER_IS_MARKED);
    }

    /**
     * @return PlayerFilter
     */
    private function getDurationFilter(): PlayerFilter
    {
        return PlayerFilter::getSelf(
            PlayerFilter::FILTER_DURATION,
            PlayerFilterPosition::DURATION_POSITIONS_MAP
        );
    }

    /**
     * @return PlayerFilter
     */
    private function getPublicationDateFilter(): PlayerFilter
    {
        return PlayerFilter::getSelf(
            PlayerFilter::FILTER_PUBLICATION_DATE,
            PlayerFilterPosition::PUBLICATION_DATE_POSITIONS_MAP
        );
    }

    /**
     * @return PlayerFilter
     */
    private function getStyleFilter(): PlayerFilter
    {
        return PlayerFilter::getSelf(
            PlayerFilter::FILTER_STYLE,
            $this->getStylePositionKeyToNameMap()
        );
    }

    /**
     * @return array
     */
    private function getStylePositionKeyToNameMap(): array
    {
        $styles = Style::find()->orderBy('name')->all();
        $result = [];
        /** @var Style $style */
        foreach($styles as $style) {
            $result[$style->id] = $style->name;
        }
        return $result;
    }

    /**
     * @return PlayerFilter
     */
    private function getKeyFilter(): PlayerFilter
    {
        return PlayerFilter::getSelf(
            PlayerFilter::FILTER_KEY,
            PlayerFilterPosition::KEY_POSITIONS_MAP
        );
    }

    /**
     * @return PlayerFilter
     */
    private function getSourceFilter(): PlayerFilter
    {
        return PlayerFilter::getSelf(
            PlayerFilter::FILTER_SOURCE,
            $this->getSourcePositionKeyToNameMap()
        );
    }

    /**
     * @return string[]
     */
    private function getSourcePositionKeyToNameMap(): array
    {
        $integrationToNameMap = [];
        /** @var Integration $integration */
        foreach (Integration::find()->orderBy('name')->all() as $integration) {
            $integrationToNameMap[$integration->id] = $integration->name;
        }

        return $integrationToNameMap;
    }

    /**
     * @return PlayerFilter
     */
    private function getExtensionFilter(): PlayerFilter
    {
        return PlayerFilter::getSelf(
            PlayerFilter::FILTER_EXTENSION,
            $this->getExtensionPositionKeyToNameMap()
        );
    }

    /**
     * @return array
     */
    private function getExtensionPositionKeyToNameMap(): array
    {
        $extensions = Track::find()
            ->where(['is not', 'extension', new Expression('null')])
            ->select('extension')
            ->distinct()
            ->orderBy(['extension' => SORT_ASC])
            ->column();

        $result = [];
        foreach($extensions as $extension) {
            $result[$extension] = $extension;
        }
        return $result;
    }

    private function getBpmFilter(): PlayerFilter
    {
        return PlayerFilter::getSelf(
            PlayerFilter::FILTER_BPM,
            PlayerFilterPosition::BPM_POSITIONS_MAP
        );
    }

    private function getSizeFilter(): PlayerFilter
    {
        return PlayerFilter::getSelf(
            PlayerFilter::FILTER_SIZE,
            PlayerFilterPosition::SIZE_POSITIONS_MAP
        );
    }

}