<?php

namespace common\managers;

use common\integrations\common\IntegrationFactory;
use common\exceptions\LogicException;

class ScanManager extends BaseManager
{

    /**
     * @param array $integrationList
     * @throws LogicException
     */
    public function startScanByIntegrationList(array $integrationList): void
    {
        foreach ($integrationList as $integrationId) {
            $this->startByIntegrationId($integrationId);
        }
    }

    /**
     * @param string $integrationId
     * @throws LogicException
     */
    public function startByIntegrationId(string $integrationId): void
    {
        $integration = new IntegrationFactory($integrationId);
        $integration->service();
    }

}