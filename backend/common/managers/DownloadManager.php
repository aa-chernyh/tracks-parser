<?php

namespace common\managers;

use common\collections\TracksCollection;
use common\helpers\CollectionsAdapter;
use common\helpers\Utils;
use common\modelFacades\track\TrackReadableInterface;
use common\models\Track;
use common\integrations\common\BaseIntegration;
use common\jobs\DownloadJob;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

class DownloadManager extends BaseManager
{

    /**
     * @throws Throwable
     */
    public function downloadAll(): void
    {
        $this->downloadByQuery(
            $this->getAllTracksToDownloadQuery()
        );
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    private function getAllTracksToDownloadQuery(): ActiveQuery
    {
        return Track::find()
            ->select(['integration', 'path', 'download_url'])
            ->whereDownloadUrlIsNotNull()
            ->wherePathIsNotNull();
    }

    /**
     * @param ActiveQuery $query
     * @throws Throwable
     */
    private function downloadByQuery(ActiveQuery $query): void
    {
        $tracksCollection = new TracksCollection(CollectionsAdapter::getTrackFacades($query->all()));
        foreach ($tracksCollection->notDownloaded()->all() as $trackReadable) {
            Utils::checkFolder($trackReadable->getPath());
            Yii::$app->queue->push($this->getDownloadJob($trackReadable));
        }
    }

    private function getDownloadJob(TrackReadableInterface $trackReadable): DownloadJob
    {
        $job = new DownloadJob();
        $job->downloadUrl = $trackReadable->getDownloadUrl();
        $job->path = $trackReadable->getPath();
        $job->mutexLockName = $trackReadable->getIntegrationId();

        return $job;
    }
}