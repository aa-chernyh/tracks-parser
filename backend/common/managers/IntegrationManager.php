<?php

namespace common\managers;

use ClientInterface\Base\PhpDocReader\AnnotationException;
use ClientInterface\Base\StructureHelper;
use common\models\Integration;
use common\integrations\common\BaseIntegration;
use common\integrations\configs\AbstractConfig;
use common\integrations\configs\PromodjConfig;
use common\integrations\configs\VkConfig;
use ReflectionException;
use Yii;
use yii\base\InvalidConfigException;

class IntegrationManager extends BaseManager
{

    private const INTEGRATION_TO_CONFIG_MAP = [
        BaseIntegration::PROMODJ => PromodjConfig::class,
        BaseIntegration::VK => VkConfig::class,
    ];

    /**
     * @return array
     */
    public function getList(): array
    {
        return Integration::find()->select([
            'id',
            'name',
            'description',
        ])->all();
    }

    /**
     * @param string $integrationId
     * @return Integration
     */
    public function getIntegrationById(string $integrationId): Integration
    {
        return Integration::findOne(['id' => $integrationId]);
    }

    /**
     * @param AbstractConfig $config
     * @param string $integrationId
     * @return bool
     */
    public function setIntegrationConfig(AbstractConfig $config, string $integrationId): bool
    {
        $integration = $this->getIntegrationById($integrationId);
        $integration->configuration = json_encode($config);
        return $integration->save();
    }

    /**
     * @param string $integrationId
     * @return AbstractConfig
     * @throws AnnotationException
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function getIntegrationConfig(string $integrationId): AbstractConfig
    {
        $integration = $this->getIntegrationById($integrationId);

        if(!$integration->configuration) {
            /** @var AbstractConfig $configDTO */
            $configDTO = Yii::createObject(self::INTEGRATION_TO_CONFIG_MAP[$integrationId]);
            return $configDTO;
        }

        $configArray = json_decode($integration->configuration, true);
        return $this->convertArrayToIntegrationConfig($integrationId, $configArray);
    }

    /**
     * @param string $integrationId
     * @param array $configArray
     * @return AbstractConfig
     * @throws AnnotationException
     * @throws InvalidConfigException
     * @throws ReflectionException
     */
    public function convertArrayToIntegrationConfig(string $integrationId, array $configArray): AbstractConfig
    {
        /** @var AbstractConfig $configDTO */
        $configDTO = Yii::createObject(self::INTEGRATION_TO_CONFIG_MAP[$integrationId]);
        StructureHelper::fill($configDTO, $configArray, false);
        return $configDTO;
    }
}