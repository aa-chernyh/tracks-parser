<?php

namespace common\managers;

use common\helpers\Utils;
use common\models\LogAnyError;
use common\models\LogIntegration;
use DateTime;
use queue\Queue;

class CleanLogManager extends BaseManager
{
    /**
     * @var string
     */
    private $dateToClean;

    public function __construct()
    {
        $this->dateToClean = $this->getDateToClean();
    }

    /**
     * @return string
     */
    private function getDateToClean(): string
    {
        $dateToClean = new DateTime('-1 month');
        return Utils::getDbDateTimeStringByDateTime($dateToClean);
    }

    public function cleanOld(): void
    {
        $this->cleanQueueTask();
        $this->cleanLogAnyError();
        $this->cleanLogIntegration();
    }

    private function cleanLogAnyError(): void
    {
        LogAnyError::deleteAll(['<', 'date', $this->dateToClean]);
    }

    private function cleanQueueTask(): void
    {
        Queue::cleanOldTasksByDate($this->dateToClean);
    }

    private function cleanLogIntegration(): void
    {
        LogIntegration::deleteAll(['<', 'date', $this->dateToClean]);
    }
}