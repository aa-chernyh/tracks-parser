<?php

namespace common\models\activeQuery;

use common\enums\SortingOrderEnum;
use common\exceptions\LogicException;
use common\helpers\Utils;
use api\v1\forms\player\PlayerFilter;
use api\v1\forms\player\PlayerFilterPosition;
use Exception;
use common\integrations\common\BaseIntegration;
use yii\db\ActiveQuery;
use yii\db\Expression;

class TrackActiveQuery extends ActiveQuery
{

    /** @deprecated  */
    const SECONDS_IN_MINUTE = 60;
    /** @deprecated  */
    const TRACKS_COUNT_LIMIT = 255;

    /** @deprecated  */
    const PUBLICATION_DATE_FILTER_TO_DATETIME_PARAM_MAP = [
        PlayerFilterPosition::POSITION_PUBLICATION_DATE_TODAY => null,
        PlayerFilterPosition::POSITION_PUBLICATION_DATE_LAST_3_DAYS => '-3 days',
        PlayerFilterPosition::POSITION_PUBLICATION_DATE_LAST_WEEK => '-7 days',
        PlayerFilterPosition::POSITION_PUBLICATION_DATE_LAST_MONTH => '-1 month',
        PlayerFilterPosition::POSITION_PUBLICATION_DATE_LAST_3_MONTHS => '-3 months',
        PlayerFilterPosition::POSITION_PUBLICATION_DATE_LAST_6_MONTHS => '-6 months',
        PlayerFilterPosition::POSITION_PUBLICATION_DATE_LAST_YEAR => '-1 year',
    ];

    /**
     * @param string|null $authorName
     * @return $this
     */
    public function whereAuthorName(?string $authorName): self
    {
        if ($authorName) {
            $this->joinWith('authors')->andWhere(['like', 'author.name', $authorName]);
        }
        return $this;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function whereTrackName(?string $name): self
    {
        if ($name) {
            $this->andWhere(['like', 'track.name', $name]);
        }
        return $this;
    }

    /**
     * @param int|null $year
     * @return $this
     * @deprecated
     */
    public function whereYear(?int $year): self
    {
        if ($year) {
            $this->andWhere(['>=', 'track.release_date',
                Utils::getDbDateTimeStringByDateTime(Utils::createDateTimeFromYear($year))
            ]);
            $this->andWhere(['<', 'track.release_date',
                Utils::getDbDateTimeStringByDateTime(Utils::createDateTimeFromYear($year + 1))
            ]);
        }
        return $this;
    }

    /**
     * @param bool|null $isMastered
     * @return $this
     * @deprecated
     */
    public function whereIsMastered(?bool $isMastered): self
    {
        if ($isMastered) {
            //todo $this->andWhere(['track.is_mastered' => $isMastered]);
        }
        return $this;
    }

    /**
     * @param bool|null $isMarked
     * @return $this
     * @deprecated
     */
    public function whereIsMarked(?bool $isMarked): self
    {
        if ($isMarked) {
            //todo $this->andWhere(['track.is_marked' => $isMarked]);
        }
        return $this;
    }

    /**
     * @param string|null $durationFilterPosition
     * @return $this
     * @throws LogicException
     * @deprecated
     */
    public function whereDurationFilterPosition(?string $durationFilterPosition): self
    {
        if ($durationFilterPosition) {
            switch ($durationFilterPosition) {
                case PlayerFilterPosition::POSITION_DURATION_LESS_THEN_MINUTE:
                    $this->andWhere(['<', 'track.duration', self::SECONDS_IN_MINUTE]);
                    break;
                case PlayerFilterPosition::POSITION_DURATION_LESS_THEN_10_MINUTES:
                    $this->andWhere(['<', 'track.duration', 10 * self::SECONDS_IN_MINUTE]);
                    break;
                case PlayerFilterPosition::POSITION_DURATION_LESS_THEN_30_MINUTES:
                    $this->andWhere(['<', 'track.duration', 30 * self::SECONDS_IN_MINUTE]);
                    break;
                case PlayerFilterPosition::POSITION_DURATION_LESS_THEN_HOUR:
                    $this->andWhere(['<', 'track.duration', 60 * self::SECONDS_IN_MINUTE]);
                    break;
                case PlayerFilterPosition::POSITION_DURATION_MORE_THEN_HOUR:
                    $this->andWhere(['>', 'track.duration', 60 * self::SECONDS_IN_MINUTE]);
                    break;
                default:
                    $message = "Нет маппинга значения {$durationFilterPosition} для фильтра" . PlayerFilter::FILTER_NAME_DURATION;
                    throw new LogicException($message);
            }
        }
        return $this;
    }

    public function whereDurationIn(?int $min, ?int $max): self
    {
        if($min) {
            $this->andWhere(['>=', 'track.duration', $min]);
        }

        if($max) {
            $this->andWhere(['<=', 'track.duration', $max]);
        }

        return $this;
    }

    /**
     * @param string|null $publicationDateFilterPosition
     * @return $this
     * @throws LogicException
     * @throws Exception
     * @deprecated
     */
    public function wherePublicationDateFilterPosition(?string $publicationDateFilterPosition): self
    {
        if ($publicationDateFilterPosition) {
            if (!array_key_exists($publicationDateFilterPosition, self::PUBLICATION_DATE_FILTER_TO_DATETIME_PARAM_MAP)) {
                $message = "Нет маппинга значения $publicationDateFilterPosition для фильтра " . PlayerFilter::FILTER_NAME_PUBLICATION_DATE;
                throw new LogicException($message);
            }

            $dateTime = Utils::createDateTimeWithNullTime(self::PUBLICATION_DATE_FILTER_TO_DATETIME_PARAM_MAP[$publicationDateFilterPosition]);
            $this->andWhere(['>=', 'track.release_date', Utils::getDbDateTimeStringByDateTime($dateTime)]);
        }
        return $this;
    }

    public function whereReleaseDateIn(?string $dateFrom, ?string $dateTo): self
    {
        if($dateFrom) {
            $this->andWhere(['>=', 'track.release_date', $dateFrom]);
        }

        if($dateTo) {
            $this->andWhere(['<=', 'track.release_date', $dateTo]);
        }

        return $this;
    }

    public function whereInsertDateIn(?string $dateFrom, ?string $dateTo): self
    {
        if($dateFrom) {
            $this->andWhere(['>=', 'track.insert_date', $dateFrom]);
        }

        if($dateTo) {
            $this->andWhere(['<=', 'track.insert_date', $dateTo]);
        }

        return $this;
    }

    /**
     * @param string|null $style
     * @return $this
     * @deprecated
     */
    public function whereStyle(?string $style): self
    {
        if ($style) {
            $this->joinWith('styles')->andWhere(['style.id' => $style]);
        }
        return $this;
    }

    /**
     * @param string[] $ids
     * @return $this
     */
    public function whereStyleIn(?array $ids = []): self
    {
        if($ids) {
            $this->joinWith('styles')->andWhere(['style.id' => $ids]);
        }

        return $this;
    }

    /**
     * @param string|null $key
     * @return $this
     * @throws LogicException
     * @deprecated
     */
    public function whereKey(?string $key): self
    {
        if ($key) {
            if (!array_key_exists($key, PlayerFilterPosition::KEY_POSITIONS_MAP)) {
                $message = "Нет маппинга значения $key для фильтра " . PlayerFilter::FILTER_NAME_KEY;
                throw new LogicException($message);
            }
            $this->andWhere(['track.key' => $key]);
        }
        return $this;
    }

    public function whereKeyIn(?array $ids = []): self
    {
        if($ids) {
            $this->andWhere(['track.key' => $ids]);
        }

        return $this;
    }

    /**
     * @param string|null $source
     * @return $this
     * @throws LogicException
     * @deprecated
     */
    public function whereSource(?string $source): self
    {
        if ($source) {
            if (!in_array($source, BaseIntegration::LIST)) {
                $message = "Нет маппинга значения $source для фильтра " . PlayerFilter::FILTER_NAME_SOURCE;
                throw new LogicException($message);
            }
            $this->andWhere(['track.integration' => $source]);
        }
        return $this;
    }

    public function whereSourceIn(?array $ids = []): self
    {
        if($ids) {
            $this->andWhere(['track.integration' => $ids]);
        }

        return $this;
    }

    /**
     * @param string|null $extension
     * @return $this
     * @deprecated
     */
    public function whereExtension(?string $extension): self
    {
        if ($extension) {
            $this->andWhere(['track.extension' => $extension]);
        }
        return $this;
    }

    public function whereExtensionIn(?array $ids = []): self
    {
        if($ids) {
            $this->andWhere(['track.extension' => $ids]);
        }

        return $this;
    }

    /**
     * @param string|null $bpmFilterPosition
     * @return $this
     * @throws LogicException
     * @deprecated
     */
    public function whereBpmFilterPosition(?string $bpmFilterPosition): self
    {
        if ($bpmFilterPosition) {
            switch ($bpmFilterPosition) {
                case PlayerFilterPosition::POSITION_BPM_0_90:
                    $this->andWhere(['<=', 'track.bpm', 90]);
                    break;
                case PlayerFilterPosition::POSITION_BPM_91_119:
                    $this->andWhere(['>=', 'track.bpm', 91]);
                    $this->andWhere(['<=', 'track.bpm', 119]);
                    break;
                case PlayerFilterPosition::POSITION_BPM_120_132:
                    $this->andWhere(['>=', 'track.bpm', 120]);
                    $this->andWhere(['<=', 'track.bpm', 132]);
                    break;
                case PlayerFilterPosition::POSITION_BPM_133_160:
                    $this->andWhere(['>=', 'track.bpm', 133]);
                    $this->andWhere(['<=', 'track.bpm', 160]);
                    break;
                case PlayerFilterPosition::POSITION_BPM_161_MAX:
                    $this->andWhere(['>=', 'track.bpm', 161]);
                    break;
                default:
                    $message = "Нет маппинга значения {$bpmFilterPosition} для фильтра" . PlayerFilter::FILTER_NAME_BPM;
                    throw new LogicException($message);
            }
        }
        return $this;
    }

    public function whereBpmIn(?int $min, ?int $max): self
    {
        if($min) {
            $this->andWhere(['>=', 'track.bpm', $min]);
        }

        if($max) {
            $this->andWhere(['<=', 'track.bpm', $max]);
        }

        return $this;
    }

    /**
     * @param string|null $sizeFilterPosition
     * @return $this
     * @throws LogicException
     * @deprecated
     */
    public function whereSizeFilterPosition(?string $sizeFilterPosition): self
    {
        if ($sizeFilterPosition) {
            switch ($sizeFilterPosition) {
                case PlayerFilterPosition::POSITION_SIZE_0_15:
                    $this->andWhere(['<=', 'track.size', 15728640]);
                    break;
                case PlayerFilterPosition::POSITION_SIZE_16_60:
                    $this->andWhere(['>', 'track.size', 15728640]);
                    $this->andWhere(['<=', 'track.size', 62914560]);
                    break;
                case PlayerFilterPosition::POSITION_SIZE_61_120:
                    $this->andWhere(['>', 'track.size', 62914560]);
                    $this->andWhere(['<=', 'track.size', 125829120]);
                    break;
                case PlayerFilterPosition::POSITION_SIZE_121_MAX:
                    $this->andWhere(['>', 'track.size', 125829120]);
                    break;
                case PlayerFilterPosition::POSITION_SIZE_0_60:
                    $this->andWhere(['<=', 'track.size', 62914560]);
                    break;
                default:
                    $message = "Нет маппинга значения {$sizeFilterPosition} для фильтра" . PlayerFilter::FILTER_NAME_SIZE;
                    throw new LogicException($message);
            }
        }
        return $this;
    }

    public function whereSizeIn(?int $min, ?int $max): self
    {
        if($min) {
            $this->andWhere(['>=', 'track.size', $min]);
        }

        if($max) {
            $this->andWhere(['<=', 'track.size', $max]);
        }

        return $this;
    }

    public function whereDownloadUrlIsNotNull(): self
    {
        return $this->andWhere(['is not', 'download_url', new Expression('null')]);
    }

    public function wherePathIsNotNull(): self
    {
        return $this->andWhere(['is not', 'path', new Expression('null')]);
    }

    public function whereIdIs(int $id): self
    {
        return $this->andWhere(['id' => $id])->limit(1);
    }

    public function onlyPath(): self
    {
        return $this->select(['path']);
    }

    public function hasDuration(): self
    {
        return $this->andWhere(['is not', 'duration', new Expression('null')]);
    }

    public function hasPath(): self
    {
        return $this->andWhere(['is not', 'path', new Expression('null')]);
    }

    public function hasExtension(): self
    {
        return $this->andWhere(['is not', 'extension', new Expression('null')]);
    }

    public function hasBPM(): self
    {
        return $this->andWhere(['is not', 'bpm', new Expression('null')]);
    }

    public function hasSize(): self
    {
        return $this->andWhere(['is not', 'size', new Expression('null')]);
    }

    public function order(array $fields, int $order = SortingOrderEnum::ID_ASC): self
    {
        $params = [];
        foreach ($fields as $field) {
            $params[$field] = $order;
        }
        parent::addOrderBy($params);

        return $this;
    }

    public function withAuthors(): self
    {
        return $this->with('authors');
    }

    public function withStyles(): self
    {
        return $this->with('styles');
    }

    public function withIntegration(): self
    {
        return $this->with('integrationAR');
    }

    public function whereIdIn(array $ids): self
    {
        return $this->andWhere(['track.id' => $ids]);
    }
}