<?php

namespace common\models;

use common\helpers\Utils;
use common\components\MetaActiveRecord;
use common\modelFacades\track\TrackFacade;
use common\models\activeQuery\TrackActiveQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * @property integer $id
 * @property string $integration Источник
 * @property string $name Название
 * @property string $path Путь хранения
 * @property string $extension Расширение без точки
 * @property string $page_url Страница трека
 * @property string $download_url Ссылка на скачивание
 * @property float $duration Длительность в секундах
 * @property integer $bpm Ударов в минуту
 * @property string $key Тональность
 * @property float $size Размер в байтах
 * @property string $release_date Дата публикации
 * @property integer $bitrate Битрейт
 * @property string $insert_date Дата добавления в таблицу
 *
 * Foreign key props
 * @property Author[] $authors
 * @property Style[] $styles
 * @property Integration $integrationAR
 */
class Track extends MetaActiveRecord
{

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getAuthors(): ActiveQuery
    {
        return $this
            ->hasMany(Author::class, ['id' => 'author_id'])
            ->viaTable('track_to_author', ['track_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getStyles(): ActiveQuery
    {
        return $this
            ->hasMany(Style::class, ['id' => 'style_id'])
            ->viaTable('track_to_style', ['track_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getIntegrationAR(): ActiveQuery
    {
        return $this->hasOne(Integration::class, ['id' => 'integration']);
    }

    /**
     * @return TrackActiveQuery
     * @throws InvalidConfigException
     */
    public static function find(): TrackActiveQuery
    {
        return Yii::createObject(TrackActiveQuery::class, [get_called_class()]);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['name', 'integration', 'download_url', 'path'], 'required'],
            [['name'], 'unique'],
            [['download_url', 'page_url'], 'url'],

            [['duration'], 'double'],
            [['key', 'extension'], 'string', 'max' => 4],

            [
                ['name', 'integration'],
                'string', 'max' => 255
            ],
            [
                ['size', 'bpm', 'bitrate'],
                'integer'
            ],
            [
                ['release_date', 'insert_date'],
                'datetime',
                'format' => 'php:' . Utils::getDefaultDbDatetimeFormat()
            ],
        ];
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'track';
    }

    /**
     * @return string[]
     */
    protected function getJsonAttributes(): array
    {
        return [];
    }

    public function isDownloaded(): bool
    {
        return $this->path && file_exists($this->path) && filesize($this->path) > TrackFacade::MIN_TRACK_FILE_SIZE;
    }
}
