<?php

namespace common\models;

use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property string $id ID
 * @property string $name Название
 */
class Style extends ActiveRecord
{

    public const ID_OTHER = 'other';
    public const ID_METAL = 'metal';
    public const ID_CHANSON = 'chanson';
    public const ID_CLASSICAL = 'classical';
    public const ID_INDIE_POP = 'indie_pop';
    public const ID_ETHNIC = 'ethnic';

    public static function tableName(): string
    {
        return 'style';
    }

    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            [['id', 'name'], 'string', 'max' => 255],
            [['id', 'name'], 'required'],
        ];
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getTracks(): ActiveQuery
    {
        return $this
            ->hasMany(Track::class, ['id' => 'track_id'])
            ->viaTable('track_to_style', ['style_id' => 'id']);
    }
}