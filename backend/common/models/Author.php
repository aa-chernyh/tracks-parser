<?php

namespace common\models;

use common\components\MetaActiveRecord;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * @property integer $id
 * @property string $name Имя
 * @property string $url Ссылка на страницу
 * @property string $real_name Реальное имя
 *
 * Параметры Promodj
 * @property string $main_style Основной стиль
 *
 * Foreign key props
 * @property ActiveQuery $tracks
 */
class Author extends MetaActiveRecord
{

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getTracks(): ActiveQuery
    {
        return $this
            ->hasMany(Track::class, ['id' => 'track_id'])
            ->viaTable('track_to_author', ['author_id' => 'id']);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['name', 'real_name', 'main_style'], 'string', 'max' => 255],
            [['url'], 'url'],
        ];
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'author';
    }

    /**
     * @return string[]
     */
    protected function getJsonAttributes(): array
    {
        return [
            'pr',
            'main_style'
        ];
    }
}
