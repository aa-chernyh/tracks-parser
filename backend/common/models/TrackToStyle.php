<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * @property int $track_id ID трека
 * @property string $style_id ID стиля
 */
class TrackToStyle extends ActiveRecord
{

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['track_id'], 'integer'],
            [['style_id'], 'string'],
            [['track_id', 'style_id'], 'required'],
        ];
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'track_to_style';
    }
}