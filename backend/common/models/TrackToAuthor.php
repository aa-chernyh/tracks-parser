<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class TrackToAuthor
 * @package common\models
 * @property int $track_id ID трека
 * @property int $author_id ID автора
 */
class TrackToAuthor extends ActiveRecord
{

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['track_id', 'author_id'], 'integer'],
            [['track_id', 'author_id'], 'required'],
        ];
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'track_to_author';
    }
}