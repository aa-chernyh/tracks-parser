<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * @property integer $id Константа интеграции из integrations\common\BaseIntegration
 * @property string $name Название для отображения
 * @property string $description Описание
 * @property Json $configuration Конфигурация
 *
 * Foreign key props
 * @property ActiveRecord[] $tracks
 */

class Integration extends ActiveRecord
{

    public const ID_PROMODJ = 'Promodj';
    public const ID_VK = 'VK';

    const ID_LIST = [
        self::ID_PROMODJ,
        self::ID_VK,
    ];

    /**
     * @return ActiveQuery
     */
    public function getTracks()
    {
        return $this->hasMany(Track::class, ['integration' => 'id']);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id', 'name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1023],
            [['id', 'name'], 'required'],
        ];
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'integration';
    }
}
