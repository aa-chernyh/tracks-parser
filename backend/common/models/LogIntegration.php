<?php

namespace common\models;

use common\helpers\Utils;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $request Запрос
 * @property string $response Ответ
 * @property boolean $is_success Успешно ли?
 * @property string $type Тип запроса
 * @property string $request_url URL запроса
 * @property string $request_headers Заголовки запроса
 * @property string $response_headers Заголовки ответа
 * @property string $integration Интеграция
 * @property string $date Дата выполнения запроса
 * @property string $errors Ошибки
 */
class LogIntegration extends ActiveRecord
{

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['is_success'], 'boolean'],
            [
                [
                    'request',
                    'response',
                    'request_url',
                    'request_headers',
                    'response_headers',
                    'errors',
                ],
                'string'
            ],
            [
                [
                    'type',
                    'integration'
                ],
                'string', 'max' => 255
            ],
            [
                ['date'],
                'datetime',
                'format' => 'php:' . Utils::getDefaultDbDatetimeFormat()
            ]
        ];
    }

    public static function tableName(): string
    {
        return 'log_integration';
    }
}
