<?php

namespace common\models;

use common\helpers\Utils;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $date Дата
 * @property string $message Текст ошибки
 * @property string $exception_name Имя Exception-класса
 * @property string $stacktrace StackTrace
 */
class LogAnyError extends ActiveRecord
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['date'], 'datetime', 'format' => 'php:' . Utils::getDefaultDbDatetimeFormat()],
            [['message'], 'string', 'max' => 1023],
            [['exception_name'], 'string', 'max' => 511],
        ];
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'log_any_error';
    }

}
