<?php

namespace common\exceptions;

use common\helpers\LogHelper;
use Exception;
use Throwable;

abstract class ProjectException extends Exception
{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        LogHelper::writeException($this);
    }
}