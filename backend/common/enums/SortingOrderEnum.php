<?php

namespace common\enums;

class SortingOrderEnum extends AbstractEnum
{

    public const ID_ASC = SORT_ASC;
    public const ID_DESC = SORT_DESC;

    public const NAME_ASC = 'Прямая';
    public const NAME_DESC = 'Обратная';

    private const ID_LIST = [
        self::ID_ASC,
        self::ID_DESC,
    ];

    private const NAME_LIST = [
        self::NAME_ASC,
        self::NAME_DESC,
    ];

    private const MAPPING = [
        self::ID_ASC => self::NAME_ASC,
        self::ID_DESC => self::NAME_DESC,
    ];

    public static function getIdList(): array
    {
        return self::ID_LIST;
    }

    public static function getNameList(): array
    {
        return self::NAME_LIST;
    }

    public static function getIdToNameMapping(): array
    {
        return self::MAPPING;
    }
}