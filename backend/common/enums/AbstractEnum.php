<?php

namespace common\enums;

abstract class AbstractEnum implements EnumInterface
{

    public static function getNameToIdMapping(): array
    {
        return array_flip(static::getIdToNameMapping());
    }

    public static function getName($id)
    {
        if(!in_array($id, static::getIdList())) {
            return null;
        }

        return static::getIdToNameMapping()[$id];
    }

    public static function getId($name)
    {
        if(!in_array($name, static::getNameList())) {
            return null;
        }

        return static::getNameToIdMapping()[$name];
    }
}