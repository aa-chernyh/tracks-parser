<?php

namespace common\enums;

class TrackOrderByEnum extends AbstractEnum
{

    public const ID_INTEGRATION = 'integration';
    public const ID_NAME = 'name';
    public const ID_EXTENSION = 'extension';
    public const ID_DURATION = 'duration';
    public const ID_BPM = 'bpm';
    public const ID_KEY = 'key';
    public const ID_SIZE = 'size';
    public const ID_RELEASE_DATE = 'release_date';
    public const ID_BITRATE = 'bitrate';
    public const ID_INSERT_DATE = 'insert_date';

    public const NAME_INTEGRATION = 'Источник';
    public const NAME_NAME = 'Название';
    public const NAME_EXTENSION = 'Расширение';
    public const NAME_DURATION = 'Длительность';
    public const NAME_BPM = 'BPM';
    public const NAME_KEY = 'Тональность';
    public const NAME_SIZE = 'Размер';
    public const NAME_RELEASE_DATE = 'Дата публикации';
    public const NAME_BITRATE = 'Битрейт';
    public const NAME_INSERT_DATE = 'Дата добавления';

    private const ID_LIST = [
        self::ID_INTEGRATION,
        self::ID_NAME,
        self::ID_EXTENSION,
        self::ID_DURATION,
        self::ID_BPM,
        self::ID_KEY,
        self::ID_SIZE,
        self::ID_RELEASE_DATE,
        self::ID_BITRATE,
        self::ID_INSERT_DATE,
    ];

    private const NAME_LIST = [
        self::NAME_INTEGRATION,
        self::NAME_NAME,
        self::NAME_EXTENSION,
        self::NAME_DURATION,
        self::NAME_BPM,
        self::NAME_KEY,
        self::NAME_SIZE,
        self::NAME_RELEASE_DATE,
        self::NAME_BITRATE,
        self::NAME_INSERT_DATE,
    ];

    private const MAPPING = [
        self::ID_INTEGRATION => self::NAME_INTEGRATION,
        self::ID_NAME => self::NAME_NAME,
        self::ID_EXTENSION => self::NAME_EXTENSION,
        self::ID_DURATION => self::NAME_DURATION,
        self::ID_BPM => self::NAME_BPM,
        self::ID_KEY => self::NAME_KEY,
        self::ID_SIZE => self::NAME_SIZE,
        self::ID_RELEASE_DATE => self::NAME_RELEASE_DATE,
        self::ID_BITRATE => self::NAME_BITRATE,
        self::ID_INSERT_DATE => self::NAME_INSERT_DATE,
    ];

    public static function getIdList(): array
    {
        return self::ID_LIST;
    }

    public static function getNameList(): array
    {
        return self::NAME_LIST;
    }

    public static function getIdToNameMapping(): array
    {
        return self::MAPPING;
    }
}