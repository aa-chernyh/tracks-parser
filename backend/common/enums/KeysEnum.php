<?php

namespace common\enums;

class KeysEnum extends AbstractEnum
{

    public const ID_A = 'A';
    public const ID_A_FLAT = 'A_flat';
    public const ID_A_SHARP = 'A_sharp';
    public const ID_B = 'B';
    public const ID_B_FLAT = 'B_flat';
    public const ID_C = 'C';
    public const ID_C_SHARP = 'C_sharp';
    public const ID_D = 'D';
    public const ID_E_FLAT = 'E_flat';
    public const ID_E = 'E';
    public const ID_F = 'F';
    public const ID_F_SHARP = 'F_sharp';
    public const ID_G = 'G';
    public const ID_Am = 'Am';
    public const ID_Am_FLAT = 'Am_flat';
    public const ID_Bm = 'Bm';
    public const ID_Bm_FLAT = 'Bm_flat';
    public const ID_Cm = 'Cm';
    public const ID_Cm_SHARP = 'Cm_sharp';
    public const ID_Dm = 'Dm';
    public const ID_Em_FLAT = 'Em_flat';
    public const ID_Em = 'Em';
    public const ID_Fm = 'Fm';
    public const ID_Fm_SHARP = 'Fm_sharp';
    public const ID_Gm = 'Gm';
    public const ID_Gm_SHARP = 'Gm_sharp';

    public const NAME_A = 'A';
    public const NAME_A_FLAT = 'Ab';
    public const NAME_A_SHARP = 'A#';
    public const NAME_B = 'B';
    public const NAME_B_FLAT = 'Bb';
    public const NAME_C = 'C';
    public const NAME_C_SHARP = 'C#';
    public const NAME_D = 'D';
    public const NAME_E_FLAT = 'Eb';
    public const NAME_E = 'E';
    public const NAME_F = 'F';
    public const NAME_F_SHARP = 'F#';
    public const NAME_G = 'G';
    public const NAME_Am = 'Am';
    public const NAME_Am_FLAT = 'Abm';
    public const NAME_Bm = 'Bm';
    public const NAME_Bm_FLAT = 'Bbm';
    public const NAME_Cm = 'Cm';
    public const NAME_Cm_SHARP = 'C#m';
    public const NAME_Dm = 'Dm';
    public const NAME_Em_FLAT = 'Ebm';
    public const NAME_Em = 'Em';
    public const NAME_Fm = 'Fm';
    public const NAME_Fm_SHARP = 'F#m';
    public const NAME_Gm = 'Gm';
    public const NAME_Gm_SHARP = 'G#m';

    private const ID_LIST = [
        self::ID_A,
        self::ID_A_FLAT,
        self::ID_A_SHARP,
        self::ID_B,
        self::ID_B_FLAT,
        self::ID_C,
        self::ID_C_SHARP,
        self::ID_D,
        self::ID_E_FLAT,
        self::ID_E,
        self::ID_F,
        self::ID_F_SHARP,
        self::ID_G,
        self::ID_Am,
        self::ID_Am_FLAT,
        self::ID_Bm,
        self::ID_Bm_FLAT,
        self::ID_Cm,
        self::ID_Cm_SHARP,
        self::ID_Dm,
        self::ID_Em_FLAT,
        self::ID_Em,
        self::ID_Fm,
        self::ID_Fm_SHARP,
        self::ID_Gm,
        self::ID_Gm_SHARP,
    ];

    private const NAME_LIST = [
        self::NAME_A,
        self::NAME_A_FLAT,
        self::NAME_A_SHARP,
        self::NAME_B,
        self::NAME_B_FLAT,
        self::NAME_C,
        self::NAME_C_SHARP,
        self::NAME_D,
        self::NAME_E_FLAT,
        self::NAME_E,
        self::NAME_F,
        self::NAME_F_SHARP,
        self::NAME_G,
        self::NAME_Am,
        self::NAME_Am_FLAT,
        self::NAME_Bm,
        self::NAME_Bm_FLAT,
        self::NAME_Cm,
        self::NAME_Cm_SHARP,
        self::NAME_Dm,
        self::NAME_Em_FLAT,
        self::NAME_Em,
        self::NAME_Fm,
        self::NAME_Fm_SHARP,
        self::NAME_Gm,
        self::NAME_Gm_SHARP,
    ];

    private const MAPPING = [
        self::ID_A => self::NAME_A,
        self::ID_A_FLAT => self::NAME_A_FLAT,
        self::ID_A_SHARP => self::NAME_A_SHARP,
        self::ID_B => self::NAME_B,
        self::ID_B_FLAT => self::NAME_B_FLAT,
        self::ID_C => self::NAME_C,
        self::ID_C_SHARP => self::NAME_C_SHARP,
        self::ID_D => self::NAME_D,
        self::ID_E_FLAT => self::NAME_E_FLAT,
        self::ID_E => self::NAME_E,
        self::ID_F => self::NAME_F,
        self::ID_F_SHARP => self::NAME_F_SHARP,
        self::ID_G => self::NAME_G,
        self::ID_Am => self::NAME_Am,
        self::ID_Am_FLAT => self::NAME_Am_FLAT,
        self::ID_Bm => self::NAME_Bm,
        self::ID_Bm_FLAT => self::NAME_Bm_FLAT,
        self::ID_Cm => self::NAME_Cm,
        self::ID_Cm_SHARP => self::NAME_Cm_SHARP,
        self::ID_Dm => self::NAME_Dm,
        self::ID_Em_FLAT => self::NAME_Em_FLAT,
        self::ID_Em => self::NAME_Em,
        self::ID_Fm => self::NAME_Fm,
        self::ID_Fm_SHARP => self::NAME_Fm_SHARP,
        self::ID_Gm => self::NAME_Gm,
        self::ID_Gm_SHARP => self::NAME_Gm_SHARP,
    ];

    public static function getIdList(): array
    {
        return self::ID_LIST;
    }

    public static function getNameList(): array
    {
        return self::NAME_LIST;
    }

    public static function getIdToNameMapping(): array
    {
        return self::MAPPING;
    }
}