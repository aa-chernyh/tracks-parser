<?php

namespace common\enums;

interface EnumInterface
{

    /**
     * @return mixed[]
     */
    public static function getIdList(): array;

    /**
     * @return mixed[]
     */
    public static function getNameList(): array;

    /**
     * @return mixed[]
     */
    public static function getIdToNameMapping(): array;

    /**
     * @return mixed[]
     */
    public static function getNameToIdMapping(): array;

    /**
     * @param mixed $id
     * @return mixed
     */
    public static function getName($id);

    /**
     * @param mixed $name
     * @return mixed
     */
    public static function getId($name);
}