<?php

namespace common\collections;

use common\modelFacades\track\TrackFacade;
use common\modelFacades\track\TrackReadableInterface;
use Illuminate\Support\Collection;

/**
 * @property TrackFacade[] $items
 * @method TrackFacade[] all()
 * @method self filter(callable $callback = null)
 * @method TrackFacade first(callable $callback = null, mixed|null $default = null)
 */
class TracksCollection extends Collection
{

    /**
     * @param TrackFacade[] $tracks
     */
    public function __construct(array $tracks = [])
    {
        parent::__construct($tracks);
    }

    public function downloaded(): self
    {
        return $this->filter(function (TrackReadableInterface $trackReadable) {
            return $trackReadable->isDownloaded();
        });
    }

    public function notDownloaded(): self
    {
        return $this->filter(function (TrackReadableInterface $trackReadable) {
            return !$trackReadable->isDownloaded();
        });
    }
}