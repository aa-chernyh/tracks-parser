<?php

namespace common\collections;

use common\components\SingletonCollection;
use common\helpers\CollectionsAdapter;
use common\modelFacades\style\StyleFacade;
use common\models\Style;

/**
 * @property StyleFacade[] $items
 * @method StyleFacade[] all()
 * @method StyleFacade first(callable $callback = null, mixed|null $default = null)
 */
class StylesCollection extends SingletonCollection
{

    protected function __construct()
    {
        parent::__construct();
    }

    protected function getItems(): array
    {
        return CollectionsAdapter::getStyleFacades(Style::find()->orderBy('name')->all() ?? []);
    }

    public function getByName(string $name): ?StyleFacade
    {
        return $this->first(function (StyleFacade $styleReadable) use ($name) {
            return $styleReadable->getName() === $name;
        });
    }

    public function getById(string $id): ?StyleFacade
    {
        return $this->first(function (StyleFacade $styleReadable) use ($id) {
            return $styleReadable->getId() === $id;
        });
    }
}