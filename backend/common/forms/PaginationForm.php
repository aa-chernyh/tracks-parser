<?php

namespace common\forms;

use yii\base\Model;

class PaginationForm extends Model
{
    public ?int $totalCount = null;
    public ?int $perPage = null;
    public ?int $currentPage = null;

    public function rules(): array
    {
        return [
            [['perPage', 'currentPage'], 'required'],
            ['totalCount', 'safe'],
        ];
    }
}