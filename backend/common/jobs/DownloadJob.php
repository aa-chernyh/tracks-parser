<?php

namespace common\jobs;

use common\exceptions\LogicException;
use common\helpers\DownloadHelper;
use Yii;

class DownloadJob extends BaseJob
{

    public ?string $downloadUrl = null;
    public ?string $path = null;
    public bool $isUseProxy = false;
    public ?string $mutexLockName = null;

    /**
     * @throws LogicException
     */
    public function run(): void
    {
        $this->checkParams();

        if($this->mutexLockName) {
            Yii::$app->mutex->lock($this->mutexLockName);
        }

        try {
            if(preg_match('/.*(.m3u8)$/', $this->downloadUrl)) {
                DownloadHelper::downloadM3U8($this->downloadUrl, $this->path, $this->isUseProxy);
            } else {
                DownloadHelper::download($this->downloadUrl, $this->path, $this->isUseProxy);
            }
        } finally {
            if($this->mutexLockName) {
                Yii::$app->mutex->unlock($this->mutexLockName);
            }
        }
    }

    /**
     * @throws LogicException
     */
    private function checkParams(): void
    {
        if(!$this->downloadUrl || !$this->path) {
            $messageTemplate = 'Входные параметры неверны: %s';
            $params = json_encode($this);
            throw new LogicException(sprintf($messageTemplate, $params));
        }
    }
}