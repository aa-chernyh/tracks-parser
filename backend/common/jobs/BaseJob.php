<?php

namespace common\jobs;

use queue\models\IJob;
use yii\base\BaseObject;

abstract class BaseJob extends BaseObject implements IJob
{

}