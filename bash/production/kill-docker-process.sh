#!/bin/bash
IFS=$' \t\n' read -r -a array <<< $(docker top tracks-parser-app | grep "$1") && kill -9 "${array[1]}"

# Пояснения по поводу работы команды
# docker top tracks-parser-app - выводит таблицу с процессами, запущенными в контейнере
# $1 - первый аргумент переданной в вызове скрипта команды. Например, "php /var/www/tracks-parser/backend/console/yii queue/listen"
# | grep "php /var/www/tracks-parser/backend/condole/yii queue/listen" - выделяет строку с нужным нам процессом
# IFS - аналог explode в PHP. Разделяет строку на массив строк (array) по разделителю табуляции (в данном случае ' \t\n').
# Нужный нам PID находится во второй колонке (array[1])
# Тушить процесс по этому PID нужно прямо из-под хост-системы
# Запускать скрипт нужно через bash, потому что в dash нет оператора перенаправления (<<<)
