#!/bin/bash
cd /var/www/tracks-parser || exit

echo 'Останавливаем фоновые процессы и docker...'
sudo systemctl stop tp-socket
sudo systemctl stop tp-queue
sudo systemctl stop tp-docker

echo 'Сбрасываем изменения в коде...'
git reset --hard HEAD
git clean -d -f

echo 'Получаем изменения из репозитория...'
git pull tp master --rebase

echo 'Копируем файлы среды...'
cp -R /var/www/tracks-parser/environment/production/var/www/tracks-parser/* /var/www/tracks-parser/

echo 'Билдим контейнеры...'
docker-compose -f docker/docker-compose.yml build

echo 'Запускаем docker...'
sudo systemctl start tp-docker
sleep 10s

echo 'Устанавливаем composer-пакеты...'
docker exec -it tracks-parser-app composer install -n

echo 'Накатываем миграции...'
docker exec -it tracks-parser-app php console/yii migrate --interactive=0

echo 'Устанавливаем node_modules...'
docker run -v $(pwd)/frontend:/app -w /app --rm node:16.14-alpine3.14 yarn install

echo 'Собираем фронт...'
docker run -v $(pwd)/frontend:/app -w /app --rm node:16.14-alpine3.14 yarn build

echo 'Запускаем фоновые таски...'
sudo systemctl start tp-queue
sudo systemctl start tp-socket

echo 'Собираем docker-container для frontend v2...'
docker build -t tp-frontend-v2:latest ./docker/containers/frontend-v2/

echo 'Устанавливаем node_modules frontend v2...'
docker run -it --rm --volume $(pwd)/frontend-v2:/app -w /app tp-frontend-v2:latest yarn install

echo 'Собираем frontend v2...'
docker run -it --rm --volume $(pwd)/frontend-v2:/app -w /app tp-frontend-v2:latest quasar build

echo 'и 3, и 4... Закончили упражнение!'