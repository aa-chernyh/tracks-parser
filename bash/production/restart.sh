#!/bin/bash
echo 'Останавливаем фоновые процессы...'
sudo systemctl stop tp-socket
sudo systemctl stop tp-queue

echo 'Останавливаем docker...'
sudo systemctl stop tp-docker
sleep 10s;

echo 'Запускаем docker...'
sudo systemctl start tp-docker
sleep 10s;

echo 'Запускаем фоновые процессы...'
sudo systemctl start tp-queue
sudo systemctl start tp-socket


