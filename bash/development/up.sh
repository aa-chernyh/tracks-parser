#!/bin/bash
systemctl stop mysql
systemctl stop nginx
systemctl stop openvpn@client
cd ../../docker || exit
docker-compose up -d
sleep 4s;
echo 'Запуск фоновых процессов...'
docker exec -itd tracks-parser-app php console/yii queue/listen
docker exec -itd tracks-parser-app php console/yii socket/start

